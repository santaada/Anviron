<?php declare(strict_types=1);

namespace App\Security\Authorization;
 
enum UserRoles: string
{
    case ROLE_USER = 'ROLE_USER';
    case ROLE_SYSTEM_ADMIN = 'ROLE_SYSTEM_ADMIN';
}