<?php declare(strict_types=1);

namespace App\Security\Authorization\Voter;

use App\Entity\EnvironmentFactoryPort;
use App\Security\Authorization\UserRoles;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;


final class EnvironmentFactoryPortVoter extends Voter
{
    public const READ = 'ENVIRONMENT_FACTORY_PORT_READ';
    public const CREATE = 'ENVIRONMENT_FACTORY_PORT_CREATE';
    public const UPDATE = 'ENVIRONMENT_FACTORY_PORT_UPDATE';
    public const DELETE = 'ENVIRONMENT_FACTORY_PORT_DELETE';
    public function __construct(
        private readonly Security $security,
    ) {}


    /**
     * @param EnvironmentFactoryPort $subject
     */
    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::CREATE, self::READ, self::UPDATE, self::DELETE]);
    }

    /**
     * @param EnvironmentFactoryPort $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $userActor = $token->getUser();
        if (!$userActor instanceof UserInterface){
            return false;
        }
        if (!in_array($attribute, [self::CREATE, self::READ, self::UPDATE, self::DELETE]) ) {
            return false;
        }
        
        if ( in_array(UserRoles::ROLE_SYSTEM_ADMIN->value, $userActor->getRoles())) {
            return true; // superadmin can do anything
        }
        
        return match ($attribute) {
            self::READ => $this->canRead($subject, $userActor),
            self::CREATE => $this->canCreate($userActor),
            self::UPDATE => $this->canUpdate($subject, $userActor),
            self::DELETE => $this->canDelete($subject, $userActor),
            default => false,
        };
    }
    
    private function canRead(?EnvironmentFactoryPort $subject, UserInterface $userActor): bool
    {
        if ($subject?->getId() === null) {
            return $this->canCreate($userActor);
        }
        return $this->canUpdate($subject, $userActor) || $this->canDelete($subject, $userActor);
    }
    
    private function canCreate(UserInterface $userActor): bool
    {
        // can create environments => can create port reqs for environments
        return $this->security->isGranted(EnvironmentFactoryVoter::CREATE);
    }

    private function canUpdate(?EnvironmentFactoryPort $subject, UserInterface $userActor): bool
    {
        if ($subject?->getId() === null) {
            return $this->canCreate($userActor);
        }
        // can update environments => can create port reqs for environments
        return $this->security->isGranted(EnvironmentFactoryVoter::UPDATE, $subject->getEnvironmentFactory());
    }

    private function canDelete(?EnvironmentFactoryPort $subject, UserInterface $userActor): bool
    {
        // can delete environment => can delete port reqs for environment
        return $this->security->isGranted(EnvironmentFactoryVoter::DELETE, $subject?->getEnvironmentFactory());
    }
}
