<?php declare(strict_types=1);

namespace App\Security\Authorization\Voter;

use App\Entity\EnvironmentImage;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Service\CourseMemberService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;


final class EnvironmentImageVoter extends Voter
{
    public const CREATE = 'ENVIRONMENT_IMAGE_CREATE';
    public const READ = 'ENVIRONMENT_IMAGE_READ';
    public const UPDATE = 'ENVIRONMENT_IMAGE_UPDATE';
    public const DELETE = 'ENVIRONMENT_IMAGE_DELETE';


    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly CourseMemberService $courseMemberService,
    ) {}


    /**
     * @param EnvironmentImage $subject
     */
    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [
                self::CREATE,
                self::READ, 
                self::UPDATE, 
                self::DELETE,
            ]) && $subject instanceof EnvironmentImage;
    }

    /**
     * @param EnvironmentImage $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $userActor = $token->getUser();
        if (!$userActor instanceof UserInterface){
            return false;
        }
        if (!$subject instanceof EnvironmentImage) {
            return false;
        }
        
        if ( in_array(UserRoles::ROLE_SYSTEM_ADMIN->value, $userActor->getRoles())) {
            return true; // superadmin can do anything
        }

        return match ($attribute) {
            self::CREATE => $this->canCreate($subject, $userActor),
            self::READ => $this->canRead($subject, $userActor),
            self::UPDATE => $this->canUpdate($subject, $userActor),
            self::DELETE => $this->canDelete($subject, $userActor),
            default => false,
        };
    }


    private function canCreate(EnvironmentImage $subject, UserInterface $userActor): bool
    {
        return false; // only superadmins can create EnvironmentImages
    }

    
    private function canRead(EnvironmentImage $subject, UserInterface $userActor): bool
    {
        $userActorInstance = $this->userRepository->findOneBy([
            'username' => $userActor->getUserIdentifier(),
        ]);
        assert($userActorInstance instanceof User);
        // Only teachers can see environment images
        return $this->courseMemberService->hasAnyTeacherRole($userActorInstance);
    }

    
    private function canUpdate(EnvironmentImage $subject, UserInterface $userActor): bool
    {
        return false; // only superadmins can update EnvironmentImages
    }
    

    private function canDelete(EnvironmentImage $subject, UserInterface $userActor): bool
    {
        return false; // only superadmins can delete EnvironmentImages
    }
}
