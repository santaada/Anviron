<?php declare(strict_types=1);

namespace App\Security\Authorization\Voter;

use App\Entity\EnvironmentFactory;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Service\CourseMemberService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;


final class EnvironmentFactoryVoter extends Voter
{
    public const CREATE = 'ENVIRONMENT_FACTORY_CREATE';
    public const READ = 'ENVIRONMENT_FACTORY_READ';
    public const UPDATE = 'ENVIRONMENT_FACTORY_UPDATE';
    public const UPSERT = 'ENVIRONMENT_FACTORY_UPSERT';
    public const RUN = 'ENVIRONMENT_FACTORY_RUN';
    public const DELETE = 'ENVIRONMENT_FACTORY_DELETE';

    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly CourseMemberService $courseMemberService,
    ) {}

    /**
     * @param EnvironmentFactory $subject
     */
    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [
                self::UPDATE, 
                self::RUN,
                self::DELETE,
        ]) && $subject instanceof EnvironmentFactory || in_array($attribute, [self::CREATE, self::READ, self::UPSERT]);
    }

    /**
     * @param EnvironmentFactory $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $userActor = $token->getUser();
        if (!$userActor instanceof UserInterface){
            return false;
        }
        if (! in_array($attribute, [self::CREATE, self::READ, self::UPDATE, self::UPSERT, self::RUN, self::DELETE])) {
            return false;
        }
        
        if ( in_array(UserRoles::ROLE_SYSTEM_ADMIN->value, $userActor->getRoles())) {
            return true; // superadmin can do anything
        }
        
        return match ($attribute) {
            self::UPSERT => $this->canUpsert($subject, $userActor),
            self::CREATE => $this->canCreate($subject, $userActor),
            self::READ => $this->canRead($subject, $userActor),
            self::UPDATE => $this->canUpdate($subject, $userActor),
            self::RUN => $this->canRun($subject, $userActor),
            self::DELETE => $this->canDelete($subject, $userActor),
            default => false,
        };
    }

    private function canCreate(?EnvironmentFactory $subject, UserInterface $userActor): bool
    {
        $userActorInstance = $this->userRepository->findOneBy([
            'username' => $userActor->getUserIdentifier(),
        ]);
        assert($userActorInstance instanceof User);
        // Only teachers can create environment factories
        return $this->courseMemberService->hasAnyTeacherRole($userActorInstance);
    }
    
    private function canRead(?EnvironmentFactory $subject, UserInterface $userActor): bool
    {
        // Read of empty instance is permitted for those who can create 
        if ($subject === null) {
            return $this->canCreate($subject, $userActor);
        }
        
        // Maintainer can always read
        if ($subject->getMaintainedBy() && 
            $subject->getMaintainedBy()->getUserIdentifier() === $userActor->getUserIdentifier()) {
            return true;
        }
        
        $userActorInstance = $this->userRepository->findOneBy([
            'username' => $userActor->getUserIdentifier(),
        ]);
        assert($userActorInstance instanceof User);

        // Any teacher of a course linked with this EFactory can read
        if ($this->courseMemberService->hasAnyTeacherRoleInCourses(
            $userActorInstance, $subject->getCourses()->toArray())
        ) {
            return true;
        }
        
        // Students of a course linked with this EFactory can read
        if ($subject->getIsVisibleForCourseStudents()) {
            if ($this->courseMemberService->hasAnyStudentRoleInCourses(
                $userActorInstance, $subject->getCourses()->toArray())
            ) {
                return true;
            }
        }
        
        return false;
    }
    
    private function canUpdate(EnvironmentFactory $subject, UserInterface $userActor): bool
    {
        // Only maintainer can update
        if (
            $subject->getMaintainedBy() && 
            $subject->getMaintainedBy()->getUserIdentifier() === $userActor->getUserIdentifier()) {
            return true;
        }
        return false;
    }

    
    private function canRun(EnvironmentFactory $subject, UserInterface $userActor): bool
    {
        return $this->canRead($subject, $userActor);
    }
    
    private function canUpsert(?EnvironmentFactory $subject, UserInterface $userActor): bool
    {
        if ($subject?->getId() === null) {
            return $this->canCreate($subject, $userActor);
        }
        return $this->canUpdate($subject, $userActor);
    }

    private function canDelete(EnvironmentFactory $subject, UserInterface $userActor): bool
    {
        // currently updaters and deleters are the same
        return $this->canUpsert($subject, $userActor);
    }
}
