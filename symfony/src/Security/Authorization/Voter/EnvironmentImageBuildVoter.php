<?php declare(strict_types=1);

namespace App\Security\Authorization\Voter;


use App\Entity\EnvironmentImage;
use App\Entity\EnvironmentImageBuild;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Service\CourseMemberService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;


final class EnvironmentImageBuildVoter extends Voter
{
    public const READ = 'ENVIRONMENT_IMAGE_BUILD_READ';
    
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly CourseMemberService $courseMemberService,
    ) {}

    /**
     * @param EnvironmentImage $subject
     */
    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [
                self::READ, 
            ]);
    }

    /**
     * @param EnvironmentImageBuild $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $userActor = $token->getUser();
        if (!$userActor instanceof UserInterface){
            return false;
        }
        if (!$subject instanceof EnvironmentImageBuild) {
            return false;
        }
        
        if ( in_array(UserRoles::ROLE_SYSTEM_ADMIN->value, $userActor->getRoles())) {
            return true; // superadmin can do anything
        }

        return match ($attribute) {
            self::READ => $this->canRead($subject, $userActor),
            default => false,
        };
    }
    
    private function canRead(EnvironmentImageBuild $subject, UserInterface $userActor): bool
    {
        $userActorInstance = $this->userRepository->findOneBy([
            'username' => $userActor->getUserIdentifier(),
        ]);
        assert($userActorInstance instanceof User);
        // Only teachers or system admins can see environment images
        return $this->courseMemberService->hasAnyTeacherRole($userActorInstance);
    }
}
