<?php declare(strict_types=1);

namespace App\Security\Authorization\Voter;

use App\Entity\CourseMember;
use App\Entity\CourseRoleType;
use App\Entity\User;
use App\Repository\CourseMemberRepository;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Service\CourseMemberService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;


final class UserVoter extends Voter
{
    public const READ = 'USER_READ';
    public const UPDATE_DETAIL = 'USER_UPDATE_DETAIL';
    public const UPDATE_ROLES = 'USER_UPDATE_ROLES';
    public const DELETE = 'USER_DELETE';


    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly CourseMemberRepository $courseMemberRepository,
        private readonly CourseMemberService $courseMemberService,
    ) {}

    /**
     * @param UserInterface $subject
     */
    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [
                self::READ, 
                self::UPDATE_DETAIL, 
                self::UPDATE_ROLES, 
                self::DELETE,
            ]) && $subject instanceof UserInterface;
    }
    
    /**
     * @param UserInterface $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $userSubject = $subject;
        // if the user is anonymous, do not grant access
        if (!$userSubject instanceof UserInterface) {
            return false;
        }
        $userActor = $token->getUser();
        if (!$userActor instanceof UserInterface) {
            return false;
        }
        if ( in_array(UserRoles::ROLE_SYSTEM_ADMIN->value, $userActor->getRoles())) {
            return true; // superadmin can do anything
        }

        return match ($attribute) {
            self::READ => $this->canRead($userSubject, $userActor),
            self::UPDATE_DETAIL => $this->canUpdateDetail($userSubject, $userActor),
            self::UPDATE_ROLES => $this->canUpdateRoles($userSubject, $userActor),
            self::DELETE => $this->canDelete($userSubject, $userActor),
            default => false,
        };
    }

    private function canRead(UserInterface $userSubject, UserInterface $userActor): bool
    {
        // 0 User should always see its own profile
        if ($userActor->getUserIdentifier() === $userSubject->getUserIdentifier()) {
            return true; // Everyone should see the own profile
        }

        // 1 Teachers are visible to all app users
        $userSubjectInstance = $this->userRepository->findOneBy([
            'username' => $userSubject->getUserIdentifier(),
        ]);
        assert($userSubjectInstance instanceof User);
        if ($this->courseMemberService->hasAnyTeacherRole($userSubjectInstance)) {
            return true; 
        }
        
        // 2 Students are visible to teachers of subjects they're enrolled in
        $userActorInstance = $this->userRepository->findOneBy([
            'username' => $userActor->getUserIdentifier(),
        ]);
        // Find actor's courses he teaches
        $userActorCourseMembersTeach = $this->courseMemberRepository->findBy([
            'user' => $userActorInstance,
            'role' => CourseRoleType::TEACHER->value,
        ]);
        // Doesn't teach anything => can't see any students
        if ($userActorCourseMembersTeach) {
            // Find subject's courses he studies
            $userSubjectCourseMembersStudy = $this->courseMemberRepository->findBy([
                'user' => $userSubjectInstance,
                'role' => CourseRoleType::STUDENT->value,
            ]);
            // Intersect in courses => is visible
            foreach ($userActorCourseMembersTeach as $userActorCourseMember) {
                $teacherCourse = $userActorCourseMember->getCourse();
                $studentCourses = array_map(function (CourseMember $courseMember){
                    return $courseMember->getCourse();
                }, $userSubjectCourseMembersStudy);
                if (in_array($teacherCourse, $studentCourses)){
                    return true;
                }
            }
        }
        
        // 3 The subject user's profile is not visible to the actor
        return false;
    }

    private function canUpdateDetail(UserInterface $userSubject, UserInterface $userActor): bool
    {
        if ($userActor->getUserIdentifier() === $userSubject->getUserIdentifier()) {
            return true; // Only a profile owner can alter the profile
        }
        return false;
    }

    private function canUpdateRoles(UserInterface $userSubject, UserInterface $userActor): bool
    {
        return false; // Only by superadmin - see above
    }
    
    private function canDelete(UserInterface $userSubject, UserInterface $userActor): bool
    {
        return false; // Only by superadmin - see above
    }
}
