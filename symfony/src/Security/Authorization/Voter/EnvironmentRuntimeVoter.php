<?php declare(strict_types=1);

namespace App\Security\Authorization\Voter;

use App\Entity\EnvironmentRuntime;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Service\CourseMemberService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;


final class EnvironmentRuntimeVoter extends Voter
{
    public const READ = 'ENVIRONMENT_RUNTIME_READ';    
    public const CONTROL = 'ENVIRONMENT_RUNTIME_CONTROL';
    public const DELETE = 'ENVIRONMENT_RUNTIME_DELETE';

    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly CourseMemberService $courseMemberService,
    ) {}


    /**
     * @param EnvironmentRuntime $subject
     */
    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [
                self::READ, 
                self::CONTROL,
                self::DELETE,
        ]) && $subject instanceof EnvironmentRuntime;
    }

    /**
     * @param EnvironmentRuntime $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $userActor = $token->getUser();
        if (!$userActor instanceof UserInterface){
            return false;
        }
        if (! $subject instanceof EnvironmentRuntime) {
            return false;
        }
        
        if ( in_array(UserRoles::ROLE_SYSTEM_ADMIN->value, $userActor->getRoles())) {
            return true; // superadmin can do anything
        }
        
        return match ($attribute) {
            self::READ => $this->canRead($subject, $userActor),
            self::CONTROL => $this->canShutdown($subject, $userActor),
            self::DELETE => $this->canDelete($subject, $userActor),
            default => false,
        };
    }
    
    private function canRead(EnvironmentRuntime $subject, UserInterface $userActor): bool
    {
        // Can shut down => can read
        if ($this->canShutdown($subject, $userActor)) {
            return true;
        }
        // Any teacher of an actor can see the student's runtime
        $userActorInstance = $this->userRepository->findOneBy([
            'username' => $userActor->getUserIdentifier(),
        ]);
        assert($userActorInstance instanceof User);
        if ($this->courseMemberService->isTeacherOf($userActorInstance, $subject->getUsedBy())) {
            return true;
        }
        return false;
    }
    
    private function canShutdown(EnvironmentRuntime $subject, UserInterface $userActor): bool
    {
        // User who launched the runtime can shut down
        if ($subject->getUsedBy()->getUserIdentifier() === $userActor->getUserIdentifier()) {
            return true;
        }
        return false;
    }

    private function canDelete(EnvironmentRuntime $subject, UserInterface $userActor): bool
    {
        // Only superadmins and env maintainers can delete
        if (
            $subject->getEnvironmentFactory()->getMaintainedBy()?->getUserIdentifier() 
            === 
            $userActor->getUserIdentifier()
        ) {
            return true;
        }
        return false;
    }
}
