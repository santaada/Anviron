<?php declare(strict_types=1);

namespace App\Security\Authenticator;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Integration\OAuth\Provider\ZuulOaasProviderFactory;
use App\Integration\OAuth\Session\ZuulOaasSessionManagerFactory;
use App\Integration\UsermapApi\UsermapApiIntegrationFactory;
use Doctrine\ORM\EntityManagerInterface;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;


class ZuulOaasAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private readonly ZuulOaasProviderFactory       $providerFactory,
        private readonly ZuulOaasSessionManagerFactory $sessionManagerFactory,
        private readonly UserRepository                $userRepository,
        private readonly EntityManagerInterface        $manager,
        private readonly UsermapApiIntegrationFactory  $usermapFactory,
    ) {}
    
    public function supports(Request $request): ?bool
    {
        $sessionManager = $this->sessionManagerFactory->create($request->getSession());
        $token = $sessionManager->getToken();
        return $token !== null;
    }

    public function authenticate(Request $request): Passport
    {
        $provider = $this->providerFactory->create();
        $sessionManager = $this->sessionManagerFactory->create($request->getSession());
        $usermap = $this->usermapFactory->create($request->getSession());
        
        $token = $sessionManager->getToken();
        if ($token === null) {
            throw new CustomUserMessageAuthenticationException("Invalid token. Please log in again.");
        }
        assert($token instanceof AccessToken);
        $resourceOwner = $provider->getResourceOwner($token);
        $user = $this->userRepository->findOneBy(['username' => $resourceOwner->toArray()['user_id']]);
        if ($user === null) {
            $user = new User();
        }
        $user->setUsername($resourceOwner->toArray()['user_id']);
        $user->setEmail($resourceOwner->toArray()['user_email']);
        
        $usermapUser = $usermap->getPeople($user->getUsername());
        if ($usermapUser === null) {
            throw new UserNotFoundException();
        }
        
        $user->setFirstName($usermapUser['firstName']);
        $user->setLastName($usermapUser['lastName']);
        
        $this->manager->persist($user);
        $this->manager->flush();
        
        return new SelfValidatingPassport(new UserBadge($user->getUsername(), function ($username) {
            return $this->userRepository->findOneBy(['username' => $username]);
        }));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new JsonResponse(['exc' => $exception->getMessage()]); // TODO
        // return new RedirectResponse('/login/fail');
    }
}