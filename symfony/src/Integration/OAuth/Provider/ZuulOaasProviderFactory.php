<?php declare(strict_types=1);

namespace App\Integration\OAuth\Provider;

use League\OAuth2\Client\OptionProvider\HttpBasicAuthOptionProvider;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class ZuulOaasProviderFactory
{
    public function __construct(
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly string $appHost,
        private readonly string $clientId,
        private readonly string $clientSecret,
        private readonly string $serviceAccClientId,
        private readonly string $serviceAccClientSecret,
        private readonly string $urlAuthorize,
        private readonly string $urlAccessToken,
        private readonly string $urlResourceOwnerDetails,
    ) {}
    
    public function create(): ZuulOaasProvider
    {
        $redirectUri = "https://" . $this->appHost 
            . $this->urlGenerator->generate('app_login_oauth_fit', [UrlGeneratorInterface::ABSOLUTE_PATH]);
        
        $provider = new ZuulOaasProvider([
            'clientId'                => $this->clientId,
            'clientSecret'            => $this->clientSecret,
            'redirectUri'             => $redirectUri,
            'urlAuthorize'            => $this->urlAuthorize,
            'urlAccessToken'          => $this->urlAccessToken,
            'urlResourceOwnerDetails' => $this->urlResourceOwnerDetails,
        ]);
        $provider->setOptionProvider(new HttpBasicAuthOptionProvider());
        return $provider;
    }
    
    public function createForServiceAccount(): ZuulOaasProvider
    {
        $provider = new ZuulOaasProvider([
            'clientId'                => $this->serviceAccClientId,
            'clientSecret'            => $this->serviceAccClientSecret,
            'urlAuthorize'            => $this->urlAuthorize,
            'urlAccessToken'          => $this->urlAccessToken,
            'urlResourceOwnerDetails' => $this->urlResourceOwnerDetails,
        ]);
        $provider->setOptionProvider(new HttpBasicAuthOptionProvider());
        return $provider;
    }
}