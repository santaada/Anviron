<?php declare(strict_types=1);

namespace App\Integration\OAuth\Enum;

enum AccessTokenState: int
{
    case ACQUIRATION_REQIRED = 0;
    case REFRESH_REQIRED = 1;
    case VALID = 2;
}