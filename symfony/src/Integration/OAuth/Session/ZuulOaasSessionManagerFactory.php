<?php declare(strict_types=1);

namespace App\Integration\OAuth\Session;

use Symfony\Component\HttpFoundation\Session\SessionInterface;


class ZuulOaasSessionManagerFactory
{
    public function create(SessionInterface $session): ZuulOaasSessionManager
    {
        return new ZuulOaasSessionManager($session);
    }
}