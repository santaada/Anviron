<?php declare(strict_types=1);

namespace App\Integration\OAuth\Session\Dao;


final class StateDao extends AbstractDao
{
    public function getState(): ?string
    {
        return strval($this->session->get('oauth2.state'));
    }
    
    public function setState(?string $state): void
    {
        $this->session->set('oauth2.state', $state);
    }

    /**
     * @param array{state: string} $arr
     */
    public function fromArray(array $arr): void
    {
        $this->session->set('oauth2.state', $arr['state']);
    }
}