<?php declare(strict_types=1);

namespace App\Integration\OAuth\Session\Dao;

use Symfony\Component\HttpFoundation\Session\SessionInterface;


abstract class AbstractDao
{
    public function __construct(
        protected SessionInterface $session,
    ) {}


    /**
     * @param array{state: string}|array{access_token: string, refresh_token: string, expires:string} $arr
     */
    abstract public function fromArray(array $arr): void;
}