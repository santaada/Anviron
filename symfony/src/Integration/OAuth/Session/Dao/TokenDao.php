<?php declare(strict_types=1);

namespace App\Integration\OAuth\Session\Dao;

use DateTime;
use League\OAuth2\Client\Token\AccessToken;


final class TokenDao extends AbstractDao
{    
    public function getAccess(): ?string
    {
        if ( ! $this->getExpiresAt() || $this->getExpiresAt() <= new DateTime()) {
            return null;
        }
        return strval($this->session->get('oauth2.token.access'));
    }

    public function getRefresh(): ?string
    {
        return strval($this->session->get('oauth2.token.refresh'));
    }

    public function getExpiresAt(): ?DateTime
    {
        $sessionData = intval($this->session->get('oauth2.token.expires'));
        if ( ! $sessionData ) {
            return null;
        }
        $expiresAtDatetime = new DateTime();
        $expiresAtDatetime->setTimestamp($sessionData);
        return $expiresAtDatetime;
    }
    
    /**
     * @param array{access_token: string, refresh_token: string, expires:string} $arr
     */
    public function fromArray(array $arr): void
    {
        $this->session->set('oauth2.token.access', $arr['access_token']);
        $this->session->set('oauth2.token.refresh', $arr['refresh_token']);
        $this->session->set('oauth2.token.expires', $arr['expires']);
    }
    
    public function getToken(): ?AccessToken
    {
        $access = $this->getAccess();
        $refresh = $this->getRefresh();
        $expires = $this->getExpiresAt();

        if ( ! $access || ! $refresh || ! $expires) {
            return null;
        }

        return new AccessToken([
            'access_token' => $access,
            'refresh_token' => $refresh,
            'expires' => $expires->getTimestamp(),
        ]);
    }
}