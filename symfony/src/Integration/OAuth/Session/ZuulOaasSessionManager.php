<?php declare(strict_types=1);

namespace App\Integration\OAuth\Session;

use App\Integration\OAuth\Enum\AccessTokenState;
use App\Integration\OAuth\Session\Dao\StateDao;
use App\Integration\OAuth\Session\Dao\TokenDao;
use DateTime;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class ZuulOaasSessionManager
{
    private readonly StateDao $stateDao;
    private readonly TokenDao $tokenDao;
    
    public function __construct(
        SessionInterface $session,
    ) {
        $this->stateDao = new StateDao($session);
        $this->tokenDao = new TokenDao($session);
    }
    
    // State
    
    public function setState(string $state): void
    {
        $this->stateDao->setState($state);
    }

    public function checkState(string $state): bool
    {
        return $this->stateDao->getState() === $state;
    }

    public function clearState(): void
    {
        $this->stateDao->setState(null);
    }
    
    public function getToken(): ?AccessTokenInterface
    {
        $token = $this->tokenDao->getToken();
        if (!$token || $token->hasExpired()){
            return null;
        }
        return $this->tokenDao->getToken(); // TODO Rotate token if expired
    }

    /**
     * @param array{access_token: string, refresh_token: string, expires:string} $arr
     */
    public function setToken(array $arr): void
    {
        $this->tokenDao->fromArray($arr);
    }

    public function checkAccessToken(): int
    {
        if ( ! $this->tokenDao->getAccess() || ! $this->tokenDao->getRefresh() || ! $this->tokenDao->getExpiresAt() ) {
            return AccessTokenState::ACQUIRATION_REQIRED->value;
        }

        if (new DateTime() >= $this->tokenDao->getExpiresAt()){
            return AccessTokenState::REFRESH_REQIRED->value;
        }

        return AccessTokenState::VALID->value;
    }
}