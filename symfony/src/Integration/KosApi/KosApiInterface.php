<?php declare(strict_types=1);

namespace App\Integration\KosApi;

use App\Integration\KosApi\Command\AbstractKosApiCommand;
use App\Integration\KosApi\Dto\AbstractKosApiDto;
use Generator;


interface KosApiInterface
{
    /**
     * @return Generator<AbstractKosApiDto>
     */
    public function get(AbstractKosApiCommand $command): Generator;
}