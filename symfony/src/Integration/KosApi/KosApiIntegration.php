<?php declare(strict_types=1);

namespace App\Integration\KosApi;

use App\Integration\KosApi\Command\AbstractKosApiCommand;
use App\Integration\KosApi\Dto\AbstractKosApiDto;
use App\Integration\OAuth\Provider\ZuulOaasProvider;
use App\Integration\OAuth\Provider\ZuulOaasProviderFactory;
use Generator;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;


class KosApiIntegration implements KosApiInterface
{
    private readonly ZuulOaasProvider $provider;
    
    public function __construct(
        private readonly ZuulOaasProviderFactory $providerFactory,
        private readonly KosApiAuthenticator     $authenticator,
    ) {
        $this->provider = $this->providerFactory->createForServiceAccount();
    }
    
    /**
     * @return Generator<AbstractKosApiDto>
     */
    public function get(AbstractKosApiCommand $command): Generator
    {
        if ($this->authenticator->getToken() instanceof AccessTokenInterface) {
            yield from $command->process($this->provider, $this->authenticator->getToken());
        }
    }
}