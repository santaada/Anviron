<?php declare(strict_types=1);

namespace App\Integration\KosApi\Dto;

use DateTime;
use DateTimeInterface;
use SimpleXMLElement;


final class CourseDto extends AbstractKosApiDto
{
    public function __construct(
        public string $id,
        public string $code,
        public string $title,
        // WARN KOS Api response contains in fact more data, but they're not relevant for this app's scope
    ) {}

    public static function fromXml(SimpleXMLElement $xml): ?CourseDto
    {
        /** @var SimpleXMLElement[] $id */
        $id = $xml->xpath('atom:id');
        /** @var SimpleXMLElement[] $code */
        $code = $xml->xpath('atom:content/ns:code');
        /** @var SimpleXMLElement[] $title */
        $title = $xml->xpath('atom:title');
        /** @var SimpleXMLElement[] $state */
        $state = $xml->xpath('atom:content/ns:state');
        
        if (
            ! isset($state[0])
            || $state[0]->__toString() !== 'APPROVED'
        ) {
            return null;
        }
        
        return new CourseDto(
            id: $id[0]->__toString(),
            code: $code[0]->__toString(),
            title: $title[0]->__toString(),
        );
    }
}