<?php declare(strict_types=1);

namespace App\Integration\KosApi\Dto;

use Nette\Utils\Strings;
use SimpleXMLElement;


final class CourseInstanceDto extends AbstractKosApiDto
{
    public function __construct(
        public string $courseCode,
        public string $termCode,
        // WARN KOS Api response contains in fact more data, but they're not relevant for this app's scope
    ) {}

    public static function fromXml(SimpleXMLElement $xml): ?CourseInstanceDto
    {
        /** @var SimpleXMLElement[] $linkElem */
        $linkElem = $xml->xpath('atom:link');
        
        if ( ! $linkElem || ! isset($linkElem[0]) ) return null;
        
        assert($linkElem[0]->attributes() instanceof SimpleXMLElement);
        assert($linkElem[0]->attributes()['href'] instanceof SimpleXMLElement);
        
        $link = $linkElem[0]->attributes()['href']->__toString();
        $linkSplitted = Strings::split($link, '~/\s*~', PREG_SPLIT_NO_EMPTY);
        
        return new CourseInstanceDto(
            courseCode: $linkSplitted[1],
            termCode: $linkSplitted[3], 
        );
    }
}