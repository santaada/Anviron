<?php declare(strict_types=1);

namespace App\Integration\KosApi\Dto;

use Nette\Utils\Strings;
use SimpleXMLElement;


final class StudentEnrolledCourseDto extends AbstractKosApiDto
{
    public function __construct(
        public string $subjectCode,
        public string $termCode,
    ) {}

    public static function fromXml(SimpleXMLElement $xml): ?StudentEnrolledCourseDto
    {
        /** @var SimpleXMLElement[] $courseElem */
        $courseElem = $xml->xpath('atom:content/ns:course/@xlink:href');
        /** @var SimpleXMLElement[] $semesterElem */
        $semesterElem = $xml->xpath('atom:content/ns:semester/@xlink:href');
        
        if ( ! $courseElem || ! isset($courseElem[0]) || ! isset($courseElem[0]["href"]) ) return null;
        if ( ! $semesterElem || ! isset($semesterElem[0]) || ! isset($semesterElem[0]["href"])) return null;
        
        /** @var SimpleXMLElement $courseHrefXml */
        $courseHrefXml = $courseElem[0]['href'];
        /** @var SimpleXMLElement $semesterHrefXml */
        $semesterHrefXml = $semesterElem[0]['href'];
        
        $courseHref = $courseHrefXml->__toString();
        $semesterHref = $semesterHrefXml->__toString();
        
        $courseSplitted = Strings::split($courseHref, '~/\s*~', PREG_SPLIT_NO_EMPTY);
        $semesterSplitted = Strings::split($semesterHref, '~/\s*~', PREG_SPLIT_NO_EMPTY);

        return new StudentEnrolledCourseDto(
            subjectCode: $courseSplitted[1],
            termCode: $semesterSplitted[1],
        );
    }
}