<?php declare(strict_types=1);

namespace App\Integration\KosApi\Dto;

use App\Entity\CourseRoleType;
use Nette\Utils\Strings;
use SimpleXMLElement;


final class TeacherEnrolledCourseDto extends AbstractKosApiDto
{
    public function __construct(
        public string $subjectCode,
        public string $termCode,

        /** @var string[] */
        public array $editors,
        /** @var string[] */
        public array $examiners,
        /** @var string[] */
        public array $guarantors,
        /** @var string[] */
        public array $instructors,
        /** @var string[] */
        public array $lecturers,
    ) {}

    public static function fromXml(SimpleXMLElement $xml): ?TeacherEnrolledCourseDto
    {
        /** @var SimpleXMLElement[] $linkElem */
        $linkElem = $xml->xpath('atom:link');
        /** @var SimpleXMLElement[] $termElem */
        $termElem = $xml->xpath('atom:content/ns:instance');
        
        /** @var SimpleXMLElement[] $editorsElems */
        $editorsElems = $xml->xpath('atom:content/ns:instance/ns:editors/ns:teacher/@xlink:href');
        /** @var SimpleXMLElement[] $examinersElems */
        $examinersElems = $xml->xpath('atom:content/ns:instance/ns:examiners/ns:teacher/@xlink:href');
        /** @var SimpleXMLElement[] $guarantorsElems */
        $guarantorsElems = $xml->xpath('atom:content/ns:instance/ns:guarantors/ns:teacher/@xlink:href');
        /** @var SimpleXMLElement[] $instructorsElems */
        $instructorsElems = $xml->xpath('atom:content/ns:instance/ns:instructors/ns:teacher/@xlink:href');
        /** @var SimpleXMLElement[] $lecturersElems */
        $lecturersElems = $xml->xpath('atom:content/ns:instance/ns:lecturers/ns:teacher/@xlink:href');
        
        if ( ! self::isAttributeSet($editorsElems, 'href') ) return null;
        if ( ! self::isAttributeSet($examinersElems, 'href') ) return null;
        if ( ! self::isAttributeSet($guarantorsElems, 'href') ) return null;
        if ( ! self::isAttributeSet($instructorsElems, 'href') ) return null;
        if ( ! self::isAttributeSet($lecturersElems, 'href') ) return null;
        
        assert($linkElem[0]->attributes() instanceof SimpleXMLElement);
        assert($termElem[0]->attributes() instanceof SimpleXMLElement);

        assert($linkElem[0]->attributes()['href'] instanceof SimpleXMLElement);
        assert($termElem[0]->attributes()['semester'] instanceof SimpleXMLElement);
        
        return new TeacherEnrolledCourseDto(
            subjectCode: self::getCourseCodeFromHref($linkElem[0]->attributes()['href']->__toString()),
            termCode: $termElem[0]->attributes()['semester']->__toString(),
            editors: self::getUsernamesFromElemList($editorsElems),
            examiners: self::getUsernamesFromElemList($examinersElems),
            guarantors: self::getUsernamesFromElemList($guarantorsElems),
            instructors: self::getUsernamesFromElemList($instructorsElems),
            lecturers: self::getUsernamesFromElemList($lecturersElems),
        );
    }


    /**
     * @param SimpleXMLElement[] $elems
     * @return string[]                  
     */
    private static function getUsernamesFromElemList(array $elems): array
    {
        return array_values(array_map(
            fn( $lst ) => self::getUsernameFromHref($lst),
            self::getAttributeStringList($elems, 'href')
        ));
    }

    /** @param SimpleXMLElement[] $elems */
    private static function isAttributeSet(array $elems, string $attrName): bool
    {
        return $elems && isset($elems[0]) && isset($elems[0][$attrName]);
    }
    
    private static function getAttributeString(SimpleXMLElement $elem, string $attrName): ?string
    {
        return $elem[$attrName]?->__toString();
    }
    
    /**
     * @param SimpleXMLElement[] $elems
     * @return string[]
     */
    private static function getAttributeStringList(array $elems, string $attrName): array
    {
        $res = [];
        foreach ($elems as $elem) {
            $parsedElem = self::getAttributeString($elem, $attrName);
            if ($parsedElem === null) continue;
            $res[] = $parsedElem;
        }
        return $res;
    }
    
    private static function getUsernameFromHref(string $href): string
    {
        $splitted = Strings::split($href, '~/\s*~', PREG_SPLIT_NO_EMPTY);
        return $splitted[1];
    }

    private static function getCourseCodeFromHref(string $href): string
    {
        $splitted = Strings::split($href, '~/\s*~', PREG_SPLIT_NO_EMPTY);
        return $splitted[1];
    }
}