<?php declare(strict_types=1);

namespace App\Integration\KosApi\Dto;

use DateTime;
use DateTimeInterface;
use SimpleXMLElement;


final class SemesterDto extends AbstractKosApiDto
{
    public function __construct(
        public string $id,
        public string $code,
        public string $title,
        public DateTime $startsAt,
        public DateTime $endsAt,
        // WARN KOS Api response contains in fact more data, but they're not relevant for this app's scope
    ) {}

    public static function fromXml(SimpleXMLElement $xml): ?SemesterDto
    {
        /** @var SimpleXMLElement[] $id */
        $id = $xml->xpath('atom:id');
        /** @var SimpleXMLElement[] $code */
        $code = $xml->xpath('atom:content/ns:code');
        /** @var SimpleXMLElement[] $title */
        $title = $xml->xpath('atom:title');
        /** @var SimpleXMLElement[] $startsAt */
        $startsAt = $xml->xpath('atom:content/ns:startDate');
        /** @var SimpleXMLElement[] $endsAt */
        $endsAt = $xml->xpath('atom:content/ns:endDate');
        
        if (
            ! isset($startsAt[0]) 
            || $code[0]->__toString() === 'invalid'
        ) {
            return null;
        }
        return new SemesterDto(
            id: $id[0]->__toString(),
            code: $code[0]->__toString(),
            title: $title[0]->__toString(),
            startsAt: new DateTime( $startsAt[0]->__toString() ),
            endsAt: new DateTime( $endsAt[0]->__toString() ),
        );
    }
}