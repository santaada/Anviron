<?php declare(strict_types=1);

namespace App\Integration\KosApi\Dto;

use SimpleXMLElement;

abstract class AbstractKosApiDto
{
    abstract public static function fromXml(SimpleXMLElement $xml): ?AbstractKosApiDto;
}