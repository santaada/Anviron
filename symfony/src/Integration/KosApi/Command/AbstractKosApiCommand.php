<?php declare(strict_types=1);

namespace App\Integration\KosApi\Command;

use App\Integration\KosApi\Dto\AbstractKosApiDto;
use App\Integration\OAuth\Provider\ZuulOaasProvider;
use Generator;
use League\OAuth2\Client\Token\AccessTokenInterface;


abstract class AbstractKosApiCommand
{
    protected static string $KOS_API_ENDPOINT = 'https://kosapi.fit.cvut.cz/api/3';
    
    protected static int $PAGE_SIZE = 200;


    /**
     * @return Generator<AbstractKosApiDto>
     */
    abstract public function process(
        ZuulOaasProvider $provider, 
        AccessTokenInterface $token, 
    ): Generator;
}
