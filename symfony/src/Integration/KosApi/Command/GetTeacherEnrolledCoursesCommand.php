<?php declare(strict_types=1);

namespace App\Integration\KosApi\Command;

use App\Integration\KosApi\Dto\TeacherEnrolledCourseDto;
use App\Integration\OAuth\Provider\ZuulOaasProvider;
use Generator;
use GuzzleHttp\Exception\ClientException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use SimpleXMLElement;


final class GetTeacherEnrolledCoursesCommand extends AbstractKosApiCommand
{
    public function __construct(
        private string $username,
    ) {}
    
    public function process(ZuulOaasProvider $provider, AccessTokenInterface $token): Generator
    {
        foreach ( $this->getTeacherEnrolledCoursesRaw($provider, $token) as $teacherEnrolledCourseDto) {
            assert( $teacherEnrolledCourseDto instanceof SimpleXMLElement );
            $parsed = TeacherEnrolledCourseDto::fromXml($teacherEnrolledCourseDto);
            if ($parsed === null) continue;
            yield $parsed;
        }
    }

    private function getTeacherEnrolledCoursesRaw(ZuulOaasProvider $provider, AccessTokenInterface $token): Generator
    {
        $initUrl = self::$KOS_API_ENDPOINT . '/teachers/' . $this->username . '/courses';
        $pageSize = parent::$PAGE_SIZE;
        $pageIterCnt = 0;
        while (true) {
            $req = $provider->getAuthenticatedRequest( // TODO What if token is expired
                'GET',
                $initUrl . '?sem=curr&offset=' . $pageSize * $pageIterCnt . '&limit=' . $pageSize,
                $token,
                [],
            );

            try {
                $res = $provider->getResponse($req);
            } catch (ClientException $exception) {
                break; // TODO Log warning? Flash message?
            }
            
            $resContents = $res->getBody()->getContents();
            $resElem = new SimpleXMLElement($resContents);
            $resEntries = $resElem->xpath('atom:entry');

            if ( ! is_array($resEntries) || count($resEntries) === 0) {
                break;
            }

            foreach ($resEntries as $resEntry) {
                $resEntry->registerXPathNamespace('ns', 'http://kosapi.feld.cvut.cz/schema/3'); // TODO Load default NS from xml
                yield $resEntry;
            }

            $pageIterCnt++;
        }
    }
}