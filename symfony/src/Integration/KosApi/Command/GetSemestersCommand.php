<?php declare(strict_types=1);

namespace App\Integration\KosApi\Command;

use App\Integration\KosApi\Dto\SemesterDto;
use App\Integration\OAuth\Provider\ZuulOaasProvider;
use Generator;
use League\OAuth2\Client\Token\AccessTokenInterface;
use SimpleXMLElement;


final class GetSemestersCommand extends AbstractKosApiCommand
{
    public function process(ZuulOaasProvider $provider, AccessTokenInterface $token): Generator
    {
        foreach ( $this->getSemestersRaw($provider, $token) as $semester ) {
            assert( $semester instanceof SimpleXMLElement );
            $parsed = SemesterDto::fromXml($semester);
            if ($parsed === null) continue; // TODO Log warnings if parsing failed? 
            yield $parsed;
        }
    }
    
    private function getSemestersRaw(ZuulOaasProvider $provider, AccessTokenInterface $token): Generator
    {
        $initUrl = self::$KOS_API_ENDPOINT . '/semesters';
        $pageSize = parent::$PAGE_SIZE;
        $pageIterCnt = 0;
        while (true) {
            $req = $provider->getAuthenticatedRequest( // TODO What if token is expired
                'GET',
                $initUrl . '?offset=' . $pageSize * $pageIterCnt . '&limit=' . $pageSize,
                $token,
                [],
            );

            $res = $provider->getResponse($req)->getBody()->getContents();
            
            $resElem = new SimpleXMLElement($res);
            $resEntries = $resElem->xpath('atom:entry');

            if ( ! is_array($resEntries) || count($resEntries) === 0) {
                break;
            }

            foreach ($resEntries as $resEntry) {
                $resEntry->registerXPathNamespace('ns', 'http://kosapi.feld.cvut.cz/schema/3'); // TODO Load default NS from xml
                yield $resEntry;
            }

            $pageIterCnt++;
        }
    }
}