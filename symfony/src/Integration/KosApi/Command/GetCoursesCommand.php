<?php declare(strict_types=1);

namespace App\Integration\KosApi\Command;

use App\Integration\KosApi\Dto\CourseDto;
use App\Integration\OAuth\Provider\ZuulOaasProvider;
use Generator;
use League\OAuth2\Client\Token\AccessTokenInterface;
use SimpleXMLElement;
use Nette\Utils\Strings;


final class GetCoursesCommand extends AbstractKosApiCommand
{
    public function process(ZuulOaasProvider $provider, AccessTokenInterface $token): Generator
    {
        foreach ( $this->getCoursesRaw($provider, $token) as $course ) {
            assert( $course instanceof SimpleXMLElement );
            $parsed = CourseDto::fromXml($course);
            if ($parsed === null) continue; // TODO Log warnings if parsing failed?
            if ( // Only include FIT related subjects
                ! Strings::startsWith($parsed->code, 'BI-')
                && ! Strings::startsWith($parsed->code, 'FI-')
                && ! Strings::startsWith($parsed->code, 'MI-')
                && ! Strings::startsWith($parsed->code, 'NI-')
            ) continue;
            yield $parsed;
        }
    }
    
    private function getCoursesRaw(ZuulOaasProvider $provider, AccessTokenInterface $token): Generator
    {
        $initUrl = self::$KOS_API_ENDPOINT . '/courses?sem=current';
        $pageSize = parent::$PAGE_SIZE;
        $pageIterCnt = 0;
        while (true) {
            $req = $provider->getAuthenticatedRequest( // TODO What if token is expired
                'GET',
                $initUrl . '&offset=' . $pageSize * $pageIterCnt . '&limit=' . $pageSize,
                $token,
                [],
            );

            $res = $provider->getResponse($req)->getBody()->getContents();
            $resElem = new SimpleXMLElement($res);
            $resEntries = $resElem->xpath('atom:entry');

            if ( ! is_array($resEntries) || count($resEntries) === 0) {
                break;
            }

            foreach ($resEntries as $resEntry) {
                $resEntry->registerXPathNamespace('ns', 'http://kosapi.feld.cvut.cz/schema/3'); // TODO Load default NS from xml
                yield $resEntry;
            }

            $pageIterCnt++;
        }
    }
}