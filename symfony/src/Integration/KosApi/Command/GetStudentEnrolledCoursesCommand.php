<?php declare(strict_types=1);

namespace App\Integration\KosApi\Command;

use App\Integration\KosApi\Dto\StudentEnrolledCourseDto;
use App\Integration\OAuth\Provider\ZuulOaasProvider;
use Generator;
use League\OAuth2\Client\Token\AccessTokenInterface;
use SimpleXMLElement;


final class GetStudentEnrolledCoursesCommand extends AbstractKosApiCommand
{
    public function __construct(
        private string $username,
    ) {}
    
    /**
     * @return Generator<StudentEnrolledCourseDto>
     */
    public function process(ZuulOaasProvider $provider, AccessTokenInterface $token): Generator
    {
        foreach ( $this->getStudentEnrolledCoursesRaw($provider, $token) as $studentEnrolledCourseDto) {
            assert( $studentEnrolledCourseDto instanceof SimpleXMLElement );
            $parsed = StudentEnrolledCourseDto::fromXml($studentEnrolledCourseDto);
            if ($parsed === null) continue;
            yield $parsed;
        }
    }

    private function getStudentEnrolledCoursesRaw(ZuulOaasProvider $provider, AccessTokenInterface $token): Generator
    {
        $initUrl = self::$KOS_API_ENDPOINT . '/students/' . $this->username . '/enrolledCourses';
        $pageSize = parent::$PAGE_SIZE;
        $pageIterCnt = 0;
        while (true) {
            $req = $provider->getAuthenticatedRequest( // TODO What if token is expired
                'GET',
                $initUrl . '?offset=' . $pageSize * $pageIterCnt . '&limit=' . $pageSize,
                $token,
                [],
            );

            $res = $provider->getResponse($req)->getBody()->getContents();
            $resElem = new SimpleXMLElement($res);
            $resEntries = $resElem->xpath('atom:entry');

            if ( ! is_array($resEntries) || count($resEntries) === 0) {
                break;
            }

            foreach ($resEntries as $resEntry) {
                $resEntry->registerXPathNamespace('ns', 'http://kosapi.feld.cvut.cz/schema/3'); // TODO Load default NS from xml
                yield $resEntry;
            }

            $pageIterCnt++;
        }
    }
}