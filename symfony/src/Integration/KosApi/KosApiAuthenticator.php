<?php declare(strict_types=1);

namespace App\Integration\KosApi;

use App\Integration\OAuth\Provider\ZuulOaasProvider;
use App\Integration\OAuth\Provider\ZuulOaasProviderFactory;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessTokenInterface;


class KosApiAuthenticator
{
    private readonly ZuulOaasProvider $provider;
    private readonly ?AccessTokenInterface $token;
    
    public function __construct(
        private readonly ZuulOaasProviderFactory $providerFactory,
    ) {
        $this->provider = $this->providerFactory->createForServiceAccount();
        try {
            $token = $this->provider->getAccessToken('client_credentials');
        } catch (IdentityProviderException $exception) {
            $token = null;
        }
        $this->token = $token;
    }
    
    public function getToken(): ?AccessTokenInterface
    {
        return $this->token;
    }
}