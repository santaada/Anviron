<?php declare(strict_types=1);

namespace App\Integration\UsermapApi;

use App\Integration\OAuth\Provider\ZuulOaasProvider;
use App\Integration\OAuth\Provider\ZuulOaasProviderFactory;
use App\Integration\OAuth\Session\ZuulOaasSessionManagerFactory;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class UsermapApiIntegrationFactory
{
    private ZuulOaasProvider $provider;
    
    public function __construct(
        private readonly ZuulOaasProviderFactory $providerFactory,
        private readonly ZuulOaasSessionManagerFactory $sessionManagerFactory,
        private readonly string $apiEndpoint,
    ){
        $this->provider = $this->providerFactory->create();
    }

    public function create(SessionInterface $session): UsermapApiInterface
    {
        $sessionManager = $this->sessionManagerFactory->create($session);
        
        return new UsermapApiIntegration(
            provider: $this->provider,
            sessionManager: $sessionManager,
            apiEndpoint: $this->apiEndpoint,
        );
    }
}