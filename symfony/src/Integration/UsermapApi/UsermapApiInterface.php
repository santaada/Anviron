<?php declare(strict_types=1);

namespace App\Integration\UsermapApi;

interface UsermapApiInterface
{
    /**
     * @return array{firstName: string, lastName: string}|null
     */
    public function getPeople(string $username): ?array;
}