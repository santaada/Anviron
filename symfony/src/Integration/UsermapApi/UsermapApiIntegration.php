<?php declare(strict_types=1);

namespace App\Integration\UsermapApi;

use App\Integration\OAuth\Provider\ZuulOaasProvider;
use App\Integration\OAuth\Session\ZuulOaasSessionManager;
use Nette\Utils\Json;


final class UsermapApiIntegration implements UsermapApiInterface
{
    public function __construct(
        private readonly ZuulOaasProvider $provider,
        private readonly ZuulOaasSessionManager $sessionManager,
        private readonly string $apiEndpoint,
    ){}


    /**
     * @return array{firstName: string, lastName: string}|null
     */
    public function getPeople(string $username): ?array
    {
        $token = $this->sessionManager->getToken();
        if ($token === null) return null;
        
        $req = $this->provider->getAuthenticatedRequest(
            'GET',
            $this->apiEndpoint . '/people/' . $username,
            $token,
            [],
        );
        
        /** @var array{firstName: string, lastName: string} $res */
        $res = Json::decode($this->provider->getResponse($req)->getBody()->getContents(), Json::FORCE_ARRAY);
        return [
            'firstName' => strval($res['firstName']),
            'lastName' => strval($res['lastName']),
        ];
    }    
}