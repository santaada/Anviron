<?php declare(strict_types=1);

namespace App\Integration\DockerApi\Parts;


class ImageApi extends \Swagger\Client\Api\ImageApi
{

    /**
     * @return array<mixed> of http client options
     */
    protected function createHttpClientOption(): array
    {
        return [
            'curl' => [
                CURLOPT_UNIX_SOCKET_PATH => '/var/run/docker.sock'
            ], 
            ... parent::createHttpClientOption()
        ];
    }
}