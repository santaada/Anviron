<?php declare(strict_types=1);

namespace App\Integration\DockerApi\Parts;


class ContainerApi extends \Swagger\Client\Api\ContainerApi
{
    /**
     * @return array<mixed> of http client options
     */
    protected function createHttpClientOption(): array
    {
        return [
            'curl' => [
                CURLOPT_UNIX_SOCKET_PATH => '/var/run/docker.sock'
            ], 
            ... parent::createHttpClientOption()
        ];
    }
}