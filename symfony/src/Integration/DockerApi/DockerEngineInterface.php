<?php declare(strict_types=1);

namespace App\Integration\DockerApi;

use App\Entity\EnvironmentRuntime;


interface DockerEngineInterface
{
    public function imageBuild(string $tarPath, string $imageName, string $imageTag): void;

    public function containerCreate(EnvironmentRuntime $runtime, string $passwordEnvValue, string $passwordEnvKey): void;

    public function containerStart(EnvironmentRuntime $runtime): void;

    public function containerStop(EnvironmentRuntime $runtime): void;

    public function containerDelete(EnvironmentRuntime $runtime): void;

    public function containerExec(EnvironmentRuntime $runtime): void;
}