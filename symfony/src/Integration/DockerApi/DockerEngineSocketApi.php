<?php declare(strict_types=1);

namespace App\Integration\DockerApi;

use App\Entity\EnvironmentRuntime;
use App\Service\EnvironmentFactoryVolumeService;
use App\Integration\DockerApi\Parts\ContainerApi;
use App\Integration\DockerApi\Parts\ImageApi;
use App\Integration\DockerApi\Parts\ExecApi;
use Exception;
use Nette\Utils\Strings;
use Swagger\Client\Model\ExecConfig;
use Swagger\Client\Model\ExecStartConfig;
use Symfony\Component\Uid\Uuid;


class DockerEngineSocketApi implements DockerEngineInterface
{
    public function __construct(
        protected EnvironmentFactoryVolumeService $factoryVolumeOperation,
        protected ImageApi                        $imageApi,
        protected ContainerApi                    $containerApi,
        protected ExecApi                         $execApi,
    ) {}
    
    private function getRuntimeNameEscaped(EnvironmentRuntime $runtime): string
    {
        $runtimeId = $runtime->getId();
        assert($runtimeId instanceof Uuid);
        return Strings::truncate($runtimeId->toRfc4122(), 255);
    }

    public function imageBuild(string $tarPath, string $imageName, string $imageTag): void
    {
        $inputStream = fopen($tarPath, "r+");
        $inputStreamContents = is_resource($inputStream) ? (stream_get_contents($inputStream) ?: '') : '';
        $dockerfile = "/Dockerfile";
        try {
            $this->imageApi->imageBuild(
                input_stream: $inputStreamContents,
                dockerfile: $dockerfile,
                t: $imageName . ":" . $imageTag,
                // extrahosts: $extrahosts, 
                // remote: $remote, 
                // q: $q, 
                // nocache: $nocache, 
                // cachefrom: $cachefrom, 
                // pull: $pull, 
                // rm: $rm, 
                // forcerm: $forcerm, 
                // memory: $memory, 
                // memswap: $memswap, 
                // cpushares: $cpushares, 
                // cpusetcpus: $cpusetcpus, 
                // cpuperiod: $cpuperiod, 
                // cpuquota: $cpuquota, 
                // buildargs: $buildargs, 
                // shmsize: $shmsize, 
                // squash: $squash, 
                // labels: $labels, 
                // networkmode: $networkmode, 
                // content_type: $content_type, 
                // x_registry_config: $x_registry_config, 
                // platform: $platform, 
                // target: $target, 
                // outputs: $outputs,
            );
        } catch (Exception $e) {
            echo 'Exception when calling ImageApi->imageBuild: ', $e->getMessage(), PHP_EOL;
        }
    }

    public function containerCreate(EnvironmentRuntime $runtime, string $passwordEnvValue, string $passwordEnvKey): void
    {
        $name = $this->getRuntimeNameEscaped($runtime);

        $volumes = [];
        $binds = [];
        foreach ($runtime->getEnvironmentFactory()->getEnvironmentFactoryVolumes() as $volume) {
            $physical = $this->factoryVolumeOperation->getOrCreatePhysicalVolume($volume, $runtime->getUsedBy(), true);
            $containerized = $volume->getContainerPath();
            $volumes[$containerized] = (object)[];
            $binds[] = $physical . ":" . $containerized;
        }
        
        $exposedPorts = [];
        $portBindings = [];
        foreach ($runtime->getRuntimePorts() as $runtimePort) {
            $externalPort = $runtimePort->getExternalPort();
            $internalPort = $runtimePort->getFactoryPort()?->getExternalPort();
            assert( is_int($internalPort) && is_int($externalPort) );
            $str = $internalPort . "/" . ($runtimePort->getFactoryPort()?->getConnectionType() ?? 'tcp');
            $exposedPorts[$str] = (object)[];
            $portBindings[$str] = [(object)['HostPort' => strval($externalPort)]];
        }
        
        $build = $runtime->getEnvironmentFactory()->getEnvironmentImageBuild();
        
        $container = (object)[
            'Env' => [
              $passwordEnvKey . '=' . $passwordEnvValue,  
            ],
            'Image' =>
                ($build->getEnvironmentImage()?->getName() ?? "")
                . ":"
                . $build->getTag(),
            'Volumes' => (object)$volumes,
            'ExposedPorts' => (object)$exposedPorts,
            'HostConfig' => (object)[
                'Binds' => $binds,
                'PortBindings' => (object)$portBindings,
            ],
        ];
        
        $this->containerApi->containerCreate($container, $name);
    }
    
    public function containerStart(EnvironmentRuntime $runtime): void
    {
        $name = $this->getRuntimeNameEscaped($runtime);
        $this->containerApi->containerStart($name);
    }
    
    public function containerStop(EnvironmentRuntime $runtime): void
    {
        $name = $this->getRuntimeNameEscaped($runtime);
        $this->containerApi->containerStop($name);
    }

    public function containerDelete(EnvironmentRuntime $runtime): void
    {
        $name = $this->getRuntimeNameEscaped($runtime);
        $this->containerApi->containerDelete($name);
    }
    
    public function containerExec(EnvironmentRuntime $runtime): void
    {
        $name = $this->getRuntimeNameEscaped($runtime);
        $idResponse = $this->execApi->containerExec(new ExecConfig([
            "attach_stdout" => true,
            "attach_stderr" => true,
            "tty" => true,
            "privileged" => true,
            'cmd' => ["/bin/bash", "-c", "/home/.anviron/exec.sh >/home/.anviron/exec.out 2>/home/.anviron/exec.err"],
        ]), $name);
        
        $this->execApi->execStart($idResponse->getId(), new ExecStartConfig([
            'detach' => false,
            'tty' => true,
        ]));
    }
}