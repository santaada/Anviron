<?php declare(strict_types=1);

namespace App\Command\KosApi;

use App\Repository\UserRepository;
use App\Service\CourseMemberService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


final class KosApiImportCourseMembersCommand extends Command
{
    public function __construct(
        private readonly UserRepository         $userRepository,
        private readonly EntityManagerInterface $manager,
        private readonly CourseMemberService    $courseMemberService,
    ) {
        parent::__construct();
    }


    protected function configure(): void
    {
        $this->setName('app:kos-api:import-course-members');
        $this->setDescription('Imports the specified entity using KOS API service');
    }

    /**
     * Syncs data from KOS API with app's database. 
     * 
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $bar = new ProgressBar($output);

        $users = $this->userRepository->findAll();
        // Terms / Semesters
        foreach ($users as $user){
            $this->courseMemberService->syncUserWithKosApi($user);
            $bar->advance();
        }
        
        $this->manager->flush();
        $bar->finish();        
        
        $io->success('Import successful');

        return Command::SUCCESS;
    }
}
