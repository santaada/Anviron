<?php declare(strict_types=1);

namespace App\Command\KosApi;

use App\Entity\Course;
use App\Repository\CourseRepository;
use App\Repository\SubjectRepository;
use App\Repository\TermRepository;
use App\Integration\KosApi\Command\GetCourseInstancesCommand;
use App\Integration\KosApi\Dto\CourseInstanceDto;
use App\Integration\KosApi\KosApiIntegration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


final class KosApiImportCoursesCommand extends Command
{
    public function __construct(
        private readonly KosApiIntegration $kosApi,
        private readonly CourseRepository           $courseRepository,
        private readonly TermRepository             $termRepository,
        private readonly SubjectRepository          $subjectRepository,
        private readonly EntityManagerInterface     $manager,
    ) {
        parent::__construct();
    }


    protected function configure(): void
    {
        $this->setName('app:kos-api:import-courses');
        $this->setDescription('Imports the specified entity using KOS API service');
    }

    /**
     * Syncs data from KOS API with app's database. 
     * 
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $bar = new ProgressBar($output);

        $subjects = $this->subjectRepository->findAll();

        // Terms / Semesters
        foreach ($subjects as $subject){
            foreach ($this->kosApi->get(new GetCourseInstancesCommand($subject->getCode())) as $courseDto) {
                assert($courseDto instanceof CourseInstanceDto);
                $term = $this->termRepository->findOneBy(['code' => $courseDto->termCode]);

                if ($term === null) continue; // Or maybe create? 

                $course = $this->courseRepository->findOneBy([
                    'term' => $term,
                    'subject' => $subject,
                ]);
                // Create if non-existent
                if ( $course === null ){
                    $course = new Course();
                    $course->setSubject($subject);
                    $course->setTerm($term);
                    $this->manager->persist($course);
                }
                $bar->advance();
            }
        }
        $this->manager->flush();
        $bar->finish();        
        
        $io->success('Import successful');

        return Command::SUCCESS;
    }
}
