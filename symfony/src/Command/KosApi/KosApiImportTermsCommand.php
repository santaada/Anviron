<?php declare(strict_types=1);

namespace App\Command\KosApi;

use App\Entity\Term;
use App\Repository\TermRepository;
use App\Integration\KosApi\Command\GetSemestersCommand;
use App\Integration\KosApi\Dto\SemesterDto;
use App\Integration\KosApi\KosApiIntegration;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class KosApiImportTermsCommand extends Command
{
    public function __construct(
        private readonly KosApiIntegration      $kosApi,
        private readonly TermRepository         $termRepository,
        private readonly EntityManagerInterface $manager,
    ) {
        parent::__construct();
    }


    protected function configure(): void
    {
        $this->setName('app:kos-api:import-terms');
        $this->setDescription('Imports the specified entity using KOS API service');
    }

    /**
     * Syncs data from KOS API with app's database. 
     * 
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $bar = new ProgressBar($output);

        foreach ($this->kosApi->get( new GetSemestersCommand() ) as $semester){
            assert($semester instanceof SemesterDto);
            $term = $this->termRepository->findOneBy(['code' => $semester->code]) ?? new Term();
            $term->setCode($semester->code);
            $term->setStartsAt(DateTimeImmutable::createFromMutable($semester->startsAt));
            $term->setEndsAt(DateTimeImmutable::createFromMutable($semester->endsAt));
            $this->manager->persist($term);
            $bar->advance();
        }
        $this->manager->flush();
        $bar->finish();
        
        $io->success('Import successful');

        return Command::SUCCESS;
    }
}
