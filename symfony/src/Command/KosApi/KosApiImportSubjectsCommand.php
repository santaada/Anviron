<?php declare(strict_types=1);

namespace App\Command\KosApi;

use App\Entity\Subject;
use App\Repository\SubjectRepository;
use App\Integration\KosApi\Command\GetCoursesCommand;
use App\Integration\KosApi\Dto\CourseDto;
use App\Integration\KosApi\KosApiIntegration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


final class KosApiImportSubjectsCommand extends Command
{
    public function __construct(
        private readonly KosApiIntegration      $kosApi,
        private readonly SubjectRepository      $subjectRepository,
        private readonly EntityManagerInterface $manager,
    ) {
        parent::__construct();
    }


    protected function configure(): void
    {
        $this->setName('app:kos-api:import-subjects');
        $this->setDescription('Imports the specified entity using KOS API service');
    }

    /**
     * Syncs data from KOS API with app's database. 
     * 
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $bar = new ProgressBar($output);
        
        // Terms / Semesters
        foreach ($this->kosApi->get( new GetCoursesCommand() ) as $courseDto){
            assert($courseDto instanceof CourseDto);
            $subject = $this->subjectRepository->findOneBy(['code' => $courseDto->code]) ?? new Subject();
            $subject->setName($courseDto->title);
            $subject->setCode($courseDto->code);
            $this->manager->persist($subject);
            $bar->advance();
        }
        $this->manager->flush();
        $bar->finish();        
        
        $io->success('Import successful');

        return Command::SUCCESS;
    }
}
