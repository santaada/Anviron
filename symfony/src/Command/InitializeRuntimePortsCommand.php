<?php declare(strict_types=1);

namespace App\Command;

use App\Entity\EnvironmentRuntimePort;
use App\Repository\EnvironmentRuntimePortRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class InitializeRuntimePortsCommand extends Command
{
    public function __construct(
        private readonly EnvironmentRuntimePortRepository $portRepository,
        private readonly EntityManagerInterface $manager,
        private readonly LoggerInterface $logger,
    )
    {
        parent::__construct();
    }


    protected function configure(): void
    {
        $this
            ->setName('app:initialize:runtime-ports')
            ->setDescription('Initializes ports in range 49152–65535 which are used as a resource by the app')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Overwrites if port exists')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $bar = new ProgressBar($output);
        
        for ($i = 49152; $i <= 49500; $i++) { // TODO Add more ports and make sure it doesn't exhaust RAM
            $existingPort = $this->portRepository->findOneBy(['externalPort' => $i]);
            if ($existingPort) {
                if ( ! $input->getOption('force') ) {
                    continue;
                }
                
                if ($existingPort->getEnvironmentRuntime() !== null) {
                    $this->logger->error("InitializeRuntimePortsCommand: Cannot remove allocated EnvironmentRuntimePort instance", [
                        'id' => $existingPort->getId(),
                        'runtimeId' => $existingPort->getEnvironmentRuntime()->getId(),
                        'externalPort' => $existingPort->getExternalPort(),
                    ]);
                    continue;
                }
                
                $this->logger->info("InitializeRuntimePortsCommand: Removing EnvironmentRuntimePort instance", [
                    'id' => $existingPort->getId(),
                    'externalPort' => $existingPort->getExternalPort(),
                ]);
                $this->manager->remove($existingPort);
                $this->manager->flush();
            }
            $port = new EnvironmentRuntimePort();
            $port->setExternalPort($i);
            $this->manager->persist($port);
            $bar->advance();
        }
        
        $bar->finish();
        $this->manager->flush();
        $io->success('Ports initialized.');

        return Command::SUCCESS;
    }
}
