<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\EnvironmentImageBuild;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EnvironmentImageBuild|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnvironmentImageBuild|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnvironmentImageBuild[]    findAll()
 * @method EnvironmentImageBuild[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * 
 * @extends ServiceEntityRepository<EnvironmentImageBuild>
 */
class EnvironmentImageBuildRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EnvironmentImageBuild::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(EnvironmentImageBuild $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(EnvironmentImageBuild $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return EnvironmentImageBuild[] Returns an array of EnvironmentImageBuild objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EnvironmentImageBuild
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
