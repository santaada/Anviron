<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\EnvironmentFactoryPort;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EnvironmentFactoryPort|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnvironmentFactoryPort|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnvironmentFactoryPort[]    findAll()
 * @method EnvironmentFactoryPort[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * 
 * @extends ServiceEntityRepository<EnvironmentFactoryPort>
 */
class EnvironmentFactoryPortRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EnvironmentFactoryPort::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(EnvironmentFactoryPort $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(EnvironmentFactoryPort $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return EnvironmentFactoryPort[] Returns an array of EnvironmentFactoryPort objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EnvironmentFactoryPort
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
