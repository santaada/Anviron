<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\CourseMemberService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * Recieves an authorized request (= with Passport issued by ZuulOaasAuthenticator) after the user passes login.
 * Please note that this logic cannot be executed directly in the Login controller, as the controller lacks the
 * Passport.
 */
final class SyncCourseMembersController extends AbstractController
{
    public function __construct(
        private readonly CourseMemberService $courseMemberService,
        private readonly UserRepository $userRepository,
        private readonly UrlGeneratorInterface $urlGenerator,
    ) {}
    
    #[Route('/login/sync-roles', name: 'app_login_sync_roles')]
    public function index(Request $request): Response
    {
        $successResponseUrl = $request->query->get('onSuccess') ?? $this->urlGenerator->generate('homepage');
        $failureResponseUrl = $request->query->get('onFailure') ?? $this->urlGenerator->generate('homepage');
        assert( is_string($successResponseUrl) && is_string($failureResponseUrl));
        
        try {
            // Refresh user's course-related roles
            assert($this->getUser() instanceof UserInterface);
            $user = $this->userRepository->findOneBy(['username' => $this->getUser()->getUserIdentifier()]);
            assert($user instanceof User);
            $this->courseMemberService->syncUserWithKosApi($user);
        } catch (\Exception $exception) {
            return new RedirectResponse($failureResponseUrl);
        }
        
        return new RedirectResponse($successResponseUrl);
    }
}