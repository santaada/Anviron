<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;


final class LoginFormController extends AbstractController implements AuthenticationEntryPointInterface
{    
    #[Route('/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('@EasyAdmin/page/login.html.twig', [
            'error' => $error,
            'translation_domain' => 'admin',
            'page_title' => '<h1>Anviron</h1>',
            'action' => $this->generateUrl('app_login_oauth_fit'),
            'sign_in_label' => 'Log in via SSO',
        ]);
    }


    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return new RedirectResponse($this->generateUrl('app_login'));
    }
}