<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


final class LogoutController extends AbstractController
{    
    #[Route('/logout', name: 'app_logout')]
    public function logout(Request $request): void
    {
        $request->getSession()->clear();
        $this->addFlash('success', 'You were logged out successfuly!');
    }
}