<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\ConnectionType;
use App\Entity\EnvironmentFactoryPort;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Security\Authorization\Voter\EnvironmentFactoryPortVoter;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;


final class EnvironmentFactoryPortCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {}


    public static function getEntityFqcn(): string
    {
        return EnvironmentFactoryPort::class;
    }
    
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->renderContentMaximized()
            ->showEntityActionsInlined()
            ->setEntityLabelInSingular("Port requirement")
            ->setEntityLabelInPlural("Port requirements")
            ->setEntityPermission(EnvironmentFactoryPortVoter::READ)
        ;
    }
    
    public function configureActions(Actions $actions): Actions
    {
        return Actions::new()
            ->add(Crud::PAGE_INDEX, Action::NEW)
            ->add(Crud::PAGE_INDEX, Action::EDIT)
            ->add(Crud::PAGE_INDEX, Action::DELETE)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->add(Crud::PAGE_EDIT, Action::DELETE)
            ->add(Crud::PAGE_NEW, Action::SAVE_AND_RETURN)
            ->add(Crud::PAGE_NEW, Action::SAVE_AND_CONTINUE)
            
            ->setPermission(Action::NEW, EnvironmentFactoryPortVoter::CREATE)
            ->setPermission(Action::EDIT, EnvironmentFactoryPortVoter::UPDATE)
            ->setPermission(Action::SAVE_AND_RETURN, EnvironmentFactoryPortVoter::UPDATE)
            ->setPermission(Action::SAVE_AND_CONTINUE, EnvironmentFactoryPortVoter::UPDATE)
            ->setPermission(Action::DELETE, EnvironmentFactoryPortVoter::DELETE)
            ;
    }


    public function configureFields(string $pageName): iterable
    {
        $userInstance = $this->userRepository->findOneBy([
            'username' => $this->getUser()?->getUserIdentifier(),
        ]);
        assert($userInstance instanceof User);
        
        $factoryField = AssociationField::new('environmentFactory', 'Environment');
        // Not a system admin => cannot see every Env
        if (!$this->isGranted(UserRoles::ROLE_SYSTEM_ADMIN->value)) {
            $factoryField->setQueryBuilder(
                function (QueryBuilder $queryBuilder) use ($userInstance) {
                    return $queryBuilder
                        ->where('entity.maintainedBy = :maintainedBy')
                        ->setParameter('maintainedBy', $userInstance);
                }
            );
        }
        $connTypeOpts = array_combine(
            array_column(ConnectionType::cases(), 'name'),
            array_column(ConnectionType::cases(), 'value'),
        );
        return [
            $factoryField,
            NumberField::new('externalPort', 'Port'),
            BooleanField::new('isEntryPort', 'Entry port'),
            ChoiceField::new('connectionType', 'Connection type')
                ->setChoices($connTypeOpts)
        ];
    }
}
