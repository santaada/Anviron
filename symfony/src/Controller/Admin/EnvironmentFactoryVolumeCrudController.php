<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\EnvironmentFactoryVolume;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Security\Authorization\Voter\EnvironmentFactoryVolumeVoter;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;


final class EnvironmentFactoryVolumeCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {}

    public static function getEntityFqcn(): string
    {
        return EnvironmentFactoryVolume::class;
    }
    
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->renderContentMaximized()
            ->showEntityActionsInlined()
            ->setEntityLabelInSingular("Volume requirement")
            ->setEntityLabelInPlural("Volume requirements")
            ->setEntityPermission(EnvironmentFactoryVolumeVoter::READ)
        ;
    }
    
    public function configureActions(Actions $actions): Actions
    {
        return Actions::new()
            ->add(Crud::PAGE_INDEX, Action::NEW)
            ->add(Crud::PAGE_INDEX, Action::EDIT)
            ->add(Crud::PAGE_INDEX, Action::DELETE)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->add(Crud::PAGE_EDIT, Action::DELETE)
            ->add(Crud::PAGE_NEW, Action::SAVE_AND_RETURN)
            ->add(Crud::PAGE_NEW, Action::SAVE_AND_CONTINUE)

            ->setPermission(Action::NEW, EnvironmentFactoryVolumeVoter::CREATE)
            ->setPermission(Action::EDIT, EnvironmentFactoryVolumeVoter::UPDATE)
            ->setPermission(Action::SAVE_AND_RETURN, EnvironmentFactoryVolumeVoter::UPDATE)
            ->setPermission(Action::SAVE_AND_CONTINUE, EnvironmentFactoryVolumeVoter::UPDATE)
            ->setPermission(Action::DELETE, EnvironmentFactoryVolumeVoter::DELETE)
            ;
    }


    public function configureFields(string $pageName): iterable
    {
        $userInstance = $this->userRepository->findOneBy([
            'username' => $this->getUser()?->getUserIdentifier(),
        ]);
        assert($userInstance instanceof User);

        $factoryField = AssociationField::new('environmentFactory', 'Environment');
        // Not a system admin => cannot see every Env
        if (!$this->isGranted(UserRoles::ROLE_SYSTEM_ADMIN->value)) {
            $factoryField->setQueryBuilder(
                function (QueryBuilder $queryBuilder) use ($userInstance) {
                    return $queryBuilder
                        ->where('entity.maintainedBy = :maintainedBy')
                        ->setParameter('maintainedBy', $userInstance);
                }
            );
        }
        
        return [
            $factoryField,
            TextField::new('containerPath', 'Container path'),
        ];
    }
}
