<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\EnvironmentImage;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Security\Authorization\Voter\EnvironmentImageVoter;
use App\Service\EnvironmentImageBuildService;
use App\Service\EnvironmentImageService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Nette\Utils\Strings;
use Symfony\Component\HttpFoundation\RedirectResponse;


final class EnvironmentImageCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly AdminUrlGenerator             $adminUrlGenerator,
        private readonly UserRepository                $userRepository,
        private readonly EnvironmentImageBuildService  $imageBuildService,
        private readonly EnvironmentImageService       $imageService,
    ) {}

    public static function getEntityFqcn(): string
    {
        return EnvironmentImage::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->renderContentMaximized()
            ->showEntityActionsInlined()
            ->setEntityLabelInSingular("Environment Image")
            ->setEntityLabelInPlural("Environment Images");
    }

    public function configureActions(Actions $actions): Actions
    {
        $buildAction = Action::new('createBuild', 'Build')
            ->setIcon('fas fa-hammer')
            ->setCssClass('btn btn-success')
            ->linkToCrudAction('createBuildAction');
        
        return Actions::new()
            ->add(Crud::PAGE_INDEX, Action::NEW)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_INDEX, Action::EDIT)
            ->add(Crud::PAGE_INDEX, Action::DELETE)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->add(Crud::PAGE_EDIT, $buildAction)
            ->add(Crud::PAGE_EDIT, Action::DELETE)
            ->add(Crud::PAGE_NEW, Action::SAVE_AND_RETURN)
            ->add(Crud::PAGE_NEW, Action::SAVE_AND_CONTINUE)

            ->setPermission(Action::NEW, UserRoles::ROLE_SYSTEM_ADMIN->value)
            ->setPermission(Action::DETAIL, EnvironmentImageVoter::READ)
            ->setPermission(Action::EDIT, EnvironmentImageVoter::UPDATE)
            ->setPermission(Action::DELETE, UserRoles::ROLE_SYSTEM_ADMIN->value)
            ->setPermission(Action::SAVE_AND_CONTINUE, EnvironmentImageVoter::UPDATE)
            ->setPermission(Action::SAVE_AND_RETURN, EnvironmentImageVoter::UPDATE)
            ->setPermission('createBuild', UserRoles::ROLE_SYSTEM_ADMIN->value);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideWhenCreating()
                ->hideWhenUpdating()
                ->hideOnIndex(),
            
            TextField::new('name', "Name"),
            CodeEditorField::new('dockerfile', "Dockerfile")
                ->setLanguage('dockerfile')
                ->setNumOfRows(25)
                ->hideOnIndex(), 

            AssociationField::new('environmentImageBuilds', 'Builds')
                ->setDisabled(),
        ];
    }
    
    public function createBuildAction(AdminContext $context): RedirectResponse
    {
        $image = $context->getEntity()->getInstance();
        assert($image instanceof EnvironmentImage);
        $user = $this->userRepository->findOneBy(['username' => $this->getUser()?->getUserIdentifier()]);
        assert($user instanceof User);

        $redirectUrl = $this->adminUrlGenerator
            ->setController(self::class)
            ->setAction(Action::EDIT)
            ->setEntityId($image->getId())
            ->generateUrl();
        
        // Check if build state is valid - name must not contain spaces
        if (Strings::contains($image->getName(), ' ') || Strings::contains($image->getName(), ':')) {
            $this->addFlash('warning', 'Environment image name must not contain spaces and ":"');
            return $this->redirect($redirectUrl);
        }

        $build = $this->imageBuildService->createFromImage($image, $user);
        $this->imageBuildService->dumpDockerfileAndBuild($build);

        $this->addFlash('success', 'Environment image build succeeded');
        return $this->redirect($redirectUrl);
    }

    /**
     * @param EnvironmentImage $entityInstance
     */
    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if ( ! $this->imageService->canBeDeleted($entityInstance)){
            $this->addFlash(
                'warning',
                'This EnvironmentImage cannot be deleted. 
                Please make sure you delete all related EnvironmentImageBuild instances first.'
            );
            return;
        }
        // Deletion allowed
        parent::deleteEntity($entityManager, $entityInstance);
    }
}
