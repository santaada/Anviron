<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\EnvironmentRuntime;
use App\Entity\User;
use App\Security\Authorization\Voter\EnvironmentRuntimeVoter;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntime\Shutdown\EnvironmentRuntimeShutdownChain;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\NullFilter;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\RedirectResponse;


final class EnvironmentRuntimeCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly AdminUrlGenerator $adminUrlGenerator,
        private readonly EnvironmentRuntimeShutdownChain $shutdownChain,
        private readonly UserRepository $userRepository,
    ) {}

    public static function getEntityFqcn(): string
    {
        return EnvironmentRuntime::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->renderContentMaximized()
            ->showEntityActionsInlined()
            ->setEntityLabelInSingular("Environment Runtime")
            ->setEntityLabelInPlural("Environment Runtimes")
            ->setDefaultSort(['startedUpAt' => 'DESC'])
            ->setEntityPermission(EnvironmentRuntimeVoter::READ);
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions = Actions::new()
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_INDEX, Action::DELETE)
            ->add(Crud::PAGE_DETAIL, Action::new('openEnvironmentAction', 'Go to Environment')
                ->setIcon('fas fa-arrow-right')
                ->setCssClass('action-shutdownAction btn btn-success')
                ->linkToCrudAction('openEnvironmentAction')
                ->displayIf(static function (EnvironmentRuntime $runtime) {
                    return $runtime->getShutDownExecutedAt() === null;
                })
            )
            ->add(Crud::PAGE_DETAIL, Action::new('shutdownAction', 'Shutdown')
                ->setIcon('fas fa-power-off')
                ->setCssClass('action-shutdownAction btn btn-warning')
                ->linkToCrudAction('shutdownAction')
                ->displayIf(static function (EnvironmentRuntime $runtime) {
                    return $runtime->getShutDownExecutedAt() === null;
                })
            )
            ->add(Crud::PAGE_DETAIL, Action::new('goToEnvironmentFactoryAction', 'Environment Details')
                ->setIcon('fas fa-terminal')
                ->setCssClass('action-shutdownAction btn')
                ->linkToCrudAction('goToEnvironmentFactoryAction')
            )
            ->add(Crud::PAGE_DETAIL, Action::DELETE)
            ->setPermission(Action::DETAIL, EnvironmentRuntimeVoter::READ)
            ->setPermission(Action::DELETE, EnvironmentRuntimeVoter::DELETE)
            ->setPermission('openEnvironmentAction', EnvironmentRuntimeVoter::CONTROL)
            ->setPermission('shutdownAction', EnvironmentRuntimeVoter::CONTROL)
            ;
        return $actions;
    }
    
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(
                NullFilter::new('shutDownExecutedAt', 'Running')
                    ->setChoiceLabels('Live runtimes', 'Past runtimes')
            );
    }
    
    public function configureFields(string $pageName): iterable
    {
        $runtime = $this->getContext()?->getEntity()->getInstance();
        assert($runtime instanceof EnvironmentRuntime || $runtime === null);
        
        yield FormField::addTab('Dashboard', 'fas fa-building');
        yield AssociationField::new('environmentFactory', 'Environment')
                ->setDisabled();
        yield AssociationField::new('usedBy', 'User')
            ->setDisabled();
        yield UrlField::new('url', 'URL')
                ->setDisabled()
                ->hideOnIndex();
        yield UrlField::new('password', 'Entry password')
            ->setDisabled()
            ->hideOnIndex();
        yield DateTimeField::new('startedUpAt', 'Started up at')
                ->setDisabled();
        yield DateTimeField::new('shutDownScheduledAt', 'Shutdown scheduled at')
                ->setDisabled();
        yield DateTimeField::new('shutDownExecutedAt', 'Shutdown executed at')
                ->setDisabled();
        
        yield FormField::addTab('Ports', 'fas fa-door-open');
        yield AssociationField::new('runtimePorts')
                ->setDisabled()
                ->hideOnIndex();
        
        yield FormField::addTab('Startup log', 'fas fa-arrow-up');
        yield CodeEditorField::new('buildStdout', "Standard output")
            ->setNumOfRows(50)
            ->hideOnIndex();
        yield CodeEditorField::new('buildStderr', "Standard error output")
            ->setNumOfRows(50)
            ->hideOnIndex();
        
        if ($runtime && $runtime->getShutDownExecutedAt() !== null) {
            yield FormField::addTab('Shutdown log', 'fas fa-arrow-down');
            yield CodeEditorField::new('shutdownStdout', "Standard output")
                ->hideOnIndex();
            yield CodeEditorField::new('shutdownStderr', "Standard error output")
                ->hideOnIndex();
        }
    }

    // Live runtimes should not be deleted
    /**
     * @param EnvironmentRuntime $entityInstance
     */
    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if ( $entityInstance->getShutDownExecutedAt() === null){
            $this->addFlash(
                'warning',
                'This EnvironmentRuntime cannot be deleted. Please make sure it is shut down first.'
            );
            return;
        }
        if ( $entityInstance->getRuntimePorts()->count() !== 0){
            $this->addFlash(
                'warning',
                'This EnvironmentRuntime cannot be deleted. Please make sure it frees allocated ports first.'
            );
            return;
        }
        // Deletion allowed
        parent::deleteEntity($entityManager, $entityInstance);
    }

    public function shutdownAction(AdminContext $context): RedirectResponse
    {
        $runtime = $context->getEntity()->getInstance();

        $username = $this->getUser()?->getUserIdentifier();
        assert( is_string($username) );
        $user = $this->userRepository->findOneBy(['username' => $username]);
        assert( $user instanceof User );
        
        $result = $this->shutdownChain->start(new ChainOfResponsibilityRequest($user, $runtime->getEnvironmentFactory(), $runtime));

        if ($result->state === ChainOfResponsibilityResponse::FAIL) {
            $this->addFlash('warning', 'Unable to stop the environment runtime. ' . $result->message);
        } else {
            $this->addFlash('success', 'The environment runtime has been shut down successfully. ' . $result->message);
        }

        $redirectUrl = $this->adminUrlGenerator
            ->setController(self::class)
            ->setAction(Action::EDIT)
            ->setEntityId($runtime->getId())
            ->generateUrl();
        
        return $this->redirect($redirectUrl);
    }

    public function goToEnvironmentFactoryAction(AdminContext $context): RedirectResponse
    {
        $runtime = $context->getEntity()->getInstance();
        
        assert($runtime instanceof EnvironmentRuntime);

        $redirectUrl = $this->adminUrlGenerator
            ->setController(EnvironmentFactoryCrudController::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($runtime->getEnvironmentFactory()->getId())
            ->generateUrl();

        return $this->redirect($redirectUrl);
    }

    public function openEnvironmentAction(AdminContext $context): RedirectResponse
    {
        $runtime = $context->getEntity()->getInstance();
        assert($runtime instanceof EnvironmentRuntime);
        assert(is_string($runtime->getUrl()));
        return $this->redirect($runtime->getUrl());
    }
}
