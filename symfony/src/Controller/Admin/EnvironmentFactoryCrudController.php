<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentRuntime;
use App\Entity\User;
use App\Security\Authorization\UserRoles;
use App\Security\Authorization\Voter\EnvironmentFactoryVoter;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntime\Startup\EnvironmentRuntimeStartupChain;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\RedirectResponse;


final class EnvironmentFactoryCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly AdminUrlGenerator $adminUrlGenerator,
        private readonly UserRepository $userRepository,
        private readonly EnvironmentRuntimeStartupChain $startupChain,
    ) {}

    public static function getEntityFqcn(): string
    {
        return EnvironmentFactory::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->renderContentMaximized()
            ->showEntityActionsInlined()
            ->setEntityLabelInSingular("Environment")
            ->setEntityLabelInPlural("Environments")
            ->setEntityPermission(EnvironmentFactoryVoter::READ);
    }

    public function configureActions(Actions $actions): Actions
    {
        $runAction = Action::new('runAction', 'Run')
            ->setIcon('fas fa-fish')
            ->setCssClass('btn btn-success')
            ->linkToCrudAction('runAction');
        
        $actions = Actions::new()
            ->add(Crud::PAGE_INDEX, Action::NEW)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_INDEX, Action::EDIT)
            ->add(Crud::PAGE_INDEX, Action::DELETE)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->add(Crud::PAGE_EDIT, $runAction)
            ->add(Crud::PAGE_EDIT, Action::DELETE)
            ->add(Crud::PAGE_DETAIL, $runAction)
            ->add(Crud::PAGE_DETAIL, Action::DELETE)
            ->add(Crud::PAGE_NEW, Action::SAVE_AND_RETURN)
            ->add(Crud::PAGE_NEW, Action::SAVE_AND_CONTINUE)
            
            ->setPermission(Action::NEW, EnvironmentFactoryVoter::CREATE)
            ->setPermission(Action::DETAIL, EnvironmentFactoryVoter::READ)
            ->setPermission(Action::EDIT, EnvironmentFactoryVoter::UPDATE)
            ->setPermission(Action::DELETE, EnvironmentFactoryVoter::DELETE)
            ->setPermission(Action::SAVE_AND_RETURN, EnvironmentFactoryVoter::UPSERT)
            ->setPermission(Action::SAVE_AND_CONTINUE, EnvironmentFactoryVoter::UPSERT)
            ->setPermission('runAction', EnvironmentFactoryVoter::RUN)
            ;
        
        return $actions;
    }
    
    public function configureFields(string $pageName): iterable
    {
        $username = $this->getUser()?->getUserIdentifier();
        assert(is_string($username));
        
        $maintainedByField = AssociationField::new('maintainedBy', 'Maintainer');
        if ( ! $this->isGranted(UserRoles::ROLE_SYSTEM_ADMIN->value)) {
            $maintainedByField->setQueryBuilder(function (QueryBuilder $queryBuilder) use ($username) {
                return $queryBuilder
                    ->where('entity.username = :maintainedByUsername')
                    ->setParameter('maintainedByUsername', $username);
            });
        }
        
        yield FormField::addTab('Main configuration', 'fas fa-gear');
        
        yield FormField::addPanel('Metadata');
        yield TextField::new('name', "Name");
        if ($pageName !== Crud::PAGE_DETAIL) {
            yield AssociationField::new('environmentImageBuild', "Base image build")
                ->setHelp('Changing the base image build can result into environment malfunction. Please test the environment functionality after your change.'); 
        }
        yield $maintainedByField;            

        if ($pageName !== Crud::PAGE_DETAIL) {
            yield FormField::addPanel('Courses');
            yield AssociationField::new('courses')
                ->hideOnIndex();
            yield BooleanField::new('isVisibleForCourseStudents', "Visible for students")
                ->renderAsSwitch()
                ->hideOnIndex();
        }

        yield FormField::addTab('User instructions', 'fas fa-book');
        yield CodeEditorField::new('userDocs', "User instructions")
            ->setLanguage('markdown')
            ->setNumOfRows(25)
            ->hideOnIndex();

        if ($pageName !== Crud::PAGE_DETAIL) {
            yield FormField::addTab('Startup script', 'fas fa-arrow-up');
            yield CodeEditorField::new('startupScript', "Startup shell script")
                ->setHelp('Notice: The script will be executed on every Environment startup. Running time-consuming operations, which are not runtime-specific is highly discouraged. Please, consider running those in the Image setup or request a new Image from the system administrator.')
                ->setLanguage('shell')
                ->setNumOfRows(25)
                ->hideOnIndex();

            yield FormField::addTab('Shutdown script', 'fas fa-arrow-down');
            yield CodeEditorField::new('shutdownScript', "Shutdown shell script")
                ->setHelp('Notice: The script will be executed on every Environment shutdown. Running time-consuming operations, which are not runtime-specific is highly discouraged. Please, consider running those in the Image setup or request a new Image from the system administrator.')
                ->setLanguage('shell')
                ->setNumOfRows(25)
                ->hideOnIndex();

            yield FormField::addTab('Port requirements', 'fas fa-door-open');
            yield AssociationField::new('environmentFactoryPorts', 'Port needs')
                ->setDisabled()
                ->hideOnIndex();

            yield FormField::addTab('Volume requirements', 'fas fa-database');
            yield AssociationField::new('environmentFactoryVolumes', 'Volume needs')
                ->setDisabled()
                ->hideOnIndex();
        }            
    }
    
    /**
     * @param EnvironmentFactory $entityInstance
     */
    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if ( $entityInstance->getEnvironmentFactoryPorts()->count() !== 0 ||
            $entityInstance->getEnvironmentFactoryVolumes()->count() !== 0 ||
            $entityInstance->getEnvironmentRuntimes()->count() !== 0
        ){
            $this->addFlash(
                'warning',
                'This EnvironmentFactory cannot be deleted. 
                Please make sure it has no port / volume requirements and no runtimes.'
            );
            return;
        }
        // Deletion allowed
        parent::deleteEntity($entityManager, $entityInstance);
    }

    public function runAction(AdminContext $context): RedirectResponse
    {
        $factory = $context->getEntity()->getInstance();
        
        $username = $this->getUser()?->getUserIdentifier();
        assert( is_string($username) );
        $user = $this->userRepository->findOneBy(['username' => $username]);
        assert( $user instanceof User );

        $result = $this->startupChain->start(new ChainOfResponsibilityRequest($user, $factory));
        
        if ($result->state === ChainOfResponsibilityResponse::FAIL) {
            $this->addFlash('warning', 'Unable to start the environment runtime. ' . $result->message);
            $redirectUrl = $this->adminUrlGenerator
                ->setController(self::class)
                ->setAction(Action::DETAIL)
                ->setEntityId($factory->getId())
                ->generateUrl();
        } else {
            $this->addFlash('success', 'The environment runtime has been launched successfully. ' . $result->message);
            assert( $result->request->runtime instanceof EnvironmentRuntime );
            $redirectUrl = $this->adminUrlGenerator
                ->setController(EnvironmentRuntimeCrudController::class)
                ->setAction(Action::DETAIL)
                ->setEntityId($result->request->runtime->getId())
                ->generateUrl();
        }
        
        return $this->redirect($redirectUrl);
    }
}
