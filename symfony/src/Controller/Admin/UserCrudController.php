<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\User;
use App\Security\Authorization\UserRoles;
use App\Security\Authorization\Voter\UserVoter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;


final class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }
    
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->renderContentMaximized()
            ->showEntityActionsInlined()
            ->setEntityLabelInSingular("User")
            ->setEntityLabelInPlural("Users")
            ->setEntityPermission(UserVoter::READ);
    }
    
    public function configureActions(Actions $actions): Actions
    {
        $actions = Actions::new()
            ->add(Crud::PAGE_INDEX, Action::EDIT)
            ->add(Crud::PAGE_INDEX, Action::DELETE)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ;
        
        $actions
            ->setPermission(Action::EDIT, UserVoter::UPDATE_DETAIL)
            ->setPermission(Action::DELETE, UserVoter::DELETE);
        
        return $actions;
    }

    /**
     * @param Entity $entityInstance
     */
    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        return; // TODO
    }
    
    public function configureFields(string $pageName): iterable
    {
        $allowedRoles = [UserRoles::ROLE_USER->value, UserRoles::ROLE_SYSTEM_ADMIN->value];
        
        yield TextField::new('username')
            ->setDisabled();
        
        yield TextField::new('titleBeforeName');
        
        yield TextField::new('firstName')
            ->setDisabled();
        
        yield TextField::new('lastName')
            ->setDisabled();
        
        yield TextField::new('titleAfterName');
        
        if ($this->isGranted(UserVoter::UPDATE_ROLES, $this->getContext()?->getUser())) {
            yield ChoiceField::new('roles')
                ->setChoices(array_combine($allowedRoles, $allowedRoles))
                ->allowMultipleChoices()
                ->renderAsBadges([
                    UserRoles::ROLE_USER->value => 'success',
                    UserRoles::ROLE_SYSTEM_ADMIN->value => 'warning'
                ]);
        }
    }
}
