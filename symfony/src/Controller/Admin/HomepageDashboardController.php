<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Course;
use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentFactoryPort;
use App\Entity\EnvironmentFactoryVolume;
use App\Entity\EnvironmentImage;
use App\Entity\EnvironmentImageBuild;
use App\Entity\EnvironmentRuntime;
use App\Entity\Subject;
use App\Entity\Term;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Service\CourseMemberService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;


final class HomepageDashboardController extends AbstractDashboardController
{
    public function __construct(
        protected UserRepository $userRepository,
        protected CourseMemberService $courseMemberService,
    ) {}


    #[Route('/', name: 'homepage')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Anviron');
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        // Usually it's better to call the parent method because that gives you a
        // user menu with some menu items already created ("sign out", "exit impersonation", etc.)
        // if you prefer to create the user menu from scratch, use: return UserMenu::new()->...
        
        assert( $user instanceof User );
        
        return parent::configureUserMenu($user)
            // use the given $user object to get the user name
            ->setName($user->getFirstName() . ' ' . $user->getLastName())
            ->displayUserAvatar(false);
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        
        // Section only visible for users with at least one teacher role
        assert($this->getUser() instanceof UserInterface);
        $user = $this->userRepository->findOneBy([
            'username' => $this->getUser()->getUserIdentifier(),
        ]);
        assert($user instanceof User);
        $isUserOrTeacher = $this->courseMemberService->hasAnyTeacherRole($user) 
            || $this->isGranted(UserRoles::ROLE_SYSTEM_ADMIN->value);
        if ($isUserOrTeacher) {
            yield MenuItem::section('Administration', 'fas fa-solid fa-person-military-pointing');
            yield MenuItem::linkToCrud('Images', 'fas fa-puzzle-piece', EnvironmentImage::class);
            yield MenuItem::linkToCrud('Image Builds', 'fas fa-cog', EnvironmentImageBuild::class);
        }
        
        yield MenuItem::section('My Environments', 'fas fa-cube');
        yield MenuItem::linkToCrud('Environments', 'fas fa-terminal', EnvironmentFactory::class);
        if ($isUserOrTeacher) {
            yield MenuItem::linkToCrud('Port Requirements', 'fas fa-door-open', 
                EnvironmentFactoryPort::class);
            yield MenuItem::linkToCrud('Volume Requirements', 'fas fa-database', 
                EnvironmentFactoryVolume::class);
        }
        
        yield MenuItem::section('Runtime', 'fas fa-network-wired');
        yield MenuItem::linkToCrud('Environment Runtimes', 'fas fa-folder-open', 
            EnvironmentRuntime::class);
        
        yield MenuItem::section('KOS', 'fas fa-crow');
        yield MenuItem::linkToCrud('Users', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Courses', 'fas fa-book', Course::class);
        yield MenuItem::linkToCrud('Terms', 'fas fa-calendar', Term::class);
        yield MenuItem::linkToCrud('Subjects', 'fas fa-book-open', Subject::class);
    }
}
