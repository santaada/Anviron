<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Term;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field;


final class TermCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Term::class;
    }
    
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->renderContentMaximized()
            ->showEntityActionsInlined()
            ->setEntityLabelInSingular("Term")
            ->setEntityLabelInPlural("Terms");
    }
    
    public function configureActions(Actions $actions): Actions
    {
        return Actions::new(); 
    }

    /**
     * @param Entity $entityInstance
     */
    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        return;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            Field\TextField::new('code'),
            Field\DateField::new('startsAt'),
            Field\DateField::new('endsAt'),
        ];
    }
}
