<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Course;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field;


final class CourseCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Course::class;
    }
    
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->renderContentMaximized()
            ->showEntityActionsInlined()
            ->setEntityLabelInSingular("Course")
            ->setEntityLabelInPlural("Courses");
    }
    
    public function configureActions(Actions $actions): Actions
    {
        return Actions::new(); 
    }


    /**
     * @param Entity $entityInstance
     */
    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        return;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            Field\AssociationField::new('term', 'Term'),
            Field\AssociationField::new('subject', 'Subject'),
        ];
    }
}
