<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\EnvironmentImageBuild;
use App\Security\Authorization\UserRoles;
use App\Security\Authorization\Voter\EnvironmentImageBuildVoter;
use App\Service\EnvironmentImageBuildService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;


final class EnvironmentImageBuildCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly EnvironmentImageBuildService $imageBuildService,
    ) {}


    public static function getEntityFqcn(): string
    {
        return EnvironmentImageBuild::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->renderContentMaximized()
            ->showEntityActionsInlined()
            ->setEntityLabelInSingular("Environment Image Build")
            ->setEntityLabelInPlural("Environment Image Builds");
    }

    public function configureActions(Actions $actions): Actions
    {
        return Actions::new()
            ->add(Crud::PAGE_INDEX, Action::EDIT)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_INDEX, Action::DELETE)
            
            ->setPermission(Action::EDIT, UserRoles::ROLE_SYSTEM_ADMIN->value)
            ->setPermission(Action::DETAIL, EnvironmentImageBuildVoter::READ)
            ->setPermission(Action::DELETE, UserRoles::ROLE_SYSTEM_ADMIN->value);
    }

    /**
     * @param EnvironmentImageBuild $entityInstance
     */
    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if ( ! $this->imageBuildService->canBeDeleted($entityInstance)){
            $this->addFlash(
                'warning', 
                'This EnvironmentImageBuild cannot be deleted. 
                Please make sure you unlink it from all EnvironmentFactory instances first.'
            );
            return;
        }
        // Deletion allowed
        parent::deleteEntity($entityManager, $entityInstance);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideWhenCreating()
                ->hideWhenUpdating()
                ->hideOnIndex(),

            AssociationField::new('environmentImage', 'Environment image')
                ->setDisabled(),
            
            TextField::new('tag', "Tag"),
            
            CodeEditorField::new('dockerfile', 'Dockerfile')
                ->hideOnIndex()
                ->setLanguage('dockerfile')
                ->setNumOfRows(25),

            DateTimeField::new('createdAt', 'Created at')
                ->hideOnIndex()
                ->setDisabled(),

            AssociationField::new('createdBy', 'Created by')
                ->setDisabled(),
        ];
    }
}
