<?php declare(strict_types=1);

namespace App\Controller;

use App\Integration\OAuth\Enum\AccessTokenState;
use App\Integration\OAuth\Provider\ZuulOaasProvider;
use App\Integration\OAuth\Provider\ZuulOaasProviderFactory;
use App\Integration\OAuth\Session\ZuulOaasSessionManagerFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


final class ZuulOaasController extends AbstractController
{    
    private readonly ZuulOaasProvider $provider;

    public function __construct(
        private readonly ZuulOaasProviderFactory $providerFactory,
        private readonly ZuulOaasSessionManagerFactory $sessionManagerFactory,
        private readonly UrlGeneratorInterface $urlGenerator,
    ) {
        $this->provider = $this->providerFactory->create();
    }

    #[Route('/login/oauth/fit', name: 'app_login_oauth_fit')]
    public function index(Request $request): Response 
    {
        $sessionManager = $this->sessionManagerFactory->create($request->getSession());
        $authState = $sessionManager->checkAccessToken();
        
        $successResponseUrl = $request->query->get('onSuccess') ?? $this->urlGenerator->generate('homepage');
        $failureResponseUrl = $request->query->get('onFailure') ?? $this->urlGenerator->generate('homepage');
        
        assert( is_string($successResponseUrl) && is_string($failureResponseUrl) );
        
        // Already signed in
        if ($authState === AccessTokenState::VALID->value) {
            return new RedirectResponse($successResponseUrl);
        }
        
        $code = $request->query->get('code');
        $state = $request->query->get('state');
        
        if ( ! is_string($code) ) {
            // Redirect to OAuth server
            $authUrl = $this->provider->getAuthorizationUrl();
            $sessionManager->setState($this->provider->getState());
            return new RedirectResponse($authUrl);
        } else if ( is_string($state) && ! $sessionManager->checkState($state) ) {
            // Callback from OAuth server: State mismatch (CSRF protection)
            $sessionManager->clearState();
            return new RedirectResponse($failureResponseUrl);
        } else {
            if ($authState === AccessTokenState::REFRESH_REQIRED->value) {
                // Callback from OAuth server: Successful refresh
                $accessToken = $this->provider->getAccessToken('refresh_token', [
                    'refresh_token' => $sessionManager->getToken()?->getRefreshToken(),
                ]);

                /** @var array{access_token: string, refresh_token: string, expires: string} $tokenSerialized */
                $tokenSerialized = $accessToken->jsonSerialize();
                $sessionManager->setToken($tokenSerialized);

                return new RedirectResponse($successResponseUrl);
            } else {
                // Callback from OAuth server: Successful acquiration
                $accessToken = $this->provider->getAccessToken('authorization_code', [
                    'code' => $code,
                ]);

                /** @var array{access_token: string, refresh_token: string, expires: string} $tokenSerialized */
                $tokenSerialized = $accessToken->jsonSerialize();
                $sessionManager->setToken($tokenSerialized);
                
                // Refresh user's course-related roles
                return new RedirectResponse($this->urlGenerator->generate('app_login_sync_roles', [
                    'onSuccess' => $successResponseUrl,
                    'onFailure' => $failureResponseUrl,
                ]));
            }            
        }
    }
}