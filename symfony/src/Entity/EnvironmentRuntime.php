<?php declare(strict_types=1);

namespace App\Entity;

use App\Repository\EnvironmentRuntimeRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: EnvironmentRuntimeRepository::class)]
class EnvironmentRuntime extends AbstractEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: "doctrine.uuid_generator")]
    private Uuid $id;

    #[ORM\Column(type: 'string', length: 2047)]
    private string $url;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $startedUpAt;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $shutDownScheduledAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $shutDownExecutedAt;

    #[ORM\ManyToOne(targetEntity: EnvironmentFactory::class, inversedBy: 'environmentRuntimes')]
    #[ORM\JoinColumn(nullable: false)]
    private EnvironmentFactory $environmentFactory;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'environmentRuntimes')]
    #[ORM\JoinColumn(nullable: false)]
    private User $usedBy;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $buildStdout;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $buildStderr;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $shutdownStdout;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $shutdownStderr;

    /**
     * @var Collection<int, EnvironmentRuntimePort> $runtimePorts
     */
    #[ORM\OneToMany(mappedBy: 'environmentRuntime', targetEntity: EnvironmentRuntimePort::class)]
    private Collection $runtimePorts;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $password;

    public function __construct()
    {
        $this->runtimePorts = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getStartedUpAt(): ?DateTimeImmutable
    {
        return $this->startedUpAt;
    }

    public function setStartedUpAt(DateTimeImmutable $startedUpAt): self
    {
        $this->startedUpAt = $startedUpAt;

        return $this;
    }

    public function getShutDownScheduledAt(): ?DateTimeImmutable
    {
        return $this->shutDownScheduledAt;
    }

    public function setShutDownScheduledAt(DateTimeImmutable $shutDownScheduledAt): self
    {
        $this->shutDownScheduledAt = $shutDownScheduledAt;

        return $this;
    }

    public function getShutDownExecutedAt(): ?DateTimeImmutable
    {
        return $this->shutDownExecutedAt;
    }

    public function setShutDownExecutedAt(?DateTimeImmutable $shutDownExecutedAt): self
    {
        $this->shutDownExecutedAt = $shutDownExecutedAt;

        return $this;
    }

    public function getEnvironmentFactory(): EnvironmentFactory
    {
        return $this->environmentFactory;
    }

    public function setEnvironmentFactory(EnvironmentFactory $environmentFactory): self
    {
        $this->environmentFactory = $environmentFactory;

        return $this;
    }

    public function getUsedBy(): User
    {
        return $this->usedBy;
    }

    public function setUsedBy(User $usedBy): self
    {
        $this->usedBy = $usedBy;

        return $this;
    }

    /**
     * @return Collection<int, EnvironmentRuntimePort>
     */
    public function getRuntimePorts(): Collection
    {
        return $this->runtimePorts;
    }

    public function addRuntimePort(EnvironmentRuntimePort $runtimePort): self
    {
        if (!$this->runtimePorts->contains($runtimePort)) {
            $this->runtimePorts[] = $runtimePort;
            $runtimePort->setEnvironmentRuntime($this);
        }

        return $this;
    }

    public function removeRuntimePort(EnvironmentRuntimePort $runtimePort): self
    {
        if ($this->runtimePorts->removeElement($runtimePort)) {
            // set the owning side to null (unless already changed)
            if ($runtimePort->getEnvironmentRuntime() === $this) {
                $runtimePort->setEnvironmentRuntime(null);
            }
        }

        return $this;
    }

    public function getBuildStdout(): ?string
    {
        return $this->buildStdout;
    }
    
    public function setBuildStdout(?string $buildStdout): void
    {
        $this->buildStdout = $buildStdout;
    }
    
    public function getBuildStderr(): ?string
    {
        return $this->buildStderr;
    }
    
    public function setBuildStderr(?string $buildStderr): void
    {
        $this->buildStderr = $buildStderr;
    }
    
    public function getShutdownStdout(): ?string
    {
        return $this->shutdownStdout;
    }
    
    public function setShutdownStdout(?string $shutdownStdout): void
    {
        $this->shutdownStdout = $shutdownStdout;
    }
    
    public function getShutdownStderr(): ?string
    {
        return $this->shutdownStderr;
    }
    
    public function setShutdownStderr(?string $shutdownStderr): void
    {
        $this->shutdownStderr = $shutdownStderr;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
