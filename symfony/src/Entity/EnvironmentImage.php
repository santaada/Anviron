<?php declare(strict_types=1);

namespace App\Entity;

use App\Repository\EnvironmentImageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: EnvironmentImageRepository::class)]
class EnvironmentImage extends AbstractEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: "doctrine.uuid_generator")]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $dockerfile;

    /**
     * @var Collection<int, EnvironmentImageBuild>
     */
    #[ORM\OneToMany(mappedBy: 'environmentImage', targetEntity: EnvironmentImageBuild::class)]
    private Collection $environmentImageBuilds;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    public function __construct()
    {
        $this->environmentImageBuilds = new ArrayCollection();
    }
    
    public function __toString(): string
    {
        return $this->getName();
    }


    public function getId(): ?Uuid
    {
        return $this->id ?? null;
    }

    public function getDockerfile(): ?string
    {
        return $this->dockerfile;
    }

    public function setDockerfile(?string $dockerfile): self
    {
        $this->dockerfile = $dockerfile;

        return $this;
    }

    /**
     * @return Collection<int, EnvironmentImageBuild>
     */
    public function getEnvironmentImageBuilds(): Collection
    {
        return $this->environmentImageBuilds;
    }

    public function addEnvironmentImageBuild(EnvironmentImageBuild $environmentImageBuild): self
    {
        if (!$this->environmentImageBuilds->contains($environmentImageBuild)) {
            $this->environmentImageBuilds[] = $environmentImageBuild;
            $environmentImageBuild->setEnvironmentImage($this);
        }
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
