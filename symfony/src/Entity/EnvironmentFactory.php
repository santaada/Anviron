<?php declare(strict_types=1);

namespace App\Entity;

use App\Repository\EnvironmentFactoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: EnvironmentFactoryRepository::class)]
class EnvironmentFactory extends AbstractEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: "doctrine.uuid_generator")]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $startupScript;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $shutdownScript;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $userDocs;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'environmentFactories')]
    #[ORM\JoinColumn(nullable: false)]
    private User $maintainedBy;

    /**
     * @var Collection<int, Course>
     */
    #[ORM\ManyToMany(targetEntity: Course::class, inversedBy: 'environmentFactories')]
    private Collection $courses;

    #[ORM\Column(type: 'boolean')]
    private bool $isVisibleForCourseStudents;

    /**
     * @var Collection<int, EnvironmentRuntime>
     */
    #[ORM\OneToMany(mappedBy: 'environmentFactory', targetEntity: EnvironmentRuntime::class)]
    private Collection $environmentRuntimes;

    /**
     * @var Collection<int, EnvironmentFactoryPort>
     */
    #[ORM\OneToMany(mappedBy: 'environmentFactory', targetEntity: EnvironmentFactoryPort::class, orphanRemoval: true)]
    private Collection $environmentFactoryPorts;

    #[ORM\ManyToOne(targetEntity: EnvironmentImageBuild::class, inversedBy: 'environmentFactory')]
    #[ORM\JoinColumn(nullable: false)]
    private EnvironmentImageBuild $environmentImageBuild;

    /**
     * @var Collection<int, EnvironmentFactoryVolume>
     */
    #[ORM\OneToMany(mappedBy: 'environmentFactory', targetEntity: EnvironmentFactoryVolume::class, orphanRemoval: true)]
    private Collection $environmentFactoryVolumes;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
        $this->environmentRuntimes = new ArrayCollection();
        $this->environmentFactoryPorts = new ArrayCollection();
        $this->environmentFactoryVolumes = new ArrayCollection();
    }
    
    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getIdAsStringChecked(): string
    {
        assert($this->id instanceof Uuid);
        return $this->id->toRfc4122();
    }

    public function getStartupScript(): ?string
    {
        return $this->startupScript;
    }

    public function setStartupScript(string $startupScript): self
    {
        $this->startupScript = $startupScript;

        return $this;
    }

    public function getShutdownScript(): ?string
    {
        return $this->shutdownScript;
    }

    public function setShutdownScript(string $shutdownScript): self
    {
        $this->shutdownScript = $shutdownScript;

        return $this;
    }

    public function getUserDocs(): ?string
    {
        return $this->userDocs;
    }

    public function setUserDocs(?string $userDocs): self
    {
        $this->userDocs = $userDocs;

        return $this;
    }

    public function getMaintainedBy(): ?User
    {
        return $this->maintainedBy ?? null;
    }

    public function setMaintainedBy(User $maintainedBy): self
    {
        $this->maintainedBy = $maintainedBy;

        return $this;
    }

    /**
     * @return Collection<int, Course>
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        $this->courses->removeElement($course);

        return $this;
    }

    public function getIsVisibleForCourseStudents(): ?bool
    {
        return $this->isVisibleForCourseStudents;
    }

    public function setIsVisibleForCourseStudents(bool $isVisibleForCourseStudents): self
    {
        $this->isVisibleForCourseStudents = $isVisibleForCourseStudents;

        return $this;
    }

    /**
     * @return Collection<int, EnvironmentRuntime>
     */
    public function getEnvironmentRuntimes(): Collection
    {
        return $this->environmentRuntimes;
    }

    public function addEnvironmentRuntime(EnvironmentRuntime $environmentRuntime): self
    {
        if (!$this->environmentRuntimes->contains($environmentRuntime)) {
            $this->environmentRuntimes[] = $environmentRuntime;
            $environmentRuntime->setEnvironmentFactory($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, EnvironmentFactoryPort>
     */
    public function getEnvironmentFactoryPorts(): Collection
    {
        return $this->environmentFactoryPorts;
    }

    public function addEnvironmentFactoryPort(EnvironmentFactoryPort $environmentFactoryPort): self
    {
        if (!$this->environmentFactoryPorts->contains($environmentFactoryPort)) {
            $this->environmentFactoryPorts[] = $environmentFactoryPort;
            $environmentFactoryPort->setEnvironmentFactory($this);
        }

        return $this;
    }

    public function getEnvironmentImageBuild(): EnvironmentImageBuild
    {
        return $this->environmentImageBuild;
    }

    public function setEnvironmentImageBuild(EnvironmentImageBuild $environmentImageBuild): self
    {
        $this->environmentImageBuild = $environmentImageBuild;

        return $this;
    }

    /**
     * @return Collection<int, EnvironmentFactoryVolume>
     */
    public function getEnvironmentFactoryVolumes(): Collection
    {
        return $this->environmentFactoryVolumes;
    }

    public function addEnvironmentFactoryVolume(EnvironmentFactoryVolume $environmentFactoryVolume): self
    {
        if (!$this->environmentFactoryVolumes->contains($environmentFactoryVolume)) {
            $this->environmentFactoryVolumes[] = $environmentFactoryVolume;
            $environmentFactoryVolume->setEnvironmentFactory($this);
        }

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
