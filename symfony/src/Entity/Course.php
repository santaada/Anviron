<?php declare(strict_types=1);

namespace App\Entity;

use App\Repository\CourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: CourseRepository::class)]
class Course extends AbstractEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: "doctrine.uuid_generator")]
    private Uuid $id;

    #[ORM\ManyToOne(targetEntity: Subject::class, inversedBy: 'courses')]
    #[ORM\JoinColumn(nullable: false)]
    private Subject $subject;

    #[ORM\ManyToOne(targetEntity: Term::class, inversedBy: 'courses')]
    #[ORM\JoinColumn(nullable: false)]
    private Term $term;

    /**
     * @var Collection<int, CourseMember>
     */
    #[ORM\OneToMany(mappedBy: 'course', targetEntity: CourseMember::class)]
    private Collection $courseMembers;

    /**
     * @var Collection<int, EnvironmentFactory>
     */
    #[ORM\ManyToMany(targetEntity: EnvironmentFactory::class, mappedBy: 'courses')]
    private Collection $environmentFactories;

    public function __construct()
    {
        $this->courseMembers = new ArrayCollection();
        $this->environmentFactories = new ArrayCollection();
    }
    
    public function __toString()
    {
        return $this->subject->getCode() . " @ " . $this->term->getCode();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getSubject(): ?Subject
    {
        return $this->subject;
    }

    public function setSubject(Subject $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getTerm(): Term
    {
        return $this->term;
    }

    public function setTerm(Term $term): self
    {
        $this->term = $term;

        return $this;
    }

    /**
     * @return Collection<int, CourseMember>
     */
    public function getCourseMembers(): Collection
    {
        return $this->courseMembers;
    }

    public function addCourseMember(CourseMember $courseMember): self
    {
        if (!$this->courseMembers->contains($courseMember)) {
            $this->courseMembers[] = $courseMember;
            $courseMember->setCourse($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, EnvironmentFactory>
     */
    public function getEnvironmentFactories(): Collection
    {
        return $this->environmentFactories;
    }

    public function addEnvironmentFactory(EnvironmentFactory $environmentFactory): self
    {
        if (!$this->environmentFactories->contains($environmentFactory)) {
            $this->environmentFactories[] = $environmentFactory;
            $environmentFactory->addCourse($this);
        }

        return $this;
    }

    public function removeEnvironmentFactory(EnvironmentFactory $environmentFactory): self
    {
        if ($this->environmentFactories->removeElement($environmentFactory)) {
            $environmentFactory->removeCourse($this);
        }

        return $this;
    }
}
