<?php declare(strict_types=1);

namespace App\Entity;

enum ConnectionType: string
{
    case TCP = 'tcp';
    case UDP = 'udp';
    case SCTP = 'sctp';
}