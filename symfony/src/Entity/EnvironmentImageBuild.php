<?php declare(strict_types=1);

namespace App\Entity;

use App\Repository\EnvironmentImageBuildRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: EnvironmentImageBuildRepository::class)]
class EnvironmentImageBuild extends AbstractEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: "doctrine.uuid_generator")]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $dockerfile;
    
    #[ORM\Column(type: 'string', length: 255, unique: true, nullable: false)]
    private string $tag;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $createdAt;
    
    #[ORM\ManyToOne(targetEntity: EnvironmentImage::class, inversedBy: 'environmentImageBuilds')]
    #[ORM\JoinColumn(nullable: false)]
    private EnvironmentImage $environmentImage;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'environmentImageBuilds')]
    #[ORM\JoinColumn(nullable: false)]
    private User $createdBy;
    
    public function __toString(): string
    {
        return ($this->getEnvironmentImage()?->__toString() ?? "") . ":" . $this->getTag();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getDockerfile(): ?string
    {
        return $this->dockerfile;
    }
    
    public function setDockerfile(?string $dockerfile): void
    {
        $this->dockerfile = $dockerfile;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function setTag(string $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    public function getEnvironmentImage(): ?EnvironmentImage
    {
        return $this->environmentImage;
    }

    public function setEnvironmentImage(EnvironmentImage $environmentImage): self
    {
        $this->environmentImage = $environmentImage;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
