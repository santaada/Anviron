<?php declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: "`user`")]
class User extends AbstractEntity implements UserInterface 
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: "doctrine.uuid_generator")]
    private Uuid $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private string $username;

    /**
     * @var string[]
     */
    #[ORM\Column(type: 'json')]
    private array $roles = [];
    
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $titleBeforeName;

    #[ORM\Column(type: 'string', length: 255)]
    private string $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    private string $lastName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $titleAfterName;

    #[ORM\Column(type: 'string', length: 1023)]
    private string $email;

    /**
     * @var Collection<int, CourseMember> 
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: CourseMember::class)]
    private Collection $courseMembers;

    /**
     * @var Collection<int, EnvironmentFactory>
     */
    #[ORM\OneToMany(mappedBy: 'maintainedBy', targetEntity: EnvironmentFactory::class)]
    private Collection $environmentFactories;

    /**
     * @var Collection<int, EnvironmentRuntime>
     */
    #[ORM\OneToMany(mappedBy: 'usedBy', targetEntity: EnvironmentRuntime::class)]
    private Collection $environmentRuntimes;

    /**
     * @var Collection<int, EnvironmentImageBuild>
     */
    #[ORM\OneToMany(mappedBy: 'createdBy', targetEntity: EnvironmentImageBuild::class, orphanRemoval: true)]
    private Collection $environmentImageBuilds;

    public function __construct()
    {
        $this->courseMembers = new ArrayCollection();
        $this->environmentFactories = new ArrayCollection();
        $this->environmentRuntimes = new ArrayCollection();
        $this->environmentImageBuilds = new ArrayCollection();
    }
    
    public function __toString(): string 
    {
        return $this->getFirstName() . " " . $this->getLastName() . " | " . $this->getUsername();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getIdAsStringChecked(): string
    {
        assert($this->id instanceof Uuid);
        return $this->id->toRfc4122();
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }


    /**
     * @param string[] $roles
     */
    public function setRoles(array $roles): self
    {
        $this->roles = array_values($roles);

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, CourseMember>
     */
    public function getCourseMembers(): Collection
    {
        return $this->courseMembers;
    }

    public function addCourseMember(CourseMember $courseMember): self
    {
        if (!$this->courseMembers->contains($courseMember)) {
            $this->courseMembers[] = $courseMember;
            $courseMember->setUser($this);
        }

        return $this;
    }

    public function getTitleBeforeName(): ?string
    {
        return $this->titleBeforeName;
    }

    public function setTitleBeforeName(?string $titleBeforeName): self
    {
        $this->titleBeforeName = $titleBeforeName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getTitleAfterName(): ?string
    {
        return $this->titleAfterName;
    }

    public function setTitleAfterName(?string $titleAfterName): self
    {
        $this->titleAfterName = $titleAfterName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection<int, EnvironmentFactory>
     */
    public function getEnvironmentFactories(): Collection
    {
        return $this->environmentFactories;
    }

    public function addEnvironmentFactory(EnvironmentFactory $environmentFactory): self
    {
        if (!$this->environmentFactories->contains($environmentFactory)) {
            $this->environmentFactories[] = $environmentFactory;
            $environmentFactory->setMaintainedBy($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, EnvironmentRuntime>
     */
    public function getEnvironmentRuntimes(): Collection
    {
        return $this->environmentRuntimes;
    }

    public function addEnvironmentRuntime(EnvironmentRuntime $environmentRuntime): self
    {
        if (!$this->environmentRuntimes->contains($environmentRuntime)) {
            $this->environmentRuntimes[] = $environmentRuntime;
            $environmentRuntime->setUsedBy($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, EnvironmentImageBuild>
     */
    public function getEnvironmentImageBuilds(): Collection
    {
        return $this->environmentImageBuilds;
    }

    public function addEnvironmentImageBuild(EnvironmentImageBuild $environmentImageBuild): self
    {
        if (!$this->environmentImageBuilds->contains($environmentImageBuild)) {
            $this->environmentImageBuilds[] = $environmentImageBuild;
            $environmentImageBuild->setCreatedBy($this);
        }

        return $this;
    }
}
