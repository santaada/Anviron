<?php declare(strict_types=1);

namespace App\Entity\Callbacks;

use App\Entity\EnvironmentFactory;
use App\Service\EnvironmentFactoryService;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;

final class EnvironmentFactoryCallbacks
{
    public function __construct(
        private readonly EnvironmentFactoryService $factoryOperation,
    ) {}


    /**
     * @param LifecycleEventArgs<EntityManager> $args
     */
    public function postPersist(LifecycleEventArgs $args): void
    {
        $factory = $args->getObject();
        if( ! $factory instanceof EnvironmentFactory) {
            return;
        }
        
        $this->factoryOperation->findOrCreateSystemVolumes($factory);
    }
}