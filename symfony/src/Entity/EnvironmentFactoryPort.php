<?php declare(strict_types=1);

namespace App\Entity;

use App\Repository\EnvironmentFactoryPortRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: EnvironmentFactoryPortRepository::class)]
class EnvironmentFactoryPort extends AbstractEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: "doctrine.uuid_generator")]
    private Uuid $id;

    #[ORM\Column(type: 'integer')]
    private int $externalPort;

    #[ORM\Column(type: 'string', length: 255)]
    private string $connectionType;

    #[ORM\ManyToOne(targetEntity: EnvironmentFactory::class, inversedBy: 'environmentFactoryPorts')]
    #[ORM\JoinColumn(nullable: false)]
    private EnvironmentFactory $environmentFactory;

    /**
     * @var Collection<int, EnvironmentRuntimePort>
     */
    #[ORM\OneToMany(mappedBy: 'factoryPort', targetEntity: EnvironmentRuntimePort::class)]
    private Collection $environmentRuntimePorts;

    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => false])]
    private bool $isEntryPort = false;

    public function __construct()
    {
        $this->environmentRuntimePorts = new ArrayCollection();
    }
    
    public function __toString(): string
    {
        return strval($this->externalPort);
    }

    public function getId(): ?Uuid
    {
        return $this->id ?? null;
    }

    public function getExternalPort(): ?int
    {
        return $this->externalPort;
    }

    public function setExternalPort(int $externalPort): self
    {
        $this->externalPort = $externalPort;

        return $this;
    }

    public function getConnectionType(): ?string
    {
        return $this->connectionType;
    }

    public function setConnectionType(string $connectionType): self
    {
        $this->connectionType = $connectionType;

        return $this;
    }

    public function getEnvironmentFactory(): ?EnvironmentFactory
    {
        return $this->environmentFactory ?? null;
    }

    public function setEnvironmentFactory(EnvironmentFactory $environmentFactory): self
    {
        $this->environmentFactory = $environmentFactory;

        return $this;
    }

    /**
     * @return Collection<int, EnvironmentRuntimePort>
     */
    public function getEnvironmentRuntimePorts(): Collection
    {
        return $this->environmentRuntimePorts;
    }

    public function addEnvironmentRuntimePort(EnvironmentRuntimePort $environmentRuntimePort): self
    {
        if (!$this->environmentRuntimePorts->contains($environmentRuntimePort)) {
            $this->environmentRuntimePorts[] = $environmentRuntimePort;
            $environmentRuntimePort->setFactoryPort($this);
        }

        return $this;
    }

    public function removeEnvironmentRuntimePort(EnvironmentRuntimePort $environmentRuntimePort): self
    {
        if ($this->environmentRuntimePorts->removeElement($environmentRuntimePort)) {
            // set the owning side to null (unless already changed)
            if ($environmentRuntimePort->getFactoryPort() === $this) {
                $environmentRuntimePort->setFactoryPort(null);
            }
        }

        return $this;
    }

    public function getIsEntryPort(): ?bool
    {
        return $this->isEntryPort;
    }

    public function setIsEntryPort(bool $isEntryPort): self
    {
        $this->isEntryPort = $isEntryPort;

        return $this;
    }
}
