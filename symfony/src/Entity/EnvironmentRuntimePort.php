<?php declare(strict_types=1);

namespace App\Entity;

use App\Repository\EnvironmentRuntimePortRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: EnvironmentRuntimePortRepository::class)]
class EnvironmentRuntimePort extends AbstractEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: "doctrine.uuid_generator")]
    private Uuid $id;

    #[ORM\Column(type: 'integer', unique: true)]
    private int $externalPort;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $allocatedAt;

    #[ORM\ManyToOne(targetEntity: EnvironmentRuntime::class, inversedBy: 'runtimePorts')]
    private ?EnvironmentRuntime $environmentRuntime;

    #[ORM\ManyToOne(targetEntity: EnvironmentFactoryPort::class, inversedBy: 'environmentRuntimePorts')]
    private ?EnvironmentFactoryPort $factoryPort;

    
    public function __toString(): string
    {
        return $this->getFactoryPort()?->getExternalPort()
            . ":"
            . $this->getExternalPort();
    }
    
    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getExternalPort(): int
    {
        return $this->externalPort;
    }

    public function setExternalPort(int $externalPort): self
    {
        $this->externalPort = $externalPort;

        return $this;
    }

    public function getAllocatedAt(): ?DateTimeImmutable
    {
        return $this->allocatedAt;
    }

    public function setAllocatedAt(?DateTimeImmutable $allocatedAt): self
    {
        $this->allocatedAt = $allocatedAt;

        return $this;
    }

    public function getEnvironmentRuntime(): ?EnvironmentRuntime
    {
        return $this->environmentRuntime;
    }

    public function setEnvironmentRuntime(?EnvironmentRuntime $environmentRuntime): self
    {
        $this->environmentRuntime = $environmentRuntime;

        return $this;
    }

    public function getFactoryPort(): ?EnvironmentFactoryPort
    {
        return $this->factoryPort;
    }

    public function setFactoryPort(?EnvironmentFactoryPort $factoryPort): self
    {
        $this->factoryPort = $factoryPort;

        return $this;
    }
}
