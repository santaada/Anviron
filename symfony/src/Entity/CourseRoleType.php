<?php declare(strict_types=1);

namespace App\Entity;

enum CourseRoleType: string
{
    case STUDENT = 'STUDENT';
    case TEACHER = 'UCITEL';
    case TEACHER_TUTOR = 'UCITEL-CVICICI';
    case TEACHER_TUTOR_LAB = 'UCITEL-CVICICI-LABORATORE';
    case TEACHER_EDITOR = 'UCITEL-EDITOR';
    case TEACHER_GUARANTOR = 'UCITEL-GARANT';
    case TEACHER_LECTURER = 'UCITEL-PREDNASEJICI';
    case TEACHER_EXAMINATOR = 'UCITEL-ZKOUSEJICI';
}