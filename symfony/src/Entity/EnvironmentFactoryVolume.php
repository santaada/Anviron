<?php declare(strict_types=1);

namespace App\Entity;

use App\Repository\EnvironmentFactoryVolumeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: EnvironmentFactoryVolumeRepository::class)]
class EnvironmentFactoryVolume extends AbstractEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: "doctrine.uuid_generator")]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'string', length: 2047)]
    private string $containerPath;

    #[ORM\ManyToOne(targetEntity: EnvironmentFactory::class, inversedBy: 'environmentFactoryVolumes')]
    #[ORM\JoinColumn(nullable: false)]
    private EnvironmentFactory $environmentFactory;

    public function __toString(): string
    {
        return $this->containerPath;
    }
    
    public function getId(): ?Uuid
    {
        return $this->id;
    }
    
    public function getIdAsStringChecked(): string
    {
        assert($this->id instanceof Uuid);
        return $this->id->toRfc4122();
    }

    public function getContainerPath(): ?string
    {
        return $this->containerPath;
    }

    public function setContainerPath(string $containerPath): self
    {
        $this->containerPath = $containerPath;

        return $this;
    }

    public function getEnvironmentFactory(): ?EnvironmentFactory
    {
        return $this->environmentFactory ?? null;
    }

    public function setEnvironmentFactory(EnvironmentFactory $environmentFactory): self
    {
        $this->environmentFactory = $environmentFactory;

        return $this;
    }
}
