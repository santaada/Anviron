<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentFactoryVolume;
use App\Entity\User;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;


class EnvironmentFactoryVolumeService
{
    public function __construct(
        private readonly string $volumeDirPrefixContainer,
        private readonly string $volumeDirPrefixPhysical,
        private readonly string $volumeDir,
    ) {}
    
    public function getOrCreatePhysicalVolume(EnvironmentFactoryVolume $volume, User $user, bool $physicalPath = false): string
    {
        $userPath = $this->getUserVolumesPath($volume, $user, $physicalPath);
        $volumePath = $this->getPhysicalVolumePath($volume, $user, $physicalPath);
        
        if ( ! $physicalPath ) {
            FileSystem::createDir($userPath);
            FileSystem::createDir($volumePath);
        }
        
        return $volumePath;
    }
    
    private function getUserVolumesPath(EnvironmentFactoryVolume $volume, User $user, bool $physicalPath = false): string
    {
        return ($physicalPath ? $this->volumeDirPrefixPhysical : $this->volumeDirPrefixContainer)
            . '/' 
            . $this->volumeDir
            . '/'
            . Strings::webalize($user->getIdAsStringChecked())
            . '_'
            . $user->getUsername();
    }
    
    private function getEnvironmentFactoryVolumesPath(EnvironmentFactoryVolume $volume, User $user, bool $physicalPath = false): string
    {
        $factory = $volume->getEnvironmentFactory();
        assert($factory instanceof EnvironmentFactory);
        return $this->getUserVolumesPath($volume, $user, $physicalPath)
            . '/'
            . $factory->getIdAsStringChecked()
            . '_'
            . Strings::webalize($factory->getName());
    }

    private function getPhysicalVolumePath(EnvironmentFactoryVolume $volume, User $user, bool $physicalPath = false): string
    {
        $res = $this->getEnvironmentFactoryVolumesPath($volume, $user, $physicalPath)
        . '/'
        . Strings::webalize($volume->getIdAsStringChecked());
        return Strings::truncate($res, 254, '-');
    }
}