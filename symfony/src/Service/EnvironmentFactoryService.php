<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentFactoryVolume;
use App\Repository\EnvironmentFactoryVolumeRepository;
use Doctrine\ORM\EntityManagerInterface;


class EnvironmentFactoryService
{
    public function __construct(
        private readonly EntityManagerInterface $manager,
        private readonly EnvironmentFactoryVolumeRepository $factoryVolumeRepository,
        private readonly string $systemVolumeContainerPath,
    ) {}
    
    /**
     * @return iterable<EnvironmentFactoryVolume>
     */
    public function findOrCreateSystemVolumes(EnvironmentFactory $factory): iterable
    {
        $systemVolumes = $this->factoryVolumeRepository->findBy([
            'environmentFactory' => $factory,
            'containerPath' => $this->systemVolumeContainerPath,
        ]);
        
        if ( count($systemVolumes) === 0 ) {
            $volume = new EnvironmentFactoryVolume();
            $volume->setEnvironmentFactory($factory);
            $volume->setContainerPath($this->systemVolumeContainerPath);
            
            $this->manager->persist($volume);
            $this->manager->flush();
            
            $systemVolumes = [$volume];
        }
        
        return $systemVolumes;
    }
}