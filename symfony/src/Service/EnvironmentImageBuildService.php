<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\EnvironmentImage;
use App\Entity\EnvironmentImageBuild;
use App\Entity\User;
use App\Integration\DockerApi\DockerEngineInterface;
use App\Repository\EnvironmentFactoryRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Nette\IOException;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;
use PharData;
use Psr\Log\LoggerInterface;


class EnvironmentImageBuildService
{
    private const DOCKERFILE_NAME = 'Dockerfile';
    
    public function __construct(
        private readonly EnvironmentFactoryRepository $factoryRepository,
        private readonly EntityManagerInterface $manager,
        private readonly LoggerInterface $logger,
        private readonly DockerEngineInterface $dockerEngineApi,
        private readonly string $imageDirPrefix,
        private readonly string $imageDir,
    ) {}
    
    public function createFromImage(EnvironmentImage $image, User $createdBy): EnvironmentImageBuild
    {
        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($createdBy);
        $build->setTag($this->generateTag($image, $createdBy));
        
        $this->manager->persist($build);
        $this->manager->flush();
        
        return $build;
    }
    
    public function dumpDockerfileAndBuild(EnvironmentImageBuild $build): void
    {
        assert($build->getEnvironmentImage() instanceof EnvironmentImage);
        assert( is_string($build->getTag()) );
        
        $this->generateDirectory($build);
        $dockerfilePath = $this->dumpDockerfile($build);
        
        $tarPath = $this->generateTarPath($build);
        $pharData = new PharData($tarPath);
        $pharData->addFile($dockerfilePath ?? "", '/Dockerfile');

        $this->dockerEngineApi->imageBuild($tarPath, $build->getEnvironmentImage()->getName(), $build->getTag());
    }

    private function generateDirectory(EnvironmentImageBuild $build): string
    {
        assert($build->getEnvironmentImage() instanceof EnvironmentImage);
        
        // Make sure the parent dir exists
        $this->getOrCreateDirectory($this->imageDirPrefix . "/" . $this->imageDir);

        // Create
        $imageDirname = $this->imageDirPrefix
            . "/"
            . $this->imageDir
            . '/'
            . $this->generateImageDirectoryName($build->getEnvironmentImage());
        
        $buildDirname = $imageDirname 
            . "/" . 
            $this->generateBuildDirectoryName($build);
        
        // Make sure a dir with returned dirname exists
        $this->getOrCreateDirectory($imageDirname);
        $this->getOrCreateDirectory($buildDirname);

        return $buildDirname;
    }

    /**
     * Generates Dockerfile Tar file and saves it to generated dir
     */
    private function dumpDockerfile(EnvironmentImageBuild $build): ?string
    {
        try {
            $dockerfile = $build->getDockerfile() ?? '';
            FileSystem::write(
                $url = $this->generateDockerfilePath($build),
                $dockerfile,
            );
        } catch (IOException $exception) {
            $this->logger->warning(self::class . ": Could not save Dockerfile to cache folder", [
                'exception' => $exception,
            ]);
            dump($exception);
            return null;
        }
        return $url;
    }

    private function generateImageDirectoryName(EnvironmentImage $image): string
    {
        return Strings::webalize($image->getName())
            . "_"
            . $image->getId();
    }

    private function generateBuildDirectoryName(EnvironmentImageBuild $build): string
    {
        assert($build->getEnvironmentImage() instanceof EnvironmentImage);
        assert(is_string($build->getTag()));
        
        return Strings::webalize($build->getTag())
            . "_"
            . $build->getId();
    }
    
    private function generateDockerfilePath(EnvironmentImageBuild $build): string
    {
        return $this->generateDirectory($build) . "/" . self::DOCKERFILE_NAME;
    }

    public function generateTarPath(EnvironmentImageBuild $build): string
    {
        return $this->generateDirectory($build) . ".tar";
    }

    private function getOrCreateDirectory(string $dirname): void
    {
        FileSystem::createDir($dirname, 0777);
    }
    
    private function generateTag(EnvironmentImage $image, User $createdBy): string
    {
        return $createdBy->getUsername() . "-" . (new DateTimeImmutable('now'))->format('YmdHis');
    }
    
    public function canBeDeleted(EnvironmentImageBuild $imageBuild): bool
    {
        return count($this->factoryRepository->findBy([
            'environmentImageBuild' => $imageBuild
        ])) === 0;
    }
}