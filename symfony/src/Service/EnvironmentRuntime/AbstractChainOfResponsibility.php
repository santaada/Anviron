<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime;


abstract class AbstractChainOfResponsibility
{
    /**
     * @var \App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface[]
     */
    protected array $handlers = [];
    
    public function addHandler(ChainOfResponsibilityHandlerInterface $handler): void
    {
        $this->handlers[] = $handler;
    }
    
    public function start(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        $loopedRequest = $request;
        foreach ($this->handlers as $handler) {
            $result = $handler->handle($loopedRequest);
            if ($result->state === ChainOfResponsibilityResponse::FAIL) {
                $this->unhandleAllPrevHandlers($handler, $result->request);
                return $result;
            } else {
                $loopedRequest = $result->request;
            }
        }
        return new ChainOfResponsibilityResponse(
            $loopedRequest,
            ChainOfResponsibilityResponse::SUCCESS,
        );        
    }
    
    protected function unhandleAllPrevHandlers(ChainOfResponsibilityHandlerInterface $handlerNeedle, ChainOfResponsibilityRequest $request): void
    {
        $found = false;
        $handlerHaystack = array_reverse($this->handlers);
        foreach ($handlerHaystack as $handlerHay) {
            if ($handlerNeedle === $handlerHay) {
                $found = true;
            }
            if ($found) {
                $handlerHay->unhandle($request);
            }
        }
    }
}