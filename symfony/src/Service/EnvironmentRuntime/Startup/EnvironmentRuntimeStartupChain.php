<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Startup;


use App\Service\EnvironmentRuntime\AbstractChainOfResponsibility;
use App\Service\EnvironmentRuntime\Startup\Handler\AllocatePortsHandler;
use App\Service\EnvironmentRuntime\Startup\Handler\AllocateVolumesHandler;
use App\Service\EnvironmentRuntime\Startup\Handler\ContainerCreateHandler;
use App\Service\EnvironmentRuntime\Startup\Handler\CreateRuntimeHandler;
use App\Service\EnvironmentRuntime\Startup\Handler\ExecuteStartupScriptHandler;
use App\Service\EnvironmentRuntime\Startup\Handler\GenerateRuntimePasswordHandler;
use App\Service\EnvironmentRuntime\Startup\Handler\SameFactoryCheckHandler;


final class EnvironmentRuntimeStartupChain extends AbstractChainOfResponsibility
{
    public function __construct(
        private readonly SameFactoryCheckHandler $sameFactoryCheckHandler,
        private readonly CreateRuntimeHandler $createRuntimeHandler,
        private readonly AllocateVolumesHandler $allocateVolumesHandler,
        private readonly AllocatePortsHandler $allocatePortsHandler,
        private readonly ContainerCreateHandler $containerCreateHandler,
        private readonly ExecuteStartupScriptHandler $executeStartupScriptHandler,
        private readonly GenerateRuntimePasswordHandler $generateRuntimePasswordHandler,
    ) {
        $this->addHandler($this->sameFactoryCheckHandler); // Check if the same Factory has no other running env for the given user
        $this->addHandler($this->createRuntimeHandler);
        $this->addHandler($this->allocateVolumesHandler);
        $this->addHandler($this->allocatePortsHandler);
        $this->addHandler($this->generateRuntimePasswordHandler);
        $this->addHandler($this->containerCreateHandler);
        $this->addHandler($this->executeStartupScriptHandler);
    }
}