<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Startup\Handler;

use App\Entity\EnvironmentFactoryVolume;
use App\Entity\EnvironmentRuntime;
use App\Service\EnvironmentFactoryVolumeService;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Repository\EnvironmentFactoryVolumeRepository;
use App\Integration\DockerApi\DockerEngineSocketApi;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Nette\Utils\FileSystem;


final class ExecuteStartupScriptHandler implements ChainOfResponsibilityHandlerInterface
{
    public const SCRIPT_FNAME = 'exec.sh';
    public const STDOUT_FNAME = 'exec.out';
    public const STDERR_FNAME = 'exec.err';
    
    public function __construct(
        private readonly EntityManagerInterface             $manager,
        private readonly DockerEngineSocketApi              $dockerEngineApi,
        private readonly EnvironmentFactoryVolumeRepository $volumeRepository,
        private readonly EnvironmentFactoryVolumeService    $volumeService,
        private readonly string                             $systemVolumeContainerPath,
    ) {}

    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        try {
            assert ( $request->runtime instanceof EnvironmentRuntime );
            
            // Load scripts into the system volume
            $systemVolume = $this->volumeRepository->findOneBy([
                'environmentFactory' => $request->runtime->getEnvironmentFactory(),
                'containerPath' => $this->systemVolumeContainerPath,
            ]);
            assert($systemVolume instanceof EnvironmentFactoryVolume);
            
            $physicalSystemVolumePath = $this->volumeService->getOrCreatePhysicalVolume(
                $systemVolume, $request->user
            );
            
            // Normalize newlines in startup script
            $startupScript = $request->factory->getStartupScript() ?? '';
            $startupScriptNormalized = preg_replace('~\R~u', "\n", $startupScript) ?? '';
            
            // Pre-exec cleanup & create files
            $this->cleanupSystemVolume($physicalSystemVolumePath);
            FileSystem::write(
                $physicalSystemVolumePath . '/' . self::SCRIPT_FNAME, 
                $startupScriptNormalized,
                0777
            );
            FileSystem::write(
                $physicalSystemVolumePath . '/' . self::STDOUT_FNAME,
                '',
                0777
            );
            FileSystem::write(
                $physicalSystemVolumePath . '/' . self::STDERR_FNAME,
                '',
                0777
            );
            
            // Launch
            $this->dockerEngineApi->containerExec($request->runtime);
            
            // Extract logs
            $stdout = FileSystem::read($physicalSystemVolumePath . '/' . self::STDOUT_FNAME);
            $stderr = FileSystem::read($physicalSystemVolumePath . '/' . self::STDERR_FNAME);
            
            $request->runtime->setBuildStdout($stdout);
            $request->runtime->setBuildStderr($stderr);
            $this->manager->persist($request->runtime);
            $this->manager->flush();
            
            // Post-exec cleanup
            $this->cleanupSystemVolume($physicalSystemVolumePath);
            
        } catch (Exception $exception) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                $exception->getMessage(),
            );
        }

        return new ChainOfResponsibilityResponse(
            $request,
            ChainOfResponsibilityResponse::SUCCESS,
        );
    }


    public function getName(): string
    {
        return self::class;
    }


    public function unhandle(ChainOfResponsibilityRequest $request): void
    {        
        return;
    }
    
    private static function cleanupSystemVolume(string $physicalSystemVolumePath): void
    {
        // Cleanup
        FileSystem::delete($physicalSystemVolumePath . '/' . self::SCRIPT_FNAME);
        FileSystem::delete($physicalSystemVolumePath . '/' . self::STDOUT_FNAME);
        FileSystem::delete($physicalSystemVolumePath . '/' . self::STDERR_FNAME);
    }
}