<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Startup\Handler;

use App\Entity\EnvironmentFactoryPort;
use App\Entity\EnvironmentFactoryVolume;
use App\Entity\EnvironmentRuntime;
use App\Repository\EnvironmentFactoryPortRepository;
use App\Repository\EnvironmentFactoryVolumeRepository;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use DateInterval;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;


final class CreateRuntimeHandler implements ChainOfResponsibilityHandlerInterface
{
    public function __construct(
        private readonly EntityManagerInterface $manager,
        private readonly EnvironmentFactoryPortRepository $portRepository,
        private readonly EnvironmentFactoryVolumeRepository $volumeRepository,
    ) {}

    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        // Check if exactly one port is marked as entry port
        $portNeeds = $this->portRepository->findBy([
            'environmentFactory' => $request->factory,
        ]);
        $entryPorts = array_filter(
            $portNeeds, 
            fn(EnvironmentFactoryPort $factoryPort) => $factoryPort->getIsEntryPort() === true,
        );
        if (count($entryPorts) !== 1) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                'The EnvironmentFactory must have exactly one entry port defined.',
            );
        }
        // Check if port needs are unique
        $ports = array_map(fn(EnvironmentFactoryPort $factoryPort) => $factoryPort->getExternalPort(), $portNeeds);
        if (count($ports) !== count(array_unique($ports))) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                'The EnvironmentFactory must have distinct values of EnvironmentFactoryPort.external port.',
            );
        }        
        // Check if volume needs are unique
        $volumeNeeds = $this->volumeRepository->findBy([
            'environmentFactory' => $request->factory,
        ]);
        $volumePaths = array_map(
            fn(EnvironmentFactoryVolume $factoryVolume) => $factoryVolume->getContainerPath(),
            $volumeNeeds,
        );
        if (count($volumePaths) !== count(array_unique($volumePaths))) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                'The EnvironmentFactory must have distinct values of EnvironmentFactoryVolume.containerPath.',
            );
        }
        // Try to create the runtime
        try {
            $runtime = new EnvironmentRuntime();

            $now = new DateTimeImmutable('now');
            $runtime->setStartedUpAt($now);
            $runtime->setShutDownScheduledAt($now->add(new DateInterval('PT3H')));
            $runtime->setUrl(''); // will be set up later on by the chain
            $runtime->setUsedBy($request->user);
            $runtime->setEnvironmentFactory($request->factory);
            $this->manager->persist($runtime);
            $this->manager->flush();

            $request->runtime = $runtime;
        } catch (Exception $exception) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                $exception->getMessage(),
            );
        }
        
        return new ChainOfResponsibilityResponse(
            $request,
            ChainOfResponsibilityResponse::SUCCESS,
        );
    }


    public function getName(): string
    {
        return self::class;
    }


    public function unhandle(ChainOfResponsibilityRequest $request): void
    {    
        if ($request->runtime instanceof EnvironmentRuntime) {
            $this->manager->remove($request->runtime);
            $this->manager->flush();
        }
    }
}