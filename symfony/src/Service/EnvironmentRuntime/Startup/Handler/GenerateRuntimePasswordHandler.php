<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Startup\Handler;

use App\Entity\EnvironmentRuntime;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use Exception;
use Nette\Utils\Random;


class GenerateRuntimePasswordHandler implements ChainOfResponsibilityHandlerInterface
{
    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        try {
            assert ( $request->runtime instanceof EnvironmentRuntime );
            $request->runtime->setPassword($this->generatePassword());
        } catch (Exception $exception) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                $exception->getMessage(),
            );
        }
        
        return new ChainOfResponsibilityResponse(
            $request,
            ChainOfResponsibilityResponse::SUCCESS,
        );
    }


    public function getName(): string
    {
        return self::class;
    }


    public function unhandle(ChainOfResponsibilityRequest $request): void
    {
        return;
    }
    
    protected function generatePassword(): string
    {
        return Random::generate(3, '0-9') . "-" . Random::generate(3, '0-9');
    }
}