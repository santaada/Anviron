<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Startup\Handler;

use App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Repository\EnvironmentRuntimeRepository;


final class SameFactoryCheckHandler implements ChainOfResponsibilityHandlerInterface
{
    public function __construct(
        private readonly EnvironmentRuntimeRepository $runtimeRepository,
    ) {}

    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        // Check if the same Factory has no other running env for the given user
        $duplicates = $this->runtimeRepository->findOneBy([
            'usedBy' => $request->user,
            'environmentFactory' => $request->factory,
            'shutDownExecutedAt' => null,
        ]);
        if ($duplicates !== null) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                self::class . ': ' . 'The same environment is already running for this user.'
            );
        }
        return new ChainOfResponsibilityResponse(
            $request,
            ChainOfResponsibilityResponse::SUCCESS,
        );
    }


    public function getName(): string
    {
        return self::class;
    }


    public function unhandle(ChainOfResponsibilityRequest $request): void
    {
        return;
    }
}