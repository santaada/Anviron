<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Startup\Handler;

use App\Service\EnvironmentFactoryVolumeService;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use Exception;


final class AllocateVolumesHandler implements ChainOfResponsibilityHandlerInterface
{
    public function __construct(
        private readonly EnvironmentFactoryVolumeService $volumeOperation,
    ) {}

    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        try {
            foreach ($request->factory->getEnvironmentFactoryVolumes() as $volume) {
                $this->volumeOperation->getOrCreatePhysicalVolume($volume, $request->user);
            }
        } catch (Exception $exception) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                'Unable to allocate volumes.'
            );
        }
        
        return new ChainOfResponsibilityResponse(
            $request,
            ChainOfResponsibilityResponse::SUCCESS,
        );
    }


    public function getName(): string
    {
        return self::class;
    }


    public function unhandle(ChainOfResponsibilityRequest $request): void
    {        
        return;
    }
}