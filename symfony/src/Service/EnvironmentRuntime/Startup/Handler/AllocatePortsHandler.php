<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Startup\Handler;

use App\Entity\EnvironmentRuntime;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntimePortService;
use App\Repository\EnvironmentRuntimePortRepository;
use Psr\Log\LoggerInterface;


final class AllocatePortsHandler implements ChainOfResponsibilityHandlerInterface
{
    public function __construct(
        private readonly EnvironmentRuntimePortService    $runtimePortOperation,
        private readonly EnvironmentRuntimePortRepository $runtimePortRepository,
        private readonly LoggerInterface                  $logger,
        private readonly string                           $serverName,
    ) {}

    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        assert ( $request->runtime instanceof EnvironmentRuntime );
        
        foreach ($request->factory->getEnvironmentFactoryPorts() as $port) {
            $allocated = $this->runtimePortOperation->allocate($request->runtime, $port);
            if ($allocated === null) {
                $this->logger->error('EnvironmentRuntimeOperartion: Unable to satisfy all port needs', [
                    'factoryPort' => $port->getId(),
                ]);
                return new ChainOfResponsibilityResponse(
                    $request,
                    ChainOfResponsibilityResponse::FAIL,
                    'Unable to allocate ports.'
                );
            }
            if ($port->getIsEntryPort() === true) {
                if ($request->runtime->getUrl() !== null) {
                    $this->logger->warning('AllocatePortsHandler: Expected EnvironmentRuntime.url to be null');
                }
                $request->runtime->setUrl(
                    "http://" 
                    . $this->serverName
                    . ":" 
                    . $allocated->getExternalPort()
                    . "/"
                );
            }
        }
        
        return new ChainOfResponsibilityResponse(
            $request,
            ChainOfResponsibilityResponse::SUCCESS,
        );
    }


    public function getName(): string
    {
        return self::class;
    }


    public function unhandle(ChainOfResponsibilityRequest $request): void
    {
        $ports = $this->runtimePortRepository->findBy([
            'environmentRuntime' => $request->runtime
        ]);
        foreach ($ports as $portToFree) {
            $this->runtimePortOperation->free($portToFree);
        }
    }
}