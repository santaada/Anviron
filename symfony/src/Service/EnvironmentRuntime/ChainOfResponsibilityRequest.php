<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentRuntime;
use App\Entity\User;


class ChainOfResponsibilityRequest
{
    public function __construct(
        public User $user,
        public EnvironmentFactory $factory,
        public ?EnvironmentRuntime $runtime = null,
    ) {}
}