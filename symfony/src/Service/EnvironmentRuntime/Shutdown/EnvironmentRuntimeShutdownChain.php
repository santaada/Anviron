<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Shutdown;


use App\Service\EnvironmentRuntime\AbstractChainOfResponsibility;
use App\Service\EnvironmentRuntime\Shutdown\Handler\ContainerStopHandler;
use App\Service\EnvironmentRuntime\Shutdown\Handler\ExecuteShutdownScriptHandler;
use App\Service\EnvironmentRuntime\Shutdown\Handler\FreePortsHandler;
use App\Service\EnvironmentRuntime\Shutdown\Handler\UpdateRuntimeHandler;


final class EnvironmentRuntimeShutdownChain extends AbstractChainOfResponsibility
{
    public function __construct(
        private readonly UpdateRuntimeHandler $updateRuntimeHandler,
        private readonly FreePortsHandler $freePortsHandler,
        private readonly ContainerStopHandler $containerStopHandler,
        private readonly ExecuteShutdownScriptHandler $executeShutdownScriptHandler,
    ) {
        $this->addHandler($this->executeShutdownScriptHandler);
        $this->addHandler($this->containerStopHandler);
        $this->addHandler($this->updateRuntimeHandler);
        $this->addHandler($this->freePortsHandler);
    }
}