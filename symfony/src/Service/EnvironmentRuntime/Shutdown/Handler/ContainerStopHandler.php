<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Shutdown\Handler;

use App\Entity\EnvironmentRuntime;
use App\Integration\DockerApi\DockerEngineInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use Exception;


final class ContainerStopHandler implements ChainOfResponsibilityHandlerInterface
{
    public function __construct(
        private readonly DockerEngineInterface $dockerEngineApi,
    ) {}

    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        try {
            assert( $request->runtime instanceof EnvironmentRuntime); // This step requires runtime to be set
            $this->dockerEngineApi->containerStop($request->runtime);
            $this->dockerEngineApi->containerDelete($request->runtime);
        } catch (Exception $exception) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                $exception->getMessage(),
            );
        }
        
        return new ChainOfResponsibilityResponse(
            $request,
            ChainOfResponsibilityResponse::SUCCESS,
        );
    }


    public function getName(): string
    {
        return self::class;
    }


    public function unhandle(ChainOfResponsibilityRequest $request): void
    {
        return;
    }
}