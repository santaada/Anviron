<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Shutdown\Handler;

use App\Entity\EnvironmentFactoryVolume;
use App\Entity\EnvironmentRuntime;
use App\Service\EnvironmentFactoryVolumeService;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Repository\EnvironmentFactoryVolumeRepository;
use App\Integration\DockerApi\DockerEngineSocketApi;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Nette\Utils\FileSystem;


final class ExecuteShutdownScriptHandler implements ChainOfResponsibilityHandlerInterface
{
    private const SCRIPT_FNAME = 'exec.sh';
    private const STDOUT_FNAME = 'exec.out';
    private const STDERR_FNAME = 'exec.err';
    
    public function __construct(
        private readonly EntityManagerInterface             $manager,
        private readonly DockerEngineSocketApi              $dockerEngineApi,
        private readonly EnvironmentFactoryVolumeRepository $volumeRepository,
        private readonly EnvironmentFactoryVolumeService    $volumeOperation,
        private string                               $systemVolumeContainerPath,
    ) {}

    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        try {
            assert ( $request->runtime instanceof EnvironmentRuntime );
            
            // Load scripts into the system volume
            $systemVolume = $this->volumeRepository->findOneBy([
                'environmentFactory' => $request->runtime->getEnvironmentFactory(),
                'containerPath' => $this->systemVolumeContainerPath,
            ]);
            assert($systemVolume instanceof EnvironmentFactoryVolume);
            
            $physicalSystemVolumePath = $this->volumeOperation->getOrCreatePhysicalVolume(
                $systemVolume, $request->user
            );
            
            // Normalize newlines in shutdown script
            $shutdownScriptHandler = $request->factory->getShutdownScript() ?? '';
            $shutdownScriptNormalized = preg_replace('~\R~u', "\n", $shutdownScriptHandler) ?? '';
            
            // Pre-exec cleanup & create files
            $this->cleanupSystemVolume($physicalSystemVolumePath);
            FileSystem::write(
                $physicalSystemVolumePath . '/' . self::SCRIPT_FNAME, 
                $shutdownScriptNormalized,
                0777
            );
            FileSystem::write(
                $physicalSystemVolumePath . '/' . self::STDOUT_FNAME,
                '',
                0777
            );
            FileSystem::write(
                $physicalSystemVolumePath . '/' . self::STDERR_FNAME,
                '',
                0777
            );
            
            // Launch
            $this->dockerEngineApi->containerExec($request->runtime);
            
            // Extract logs
            $stdout = FileSystem::read($physicalSystemVolumePath . '/' . self::STDOUT_FNAME);
            $stderr = FileSystem::read($physicalSystemVolumePath . '/' . self::STDERR_FNAME);
            
            $request->runtime->setShutdownStdout($stdout);
            $request->runtime->setShutdownStderr($stderr);
            $this->manager->persist($request->runtime);
            $this->manager->flush();
            
            // Post-exec cleanup
            $this->cleanupSystemVolume($physicalSystemVolumePath);
            
        } catch (Exception $exception) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                $exception->getMessage(),
            );
        }

        return new ChainOfResponsibilityResponse(
            $request,
            ChainOfResponsibilityResponse::SUCCESS,
        );
    }


    public function getName(): string
    {
        return self::class;
    }


    public function unhandle(ChainOfResponsibilityRequest $request): void
    {        
        return;
    }
    
    private static function cleanupSystemVolume(string $physicalSystemVolumePath): void
    {
        // Cleanup
        FileSystem::delete($physicalSystemVolumePath . '/' . self::SCRIPT_FNAME);
        FileSystem::delete($physicalSystemVolumePath . '/' . self::STDOUT_FNAME);
        FileSystem::delete($physicalSystemVolumePath . '/' . self::STDERR_FNAME);
    }
}