<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Shutdown\Handler;

use App\Entity\EnvironmentRuntime;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntimePortService;
use App\Repository\EnvironmentRuntimePortRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;


final class FreePortsHandler implements ChainOfResponsibilityHandlerInterface
{
    public function __construct(
        private readonly EnvironmentRuntimePortService    $runtimePortService,
        private readonly EntityManagerInterface           $manager,
    ) {}

    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        try {
            assert( $request->runtime instanceof EnvironmentRuntime); // This step requires runtime to be set
            // Free ports
            foreach ($request->runtime->getRuntimePorts() as $port) {
                $this->runtimePortService->free($port);
            }
            $this->manager->flush();
        } catch (Exception $exception) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                $exception->getMessage(),
            );
        }

        return new ChainOfResponsibilityResponse(
            $request,
            ChainOfResponsibilityResponse::SUCCESS,
        );
    }

    public function getName(): string
    {
        return self::class;
    }
    
    public function unhandle(ChainOfResponsibilityRequest $request): void
    {
        return;
    }
}