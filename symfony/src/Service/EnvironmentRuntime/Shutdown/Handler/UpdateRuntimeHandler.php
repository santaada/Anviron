<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime\Shutdown\Handler;

use App\Entity\EnvironmentRuntime;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityHandlerInterface;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;


final class UpdateRuntimeHandler implements ChainOfResponsibilityHandlerInterface
{
    public function __construct(
        private readonly EntityManagerInterface $manager,
    ) {}

    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse
    {
        try {
            assert( $request->runtime instanceof EnvironmentRuntime); // This step requires runtime to be set
            $request->runtime->setShutDownExecutedAt(new DateTimeImmutable('now'));
            $this->manager->persist($request->runtime);
            $this->manager->flush();
        } catch (Exception $exception) {
            return new ChainOfResponsibilityResponse(
                $request,
                ChainOfResponsibilityResponse::FAIL,
                $exception->getMessage(),
            );
        }
        
        return new ChainOfResponsibilityResponse(
            $request,
            ChainOfResponsibilityResponse::SUCCESS,
        );
    }


    public function getName(): string
    {
        return self::class;
    }


    public function unhandle(ChainOfResponsibilityRequest $request): void
    {
        if ( $request->runtime instanceof EnvironmentRuntime) {
            $request->runtime->setShutDownExecutedAt(null);
            $this->manager->persist($request->runtime);
            $this->manager->flush();
        }
    }
}