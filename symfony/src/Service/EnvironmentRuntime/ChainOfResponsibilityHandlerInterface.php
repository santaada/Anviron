<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime;

interface ChainOfResponsibilityHandlerInterface
{
    public function getName(): string;
    
    public function handle(ChainOfResponsibilityRequest $request): ChainOfResponsibilityResponse;
    
    public function unhandle(ChainOfResponsibilityRequest $request): void;
}