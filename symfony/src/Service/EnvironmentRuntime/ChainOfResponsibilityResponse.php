<?php declare(strict_types=1);

namespace App\Service\EnvironmentRuntime;


class ChainOfResponsibilityResponse
{
    public const SUCCESS = 0;
    public const FAIL = 1;
    
    public function __construct(
        public ChainOfResponsibilityRequest $request,
        public int $state,
        public string $message = '',
    ) {}
}