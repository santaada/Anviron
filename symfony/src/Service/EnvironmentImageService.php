<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\EnvironmentImage;


class EnvironmentImageService
{    
    public function canBeDeleted(EnvironmentImage $image): bool
    {
        return $image->getEnvironmentImageBuilds()->count() === 0;
    }
}