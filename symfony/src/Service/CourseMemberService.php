<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\Course;
use App\Entity\CourseMember;
use App\Entity\CourseRoleType;
use App\Entity\User;
use App\Integration\KosApi\Command\GetStudentEnrolledCoursesCommand;
use App\Integration\KosApi\Command\GetTeacherEnrolledCoursesCommand;
use App\Integration\KosApi\Dto\StudentEnrolledCourseDto;
use App\Integration\KosApi\Dto\TeacherEnrolledCourseDto;
use App\Integration\KosApi\KosApiIntegration;
use App\Repository\CourseMemberRepository;
use App\Repository\CourseRepository;
use App\Repository\SubjectRepository;
use App\Repository\TermRepository;
use Doctrine\ORM\EntityManagerInterface;


class CourseMemberService
{
    public function __construct(
        private readonly KosApiIntegration      $kosApi,
        private readonly CourseRepository       $courseRepository,
        private readonly CourseMemberRepository $courseMemberRepository,
        private readonly TermRepository         $termRepository,
        private readonly SubjectRepository      $subjectRepository,
        private readonly EntityManagerInterface $manager,
    ) {
    }

    /**
     * Syncs CourseMember records for provided User, so that they represent the current status in KOS API 
     * 
     * @param User $user
     * @return void
     */
    public function syncUserWithKosApi(User $user): void
    {
        // Fetch data from KOS API for given User
        $enrollments = $this->kosApi->get( new GetStudentEnrolledCoursesCommand($user->getUsername()) );

        $this->deleteCourseMembers($user);

        foreach ($enrollments as $enrollment) {
            assert($enrollment instanceof StudentEnrolledCourseDto);
            $subject = $this->subjectRepository->findOneBy(['code' => $enrollment->subjectCode]);
            $term = $this->termRepository->findOneBy(['code' => $enrollment->termCode]);

            // Skip unknown terms / subjects. Assuming terms / subjects are synced before course members.
            if ($subject === null || $term === null) continue; 

            $course = $this->courseRepository->findOneBy(['term' => $term, 'subject' => $subject]);
            // Skip unknown courses. Assuming courses are synced before course members.     
            if ($course === null) continue;        
            
            // Create fresh CourseMember instance
            $courseMember = new CourseMember();
            $courseMember->setUser($user);
            $courseMember->setCourse($course);
            $courseMember->setRole(CourseRoleType::STUDENT->value);
            $this->manager->persist($courseMember);
            $this->manager->flush();
        }

        $teachingEnrollments = $this->kosApi->get( new GetTeacherEnrolledCoursesCommand($user->getUsername()) );

        foreach ($teachingEnrollments as $teachingEnrollment) {
            assert($teachingEnrollment instanceof TeacherEnrolledCourseDto);
            $requiredRoles = [];
            if ( in_array($user->getUsername(), $teachingEnrollment->editors, true) ) {
                $requiredRoles[] = CourseRoleType::TEACHER_EDITOR->value;
            }
            if ( in_array($user->getUsername(), $teachingEnrollment->examiners, true) ) {
                $requiredRoles[] = CourseRoleType::TEACHER_EXAMINATOR->value;
            }
            if ( in_array($user->getUsername(), $teachingEnrollment->guarantors, true) ) {
                $requiredRoles[] = CourseRoleType::TEACHER_GUARANTOR->value;
            }
            if ( in_array($user->getUsername(), $teachingEnrollment->instructors, true) ) {
                $requiredRoles[] = CourseRoleType::TEACHER_TUTOR_LAB->value;
            }
            if ( in_array($user->getUsername(), $teachingEnrollment->lecturers, true) ) {
                $requiredRoles[] = CourseRoleType::TEACHER_LECTURER->value;
            }

            if ( count($requiredRoles) > 0 ) {
                $userRoles = $user->getRoles();
                $userRoles[] = 'ROLE_TEACHER';
                $user->setRoles($userRoles);
            }

            foreach ($requiredRoles as $requiredRole) {
                $subject = $this->subjectRepository->findOneBy(['code' => $teachingEnrollment->subjectCode]);
                $term = $this->termRepository->findOneBy(['code' => $teachingEnrollment->termCode]);

                if ($subject === null || $term === null) continue; // Assuming terms / subjects are synced before course members

                $course = $this->courseRepository->findOneBy(['term' => $term, 'subject' => $subject]);
                if ($course === null) continue; // Assuming courses are synced before course members

                $courseMember = new CourseMember();
                $courseMember->setUser($user);
                $courseMember->setCourse($course);
                $courseMember->setRole($requiredRole);
                $this->manager->persist($courseMember);
            }
        }
    }


    /**
     * Checks if the given user has any teaching role in any course
     * 
     * @param \App\Entity\User $user
     * @return bool
     */
    public function hasAnyTeacherRole(User $user): bool {
        $courseMember = $this->courseMemberRepository->findOneBy([
            'user' => $user,
            'role' => CourseRoleType::TEACHER->value,
        ]);
        if ($courseMember instanceof CourseMember) {
            return true;
        }
        return false;
    }

    /**
     * Deletes all CourseMember instances related to the given user
     *
     * @param \App\Entity\User $user
     * @return void
     */
    protected function deleteCourseMembers(User $user): void
    {
        $courseMembers = $this->courseMemberRepository->findBy([
            'user' => $user,
        ]);
        foreach ($courseMembers as $courseMember) {
            $this->manager->remove($user);
        }
        $this->manager->flush();
    }


    /**
     * @param Course[]         $courses
     */
    public function hasAnyStudentRoleInCourses(User $user, array $courses): bool
    {
        $courseMember = $this->courseMemberRepository->findOneBy([
            'user' => $user,
            'course' => $courses,
            'role' => CourseRoleType::STUDENT->value,
        ]);
        return $courseMember instanceof CourseMember;
    }

    /**
     * @param Course[]         $courses
     */
    public function hasAnyTeacherRoleInCourses(User $user, array $courses): bool
    {
        $courseMember = $this->courseMemberRepository->findOneBy([
            'user' => $user,
            'course' => $courses,
            'role' => CourseRoleType::TEACHER->value,
        ]);
        return $courseMember instanceof CourseMember;
    }


    public function isTeacherOf(User $userTeacher, User $userStudent): bool
    {
        // Get all student's courses
        $studentsCourseMemberRecords = $this->courseMemberRepository->findBy([
            'user' => $userStudent,
            'role' => CourseRoleType::STUDENT->value,
        ]);
        $studentCourses = [];
        foreach ($studentsCourseMemberRecords as $studentsCourseMemberRecord) {
            $studentCourses[] = $studentsCourseMemberRecord->getCourse();
        }
        // For all student's courses: see if provided userTeacher is a teacher of that course
        return $this->hasAnyTeacherRoleInCourses($userTeacher, $studentCourses);
    }
}