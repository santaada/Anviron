<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentFactoryPort;
use App\Entity\EnvironmentRuntime;
use App\Entity\EnvironmentRuntimePort;
use App\Repository\EnvironmentRuntimePortRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;


class EnvironmentRuntimePortService
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly EnvironmentRuntimePortRepository $portRepository,
        private readonly EntityManagerInterface $manager,
    ) {}

    public function allocate(EnvironmentRuntime $runtime, EnvironmentFactoryPort $portNeed): ?EnvironmentRuntimePort
    {
        $runtimeFactory = $runtime->getEnvironmentFactory();
        $portNeedFactory = $portNeed->getEnvironmentFactory();
        assert($runtimeFactory instanceof EnvironmentFactory && $portNeedFactory instanceof EnvironmentFactory);
        // Check if runtime and port need belong to the same factory
        if ($runtimeFactory->getId() !== $portNeedFactory->getId()){
            $this->logger->error('EnvironmentRuntimePortService: Port and runtime do not share the same factory', [
                'runtime' => $runtimeFactory->getId(),
                'factoryPort' => $portNeedFactory->getId(),
            ]);
            return null;
        }
        
        $port = $this->getFreePort(); // May fail if the ports pool is empty
        if ($port === null) return null;
                 
        $port->setAllocatedAt(new DateTimeImmutable());
        $port->setFactoryPort($portNeed);
        $runtime->addRuntimePort($port);
        
        $this->manager->persist($port);
        $this->manager->persist($runtime);
        $this->manager->flush();
        
        $this->logger->info('EnvironmentRuntimePortService: Port allocated', [
            'runtime' => $runtime->getId(),
            'factoryPort' => $portNeed->getId(),
        ]);
        
        return $port;
    }
    
    public function free(EnvironmentRuntimePort $port): void
    {
        $this->logger->info('EnvironmentRuntimePortService: Port freed', [
            'runtime' => $port->getEnvironmentRuntime()?->getId(),
            'factoryPort' => $port->getFactoryPort()?->getId(),
            'runtimePort' => $port->getId(),
        ]);

        $port->setEnvironmentRuntime(null);
        $port->setFactoryPort(null);
        $this->manager->persist($port);
        $this->manager->flush();
    }
    
    public function getFreePort(): ?EnvironmentRuntimePort
    {
        return $this->portRepository->findOneBy([
            'environmentRuntime' => null, 
            'factoryPort' => null,
        ], [
            'allocatedAt' => 'ASC' // Make sure the ports are used evenly
        ]);
    }
}