<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Subject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class SubjectFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        /** @var Subject[] $subjects */
        $subjects = [];

        $subject = new Subject();
        $subject->setCode('BI-PA1');
        $subject->setName('Programování a algoritmizace 1');
        $subjects[] = $subject;

        $subject = new Subject();
        $subject->setCode('BI-PA2');
        $subject->setName('Programování a algoritmizace 2');
        $subjects[] = $subject;        
        
        $subject = new Subject();
        $subject->setCode('BI-2DT');
        $subject->setName('2D Tisk');
        $subjects[] = $subject;

        $subject = new Subject();
        $subject->setCode('BI-FUJ');
        $subject->setName('Funkcionální jazyky');
        $subjects[] = $subject;

        $subject = new Subject();
        $subject->setCode('BI-NGO');
        $subject->setName('Aplikovaná statistika');
        $subjects[] = $subject;

        $subject = new Subject();
        $subject->setCode('BI-ZON');
        $subject->setName('Náuka o bizonech');
        $subjects[] = $subject;

        foreach ($subjects as $subject) {
            $manager->persist($subject);
        }

        $manager->flush();
    }
}
