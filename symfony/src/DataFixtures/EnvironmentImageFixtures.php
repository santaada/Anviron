<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\EnvironmentImage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Nette\Utils\FileSystem;


final class EnvironmentImageFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $dockerfile = FileSystem::read( __DIR__ . '/Assets/EnvironmentImage/linuxserver-code-server/Dockerfile' );
        $image = new EnvironmentImage();
        $image->setName('linuxserver/code-server');
        $image->setDockerfile($dockerfile);
        $manager->persist($image);

        $manager->flush();
    }
}
