<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\EnvironmentImage;
use App\Entity\EnvironmentImageBuild;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class EnvironmentImageBuildFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $userRepository = $manager->getRepository(User::class);
        $dolores = $userRepository->findOneBy([
            'username' => 'umbridol'
        ]);
        assert($dolores instanceof User);
        
        $imageRepository = $manager->getRepository(EnvironmentImage::class);
        $image = $imageRepository->findOneBy([
            'name' => 'linuxserver/code-server',
        ]);
        assert($image instanceof EnvironmentImage);
        
        $build = new EnvironmentImageBuild();
        $build->setCreatedAt(new \DateTimeImmutable('now'));
        $build->setCreatedBy($dolores);
        $build->setTag('anviron-phpunit-testing-build-1');
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        
        $manager->persist($build);
        $manager->flush();
    }


    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            EnvironmentImageFixtures::class,
        ];
    }
}
