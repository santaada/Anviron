<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\EnvironmentRuntimePort;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class EnvironmentRuntimePortFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $portNumbers = [];
        for ($i = 49152; $i <= 49300; $i++) {
            $portNumbers[] = $i;
        }
        
        foreach ($portNumbers as $portNumber) {
            $port = new EnvironmentRuntimePort();
            $port->setExternalPort($portNumber);
            $manager->persist($port);
        }
        
        $manager->flush();
    }
}
