<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\Subject;
use App\Entity\Term;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class CourseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $termRepository = $manager->getRepository(Term::class);
        $subjectRepository = $manager->getRepository(Subject::class);
        
        /** @var Course[] $courses */
        $courses = [];

        $course = new Course();
        $subject = $subjectRepository->findOneBy(['code' => 'BI-PA1']);
        assert($subject instanceof Subject);
        $course->setSubject($subject);
        $term = $termRepository->findOneBy(['code' => 'T200']);
        assert($term instanceof Term);
        $course->setTerm($term);
        $courses[] = $course;

        $course = new Course();
        $subject = $subjectRepository->findOneBy(['code' => 'BI-PA2']);
        assert($subject instanceof Subject);
        $course->setSubject($subject);
        $term = $termRepository->findOneBy(['code' => 'T201']);
        assert($term instanceof Term);
        $course->setTerm($term);
        $courses[] = $course;
        
        $course = new Course();
        $subject = $subjectRepository->findOneBy(['code' => 'BI-FUJ']);
        assert($subject instanceof Subject);
        $course->setSubject($subject);
        $term = $termRepository->findOneBy(['code' => 'T200']);
        assert($term instanceof Term);
        $course->setTerm($term);
        $courses[] = $course;

        $course = new Course();
        $subject = $subjectRepository->findOneBy(['code' => 'BI-FUJ']);
        assert($subject instanceof Subject);
        $course->setSubject($subject);
        $term = $termRepository->findOneBy(['code' => 'T201']);
        assert($term instanceof Term);
        $course->setTerm($term);
        $courses[] = $course;
        
        foreach ($courses as $course) {
            $manager->persist($course);
        }

        $manager->flush();
    }


    public function getDependencies(): array
    {
        return [
            SubjectFixtures::class,
            TermFixtures::class,
        ];
    }
}
