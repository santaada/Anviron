<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        /** @var User[] $users */
        $users = [];

        $user = new User();
        $user->setTitleBeforeName('');
        $user->setFirstName('Eleonóra');
        $user->setLastName('Skočdopolová');
        $user->setTitleAfterName('');
        $user->setEmail('skocdele@fit.cvut.cz');
        $user->setUsername('skocdele');
        $users[] = $user;

        $user = new User();
        $user->setTitleBeforeName('Mgr.');
        $user->setFirstName('Jiřina');
        $user->setLastName('Řekořková');
        $user->setTitleAfterName('');
        $user->setEmail('rehorji1@fit.cvut.cz');
        $user->setUsername('rehorji1');
        $users[] = $user;

        $user = new User();
        $user->setTitleBeforeName('');
        $user->setFirstName('Joe');
        $user->setLastName('Reacher');
        $user->setTitleAfterName('');
        $user->setEmail('reachjoe@fit.cvut.cz');
        $user->setUsername('reachjoe');
        $users[] = $user;

        $user = new User();
        $user->setTitleBeforeName('doc. Ing.');
        $user->setFirstName('Dolores');
        $user->setLastName('Umbridge');
        $user->setTitleAfterName('CSc.');
        $user->setEmail('umbridol@fit.cvut.cz');
        $user->setUsername('umbridol');
        $users[] = $user;

        foreach ($users as $user) {
            $manager->persist($user);
        }
        
        $manager->flush();
    }
}
