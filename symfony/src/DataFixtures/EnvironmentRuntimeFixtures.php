<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentFactoryVolume;
use App\Entity\EnvironmentRuntime;
use App\Entity\User;
use App\Repository\EnvironmentFactoryRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class EnvironmentRuntimeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        // Prepare repositories to connect to testing db
        $userRepository = $manager->getRepository(User::class);
        assert($userRepository instanceof UserRepository);
        $factoryRepository = $manager->getRepository(EnvironmentFactory::class);
        assert($factoryRepository instanceof EnvironmentFactoryRepository);

        $now = new \DateTimeImmutable('now');
        
        // #1 - Already running environment
        $runtime = new EnvironmentRuntime();
        
        $runtime->setStartedUpAt($now);
        $runtime->setShutDownScheduledAt($now->add(new \DateInterval('PT3H')));
        $runtime->setShutDownExecutedAt($now->add(new \DateInterval('PT3H')));
        $runtime->setUrl('');
        
        $user = $userRepository->findOneBy(['username' => 'reachjoe']);
        assert($user instanceof User);
        $runtime->setUsedBy($user);
        
        $factory = $factoryRepository->findOneBy(['name' => 'Sample environment for C compilation']);
        assert($factory instanceof EnvironmentFactory);
        $runtime->setEnvironmentFactory($factory);
        
        $manager->persist($factory);
        $manager->persist($runtime);

        /*$systemVolume = new EnvironmentFactoryVolume();
        $systemVolume->setContainerPath('/home/.anviron');
        $systemVolume->setEnvironmentFactory($factory);
        $manager->persist($systemVolume);*/
        
        // End
        $manager->flush();
    }


    public function getDependencies(): array
    {
        return [
            EnvironmentFactoryFixtures::class,
            UserFixtures::class,
        ];
    }
}
