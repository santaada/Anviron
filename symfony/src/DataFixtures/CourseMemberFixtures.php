<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\CourseMember;
use App\Entity\CourseRoleType;
use App\Entity\Subject;
use App\Entity\Term;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class CourseMemberFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $courseRepository = $manager->getRepository(Course::class);
        $userRepository = $manager->getRepository(User::class);
        $subjectRepository = $manager->getRepository(Subject::class);
        $termRepository = $manager->getRepository(Term::class);

        /** @var CourseMember[] $courseMembers */
        $courseMembers = [];
        
        $subject = $subjectRepository->findOneBy(['code' => 'BI-PA1']);
        $term = $termRepository->findOneBy(['code' => 'T200']);
        
        $courseMember = new CourseMember();
        $user = $userRepository->findOneBy(['username' => 'reachjoe']);
        assert($user instanceof User);
        $courseMember->setUser($user);
        $course = $courseRepository->findOneBy(['subject' => $subject, 'term' => $term]);
        assert($course instanceof Course);
        $courseMember->setCourse($course);
        $courseMember->setRole(CourseRoleType::STUDENT->value);
        $courseMembers[] = $courseMember;

        $courseMember = new CourseMember();
        $user = $userRepository->findOneBy(['username' => 'umbridol']);
        assert($user instanceof User);
        $courseMember->setUser($user);
        $course = $courseRepository->findOneBy(['subject' => $subject, 'term' => $term]);
        assert($course instanceof Course);
        $courseMember->setCourse($course);
        $courseMember->setRole(CourseRoleType::TEACHER->value);
        $courseMembers[] = $courseMember;

        $courseMember = new CourseMember();
        $user = $userRepository->findOneBy(['username' => 'umbridol']);
        assert($user instanceof User);
        $courseMember->setUser($user);
        $course = $courseRepository->findOneBy(['subject' => $subject, 'term' => $term]);
        assert($course instanceof Course);
        $courseMember->setCourse($course);
        $courseMember->setRole(CourseRoleType::TEACHER_EXAMINATOR->value);
        $courseMembers[] = $courseMember;

        foreach ($courseMembers as $courseMember) {
            $manager->persist($courseMember);
        }

        $manager->flush();
    }


    public function getDependencies(): array
    {
        return [
            CourseFixtures::class,
            UserFixtures::class,
            SubjectFixtures::class,
            TermFixtures::class,
        ];
    }
}
