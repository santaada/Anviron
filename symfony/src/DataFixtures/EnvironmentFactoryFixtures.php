<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentImageBuild;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Nette\Utils\FileSystem;


final class EnvironmentFactoryFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $buildRepository = $manager->getRepository(EnvironmentImageBuild::class);
        $userRepository = $manager->getRepository(User::class);
        
        $factory = new EnvironmentFactory();
        $factory->setName("Sample environment for C compilation");
        $startupScript = FileSystem::read(
            __DIR__ . '/Assets/EnviornmentFactory/sample-environment-for-c-compilation/startup.sh'
        );
        $factory->setStartupScript($startupScript);
        $shutdownScript = FileSystem::read(
            __DIR__ . '/Assets/EnviornmentFactory/sample-environment-for-c-compilation/shutdown.sh'
        );
        $factory->setShutdownScript($shutdownScript);
        
        $build = $buildRepository->findOneBy(['tag' => 'anviron-phpunit-testing-build-1']);
        assert($build instanceof EnvironmentImageBuild);
        
        $factory->setEnvironmentImageBuild($build);
        $factory->setIsVisibleForCourseStudents(true);
        $user = $userRepository->findOneBy(['username' => 'umbridol']);
        assert($user instanceof User);
        $factory->setMaintainedBy($user);
        
        $manager->persist($factory);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            EnvironmentImageBuildFixtures::class,
        ];
    }
}
