<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\ConnectionType;
use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentFactoryPort;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class EnvironmentFactoryPortFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $envFactoryRepository = $manager->getRepository(EnvironmentFactory::class);

        $port = new EnvironmentFactoryPort();
        $factory = $envFactoryRepository->findOneBy(['name' => 'Sample environment for C compilation']);
        assert($factory instanceof EnvironmentFactory);
        $port->setExternalPort(8443);
        $port->setIsEntryPort(true);
        $port->setConnectionType(ConnectionType::TCP->value);
        $port->setEnvironmentFactory($factory);
        $manager->persist($port);
        
        $manager->flush();
    }


    public function getDependencies(): array
    {
        return [
            EnvironmentFactoryFixtures::class,
        ];
    }
}
