<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Term;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class TermFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        /** @var Term[] $terms */
        $terms = [];
        
        $term = new Term();
        $term->setCode('T200');
        $time = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2000-09-01 00:00:00');
        assert($time instanceof DateTimeImmutable);
        $term->setStartsAt($time);
        $time = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2001-02-15 23:59:59');
        assert($time instanceof DateTimeImmutable);
        $term->setEndsAt($time);
        $terms[] = $term;

        $term = new Term();
        $term->setCode('T201');
        $time = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2000-02-16 00:00:00');
        assert($time instanceof DateTimeImmutable);
        $term->setStartsAt($time);
        $time = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2001-06-30 23:59:59');
        assert($time instanceof DateTimeImmutable);
        $term->setEndsAt($time);
        $terms[] = $term;
        
        foreach ($terms as $term) {
            $manager->persist($term);
        }
        
        $manager->flush();
    }
}
