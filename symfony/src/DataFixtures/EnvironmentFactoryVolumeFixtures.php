<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentFactoryVolume;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class EnvironmentFactoryVolumeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $envFactoryRepository = $manager->getRepository(EnvironmentFactory::class);
        
        $volume = new EnvironmentFactoryVolume();
        $factory = $envFactoryRepository->findOneBy(['name' => 'Sample environment for C compilation']);
        assert($factory instanceof EnvironmentFactory);
        $volume->setEnvironmentFactory($factory);
        $volume->setContainerPath('/home/student/workspace');
        $manager->persist($volume);
        
        $manager->flush();
    }
    
    
    public function getDependencies(): array
    {
        return [
            EnvironmentFactoryFixtures::class,
        ];
    }
}
