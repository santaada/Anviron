#!/usr/bin/env bash

echo "Kos sync: Starting"
echo "Kos sync: Terms"
php bin/console app:kos-api:import-terms
echo "Kos sync: Subjects"
php bin/console app:kos-api:import-subjects
echo "Kos sync: Courses"
php bin/console app:kos-api:import-courses
echo "Kos sync: Course members"
php bin/console app:kos-api:import-course-members
echo "Kos sync finished"
