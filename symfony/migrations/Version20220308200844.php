<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220308200844 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TYPE CourseRoleType AS ENUM (\'Teacher\', \'Student\')');
        $this->addSql('CREATE TABLE course (id UUID NOT NULL, subject_id UUID NOT NULL, term_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_169E6FB923EDC87 ON course (subject_id)');
        $this->addSql('CREATE INDEX IDX_169E6FB9E2C35FC ON course (term_id)');
        $this->addSql('COMMENT ON COLUMN course.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN course.subject_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN course.term_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE course_member (id UUID NOT NULL, user_id UUID NOT NULL, course_id UUID NOT NULL, role CourseRoleType, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DEDC3B35A76ED395 ON course_member (user_id)');
        $this->addSql('CREATE INDEX IDX_DEDC3B35591CC992 ON course_member (course_id)');
        $this->addSql('COMMENT ON COLUMN course_member.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN course_member.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN course_member.course_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE course_study_group (id UUID NOT NULL, code VARCHAR(31) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN course_study_group.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE course_study_group_course (course_study_group_id UUID NOT NULL, course_id UUID NOT NULL, PRIMARY KEY(course_study_group_id, course_id))');
        $this->addSql('CREATE INDEX IDX_7393200FA2112BFA ON course_study_group_course (course_study_group_id)');
        $this->addSql('CREATE INDEX IDX_7393200F591CC992 ON course_study_group_course (course_id)');
        $this->addSql('COMMENT ON COLUMN course_study_group_course.course_study_group_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN course_study_group_course.course_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE course_study_group_course_member (course_study_group_id UUID NOT NULL, course_member_id UUID NOT NULL, PRIMARY KEY(course_study_group_id, course_member_id))');
        $this->addSql('CREATE INDEX IDX_971F3A96A2112BFA ON course_study_group_course_member (course_study_group_id)');
        $this->addSql('CREATE INDEX IDX_971F3A96F7369352 ON course_study_group_course_member (course_member_id)');
        $this->addSql('COMMENT ON COLUMN course_study_group_course_member.course_study_group_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN course_study_group_course_member.course_member_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE subject (id UUID NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(31) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FBCE3E7A77153098 ON subject (code)');
        $this->addSql('COMMENT ON COLUMN subject.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE term (id UUID NOT NULL, code VARCHAR(31) NOT NULL, starts_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ends_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A50FE78D77153098 ON term (code)');
        $this->addSql('COMMENT ON COLUMN term.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN term.starts_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN term.ends_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "user" (id UUID NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, title_before_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, title_after_name VARCHAR(255) NOT NULL, email VARCHAR(1023) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON "user" (username)');
        $this->addSql('COMMENT ON COLUMN "user".id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB923EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB9E2C35FC FOREIGN KEY (term_id) REFERENCES term (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_member ADD CONSTRAINT FK_DEDC3B35A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_member ADD CONSTRAINT FK_DEDC3B35591CC992 FOREIGN KEY (course_id) REFERENCES course (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_study_group_course ADD CONSTRAINT FK_7393200FA2112BFA FOREIGN KEY (course_study_group_id) REFERENCES course_study_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_study_group_course ADD CONSTRAINT FK_7393200F591CC992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_study_group_course_member ADD CONSTRAINT FK_971F3A96A2112BFA FOREIGN KEY (course_study_group_id) REFERENCES course_study_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_study_group_course_member ADD CONSTRAINT FK_971F3A96F7369352 FOREIGN KEY (course_member_id) REFERENCES course_member (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE course_member DROP CONSTRAINT FK_DEDC3B35591CC992');
        $this->addSql('ALTER TABLE course_study_group_course DROP CONSTRAINT FK_7393200F591CC992');
        $this->addSql('ALTER TABLE course_study_group_course_member DROP CONSTRAINT FK_971F3A96F7369352');
        $this->addSql('ALTER TABLE course_study_group_course DROP CONSTRAINT FK_7393200FA2112BFA');
        $this->addSql('ALTER TABLE course_study_group_course_member DROP CONSTRAINT FK_971F3A96A2112BFA');
        $this->addSql('ALTER TABLE course DROP CONSTRAINT FK_169E6FB923EDC87');
        $this->addSql('ALTER TABLE course DROP CONSTRAINT FK_169E6FB9E2C35FC');
        $this->addSql('ALTER TABLE course_member DROP CONSTRAINT FK_DEDC3B35A76ED395');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE course_member');
        $this->addSql('DROP TABLE course_study_group');
        $this->addSql('DROP TABLE course_study_group_course');
        $this->addSql('DROP TABLE course_study_group_course_member');
        $this->addSql('DROP TABLE subject');
        $this->addSql('DROP TABLE term');
        $this->addSql('DROP TABLE "user"');
    }
}
