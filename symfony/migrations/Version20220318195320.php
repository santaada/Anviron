<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220318195320 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE course_member ALTER role SET NOT NULL');
        $this->addSql('ALTER TABLE course_member ALTER role TYPE VARCHAR(31)');
        $this->addSql('ALTER TABLE "user" DROP password');
        $this->addSql('ALTER TABLE "user" ALTER title_before_name DROP NOT NULL');
        $this->addSql('ALTER TABLE "user" ALTER title_after_name DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "user" ADD password VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE "user" ALTER title_before_name SET NOT NULL');
        $this->addSql('ALTER TABLE "user" ALTER title_after_name SET NOT NULL');
        $this->addSql('ALTER TABLE course_member ALTER role DROP NOT NULL');
        $this->addSql('ALTER TABLE course_member ALTER role TYPE VARCHAR(255)');
    }
}
