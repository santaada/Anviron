<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220410152746 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE environment_factory (id UUID NOT NULL, maintained_by_id UUID NOT NULL, environment_image_id UUID NOT NULL, startup_script_path VARCHAR(2047) NOT NULL, shutdown_script_path VARCHAR(2047) NOT NULL, user_docs TEXT DEFAULT NULL, is_visible_for_course_students BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C997A975884191EA ON environment_factory (maintained_by_id)');
        $this->addSql('CREATE INDEX IDX_C997A975D9994BD1 ON environment_factory (environment_image_id)');
        $this->addSql('COMMENT ON COLUMN environment_factory.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_factory.maintained_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_factory.environment_image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE environment_factory_course (environment_factory_id UUID NOT NULL, course_id UUID NOT NULL, PRIMARY KEY(environment_factory_id, course_id))');
        $this->addSql('CREATE INDEX IDX_78D6D1C25D56E05 ON environment_factory_course (environment_factory_id)');
        $this->addSql('CREATE INDEX IDX_78D6D1C591CC992 ON environment_factory_course (course_id)');
        $this->addSql('COMMENT ON COLUMN environment_factory_course.environment_factory_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_factory_course.course_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE environment_factory_port (id UUID NOT NULL, environment_factory_id UUID NOT NULL, external_port INT NOT NULL, connection_type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FD81D69525D56E05 ON environment_factory_port (environment_factory_id)');
        $this->addSql('COMMENT ON COLUMN environment_factory_port.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_factory_port.environment_factory_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE environment_factory_volume (id UUID NOT NULL, environment_factory_id UUID NOT NULL, external_path VARCHAR(2047) NOT NULL, internal_path VARCHAR(2047) NOT NULL, is_read_only BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A889CF7B25D56E05 ON environment_factory_volume (environment_factory_id)');
        $this->addSql('COMMENT ON COLUMN environment_factory_volume.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_factory_volume.environment_factory_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE environment_image (id UUID NOT NULL, registry_url VARCHAR(2047) DEFAULT NULL, dockerfile_path VARCHAR(2047) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN environment_image.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE environment_runtime (id UUID NOT NULL, environment_factory_id UUID NOT NULL, used_by_id UUID NOT NULL, url VARCHAR(2047) NOT NULL, started_up_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, shut_down_scheduled_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, shut_down_executed_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E707BBF725D56E05 ON environment_runtime (environment_factory_id)');
        $this->addSql('CREATE INDEX IDX_E707BBF74C2B72A8 ON environment_runtime (used_by_id)');
        $this->addSql('COMMENT ON COLUMN environment_runtime.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_runtime.environment_factory_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_runtime.used_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_runtime.started_up_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN environment_runtime.shut_down_scheduled_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN environment_runtime.shut_down_executed_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE environment_runtime_port (id UUID NOT NULL, environment_runtime_id UUID DEFAULT NULL, factory_port_id UUID DEFAULT NULL, external_port INT NOT NULL, allocated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3AA31BAF4F9F4252 ON environment_runtime_port (environment_runtime_id)');
        $this->addSql('CREATE INDEX IDX_3AA31BAF38A3A6EF ON environment_runtime_port (factory_port_id)');
        $this->addSql('COMMENT ON COLUMN environment_runtime_port.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_runtime_port.environment_runtime_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_runtime_port.factory_port_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_runtime_port.allocated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE environment_factory ADD CONSTRAINT FK_C997A975884191EA FOREIGN KEY (maintained_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE environment_factory ADD CONSTRAINT FK_C997A975D9994BD1 FOREIGN KEY (environment_image_id) REFERENCES environment_image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE environment_factory_course ADD CONSTRAINT FK_78D6D1C25D56E05 FOREIGN KEY (environment_factory_id) REFERENCES environment_factory (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE environment_factory_course ADD CONSTRAINT FK_78D6D1C591CC992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE environment_factory_port ADD CONSTRAINT FK_FD81D69525D56E05 FOREIGN KEY (environment_factory_id) REFERENCES environment_factory (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE environment_factory_volume ADD CONSTRAINT FK_A889CF7B25D56E05 FOREIGN KEY (environment_factory_id) REFERENCES environment_factory (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE environment_runtime ADD CONSTRAINT FK_E707BBF725D56E05 FOREIGN KEY (environment_factory_id) REFERENCES environment_factory (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE environment_runtime ADD CONSTRAINT FK_E707BBF74C2B72A8 FOREIGN KEY (used_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE environment_runtime_port ADD CONSTRAINT FK_3AA31BAF4F9F4252 FOREIGN KEY (environment_runtime_id) REFERENCES environment_runtime (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE environment_runtime_port ADD CONSTRAINT FK_3AA31BAF38A3A6EF FOREIGN KEY (factory_port_id) REFERENCES environment_factory_port (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE environment_factory_course DROP CONSTRAINT FK_78D6D1C25D56E05');
        $this->addSql('ALTER TABLE environment_factory_port DROP CONSTRAINT FK_FD81D69525D56E05');
        $this->addSql('ALTER TABLE environment_factory_volume DROP CONSTRAINT FK_A889CF7B25D56E05');
        $this->addSql('ALTER TABLE environment_runtime DROP CONSTRAINT FK_E707BBF725D56E05');
        $this->addSql('ALTER TABLE environment_runtime_port DROP CONSTRAINT FK_3AA31BAF38A3A6EF');
        $this->addSql('ALTER TABLE environment_factory DROP CONSTRAINT FK_C997A975D9994BD1');
        $this->addSql('ALTER TABLE environment_runtime_port DROP CONSTRAINT FK_3AA31BAF4F9F4252');
        $this->addSql('DROP TABLE environment_factory');
        $this->addSql('DROP TABLE environment_factory_course');
        $this->addSql('DROP TABLE environment_factory_port');
        $this->addSql('DROP TABLE environment_factory_volume');
        $this->addSql('DROP TABLE environment_image');
        $this->addSql('DROP TABLE environment_runtime');
        $this->addSql('DROP TABLE environment_runtime_port');
    }
}
