<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220410213106 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE environment_factory ADD startup_script TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE environment_factory ADD shutdown_script TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE environment_factory DROP startup_script_path');
        $this->addSql('ALTER TABLE environment_factory DROP shutdown_script_path');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE environment_factory ADD startup_script_path TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE environment_factory ADD shutdown_script_path TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE environment_factory DROP startup_script');
        $this->addSql('ALTER TABLE environment_factory DROP shutdown_script');
    }
}
