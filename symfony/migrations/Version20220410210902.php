<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220410210902 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE environment_factory ALTER startup_script_path TYPE TEXT');
        $this->addSql('ALTER TABLE environment_factory ALTER startup_script_path DROP DEFAULT');
        $this->addSql('ALTER TABLE environment_factory ALTER startup_script_path DROP NOT NULL');
        $this->addSql('ALTER TABLE environment_factory ALTER startup_script_path TYPE TEXT');
        $this->addSql('ALTER TABLE environment_factory ALTER shutdown_script_path TYPE TEXT');
        $this->addSql('ALTER TABLE environment_factory ALTER shutdown_script_path DROP DEFAULT');
        $this->addSql('ALTER TABLE environment_factory ALTER shutdown_script_path DROP NOT NULL');
        $this->addSql('ALTER TABLE environment_factory ALTER shutdown_script_path TYPE TEXT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE environment_factory ALTER startup_script_path TYPE VARCHAR(2047)');
        $this->addSql('ALTER TABLE environment_factory ALTER startup_script_path DROP DEFAULT');
        $this->addSql('ALTER TABLE environment_factory ALTER startup_script_path SET NOT NULL');
        $this->addSql('ALTER TABLE environment_factory ALTER shutdown_script_path TYPE VARCHAR(2047)');
        $this->addSql('ALTER TABLE environment_factory ALTER shutdown_script_path DROP DEFAULT');
        $this->addSql('ALTER TABLE environment_factory ALTER shutdown_script_path SET NOT NULL');
    }
}
