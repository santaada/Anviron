<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230220142306 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE course_study_group_course DROP CONSTRAINT fk_7393200fa2112bfa');
        $this->addSql('ALTER TABLE course_study_group_course_member DROP CONSTRAINT fk_971f3a96a2112bfa');
        $this->addSql('DROP TABLE course_study_group');
        $this->addSql('DROP TABLE course_study_group_course');
        $this->addSql('DROP TABLE course_study_group_course_member');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE course_study_group (id UUID NOT NULL, code VARCHAR(31) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN course_study_group.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE course_study_group_course (course_study_group_id UUID NOT NULL, course_id UUID NOT NULL, PRIMARY KEY(course_study_group_id, course_id))');
        $this->addSql('CREATE INDEX idx_7393200f591cc992 ON course_study_group_course (course_id)');
        $this->addSql('CREATE INDEX idx_7393200fa2112bfa ON course_study_group_course (course_study_group_id)');
        $this->addSql('COMMENT ON COLUMN course_study_group_course.course_study_group_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN course_study_group_course.course_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE course_study_group_course_member (course_study_group_id UUID NOT NULL, course_member_id UUID NOT NULL, PRIMARY KEY(course_study_group_id, course_member_id))');
        $this->addSql('CREATE INDEX idx_971f3a96a2112bfa ON course_study_group_course_member (course_study_group_id)');
        $this->addSql('CREATE INDEX idx_971f3a96f7369352 ON course_study_group_course_member (course_member_id)');
        $this->addSql('COMMENT ON COLUMN course_study_group_course_member.course_study_group_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN course_study_group_course_member.course_member_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE course_study_group_course ADD CONSTRAINT fk_7393200fa2112bfa FOREIGN KEY (course_study_group_id) REFERENCES course_study_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_study_group_course ADD CONSTRAINT fk_7393200f591cc992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_study_group_course_member ADD CONSTRAINT fk_971f3a96a2112bfa FOREIGN KEY (course_study_group_id) REFERENCES course_study_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_study_group_course_member ADD CONSTRAINT fk_971f3a96f7369352 FOREIGN KEY (course_member_id) REFERENCES course_member (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
