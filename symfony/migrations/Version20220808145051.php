<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220808145051 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE environment_image_build (id UUID NOT NULL, environment_image_id UUID NOT NULL, tag VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2B04B4C389B783 ON environment_image_build (tag)');
        $this->addSql('CREATE INDEX IDX_B2B04B4CD9994BD1 ON environment_image_build (environment_image_id)');
        $this->addSql('COMMENT ON COLUMN environment_image_build.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_image_build.environment_image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN environment_image_build.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE environment_image_build ADD CONSTRAINT FK_B2B04B4CD9994BD1 FOREIGN KEY (environment_image_id) REFERENCES environment_image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE environment_factory DROP CONSTRAINT fk_c997a975d9994bd1');
        $this->addSql('DROP INDEX idx_c997a975d9994bd1');
        $this->addSql('ALTER TABLE environment_factory RENAME COLUMN environment_image_id TO environment_image_build_id');
        $this->addSql('ALTER TABLE environment_factory ADD CONSTRAINT FK_C997A975C72D9464 FOREIGN KEY (environment_image_build_id) REFERENCES environment_image_build (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_C997A975C72D9464 ON environment_factory (environment_image_build_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE environment_factory DROP CONSTRAINT FK_C997A975C72D9464');
        $this->addSql('DROP TABLE environment_image_build');
        $this->addSql('DROP INDEX IDX_C997A975C72D9464');
        $this->addSql('ALTER TABLE environment_factory RENAME COLUMN environment_image_build_id TO environment_image_id');
        $this->addSql('ALTER TABLE environment_factory ADD CONSTRAINT fk_c997a975d9994bd1 FOREIGN KEY (environment_image_id) REFERENCES environment_image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_c997a975d9994bd1 ON environment_factory (environment_image_id)');
    }
}
