<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220602095038 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE environment_runtime ADD build_stdout TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE environment_runtime ADD build_stderr TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE environment_runtime ADD shutdown_stdout TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE environment_runtime ADD shutdown_stderr TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE environment_runtime DROP build_stdout');
        $this->addSql('ALTER TABLE environment_runtime DROP build_stderr');
        $this->addSql('ALTER TABLE environment_runtime DROP shutdown_stdout');
        $this->addSql('ALTER TABLE environment_runtime DROP shutdown_stderr');
    }
}
