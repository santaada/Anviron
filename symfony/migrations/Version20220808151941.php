<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220808151941 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE environment_image_build ADD created_by_id UUID NOT NULL');
        $this->addSql('COMMENT ON COLUMN environment_image_build.created_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE environment_image_build ADD CONSTRAINT FK_B2B04B4CB03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B2B04B4CB03A8386 ON environment_image_build (created_by_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE environment_image_build DROP CONSTRAINT FK_B2B04B4CB03A8386');
        $this->addSql('DROP INDEX IDX_B2B04B4CB03A8386');
        $this->addSql('ALTER TABLE environment_image_build DROP created_by_id');
    }
}
