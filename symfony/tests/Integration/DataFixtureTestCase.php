<?php declare(strict_types=1);

namespace Tests\Integration;

use App\Kernel;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * @inspiredBy https://joeymasip.medium.com/symfony-phpunit-testing-database-data-322383ed0603
 */
abstract class DataFixtureTestCase extends KernelTestCase
{
    protected static ?Application $application = null;
    protected ContainerInterface $container;
    protected ?EntityManager $entityManager; 
    
    public function setUp(): void
    {
        parent::setUp();
        self::runCommand('doctrine:database:create --env=test');
        self::runCommand('doctrine:migrations:migrate --env=test --no-interaction');
        self::runCommand('doctrine:fixtures:load --env=test --no-interaction');

        $this->container = static::getContainer();
        $doctrine = $this->container->get('doctrine');
        assert($doctrine instanceof Registry);
        $manager = $doctrine->getManager();
        assert($manager instanceof EntityManager);
        $this->entityManager = $manager;
    }

    protected static function runCommand(string $command): int
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication(): Application
    {
        if (null === self::$application) {
            self::$application = new Application(self::createKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }
    
    protected function tearDown(): void
    {
        self::runCommand('doctrine:database:drop --env=test --force');

        parent::tearDown();

        $this->entityManager?->close();
        $this->entityManager = null; // avoid memory leaks
    }
}