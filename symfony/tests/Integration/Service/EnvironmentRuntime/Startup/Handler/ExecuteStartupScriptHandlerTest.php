<?php declare(strict_types=1);

namespace Tests\Integration\Service\EnvironmentRuntime\Startup\Handler;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentFactoryVolume;
use App\Entity\EnvironmentRuntime;
use App\Entity\User;
use App\Integration\DockerApi\DockerEngineSocketApi;
use App\Repository\EnvironmentFactoryRepository;
use App\Repository\EnvironmentFactoryVolumeRepository;
use App\Repository\EnvironmentRuntimeRepository;
use App\Repository\UserRepository;
use App\Service\EnvironmentFactoryVolumeService;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\Startup\Handler\ExecuteStartupScriptHandler;
use Doctrine\ORM\EntityManager;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;
use Tests\Integration\DataFixtureTestCase;


class ExecuteStartupScriptHandlerTest extends DataFixtureTestCase
{
    protected const SYSTEM_VOLUME_CONTAINER_PATH = '/home/.anviron';
    protected const PHYSICAL_SYSTEM_VOLUME_PATH = '/var/www/var/anviron/volume';
    
    public function testSuccesfulExecution(): void
    {
        $this->assertTrue(true);
        $userRepository = $this->entityManager?->getRepository(User::class);
        assert($userRepository instanceof UserRepository);
        $user = $userRepository->findOneBy(['username' => 'reachjoe']);
        assert($user instanceof User);
        
        $factoryRepository = $this->entityManager?->getRepository(EnvironmentFactory::class);
        assert($factoryRepository instanceof EnvironmentFactoryRepository);
        $factory = $factoryRepository->findOneBy(['name' => 'Sample environment for C compilation']);
        assert($factory instanceof EnvironmentFactory);
        
        $runtimeRepository = $this->entityManager?->getRepository(EnvironmentRuntime::class);
        assert($runtimeRepository instanceof EnvironmentRuntimeRepository);
        $runtime = $runtimeRepository->findOneBy([
            'usedBy' => $user,
            'environmentFactory' => $factory,
            'shutDownExecutedAt' => null,
        ]);
        assert($runtime instanceof EnvironmentRuntime);
        
        $request = new ChainOfResponsibilityRequest(
            $user,
            $factory,
            $runtime
        );
        
        // Build EntityManager mock
        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist', 'flush'])
            ->getMock();
        $entityManagerMock
            ->expects($this->once())
            ->method('persist')
            ->with($request->runtime);
        
        // Build DockerEngineSocketApi mock
        $dockerEngineApiMock = $this->getMockBuilder(DockerEngineSocketApi::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['containerExec'])
            ->getMock();
        $dockerEngineApiMock
            ->expects($this->once())
            ->method('containerExec')
            ->with($request->runtime);
        
        // Build other dependencies
        $startupScriptHandler = $this->container->get(ExecuteStartupScriptHandler::class);
        $this->assertTrue($startupScriptHandler instanceof ExecuteStartupScriptHandler);
        
        $volumeRepository = $this->entityManager?->getRepository(EnvironmentFactoryVolume::class);
        assert($volumeRepository instanceof EnvironmentFactoryVolumeRepository);

        $volumeService = $this->container->get(EnvironmentFactoryVolumeService::class);
        assert($volumeService instanceof EnvironmentFactoryVolumeService);
        
        // Add some junk data to system volume to ensure cleanup happens
        $userDir = self::PHYSICAL_SYSTEM_VOLUME_PATH . "/" . $user->getId()->toRfc4122() . "_" . Strings::webalize($user->getUsername());
        $factoryDir = $userDir . '/' . $factory->getId() . "_" . Strings::webalize($factory->getName());
        $volumeDirs = array_map(
            fn (EnvironmentFactoryVolume $volume) => $factoryDir . '/' . Strings::webalize($volume->getId()?->toRfc4122() ?? ""), 
            $factory->getEnvironmentFactoryVolumes()->toArray()
        );
        
        $this->assertDirectoryDoesNotExist($userDir);
        $this->assertDirectoryDoesNotExist($factoryDir);
        array_map(fn(string $volumeDir) => $this->assertDirectoryDoesNotExist($volumeDir), $volumeDirs);
        
        FileSystem::createDir($userDir);
        FileSystem::createDir($factoryDir);
        array_map(fn(string $volumeDir) => FileSystem::createDir($volumeDir), $volumeDirs);
        
        $this->assertDirectoryExists($userDir);
        $this->assertDirectoryExists($factoryDir);
        array_map(fn(string $volumeDir) => $this->assertDirectoryExists($volumeDir), $volumeDirs);

        $junk = random_bytes(64);
        array_map(fn(string $volumeDir) => FileSystem::write($volumeDir . '/' . 'exec.sh', $junk), $volumeDirs);
        
        // Execute test case
        $handler = new ExecuteStartupScriptHandler(
            $entityManagerMock, 
            $dockerEngineApiMock, 
            $volumeRepository, 
            $volumeService, 
            self::SYSTEM_VOLUME_CONTAINER_PATH,
        );
        
        $response = $handler->handle($request);
        
        // Assertions
        $this->assertTrue( is_string($response->request->runtime?->getBuildStdout()) );
        $this->assertTrue( is_string($response->request->runtime?->getBuildStderr()) );
        
        // Cleanup
        array_map(fn(string $volumeDir) => FileSystem::delete($volumeDir), $volumeDirs);
        FileSystem::delete($factoryDir);
        FileSystem::delete($userDir);
        
        $this->assertDirectoryDoesNotExist($userDir);
        $this->assertDirectoryDoesNotExist($factoryDir);
        array_map(fn(string $volumeDir) => $this->assertDirectoryDoesNotExist($volumeDir), $volumeDirs);
    }
}
