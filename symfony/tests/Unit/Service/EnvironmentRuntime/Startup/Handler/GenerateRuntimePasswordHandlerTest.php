<?php declare(strict_types=1);


namespace Tests\Unit\Service\EnvironmentRuntime\Startup\Handler;

use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntime\Startup\Handler\GenerateRuntimePasswordHandler;
use Tests\Unit\Service\EnvironmentRuntime\AbstractHandlerTest;

final class GenerateRuntimePasswordHandlerTest extends AbstractHandlerTest
{
    public function testHandle(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);

        $this->assertTrue($request->runtime?->getPassword() === null);
        
        $handler = new GenerateRuntimePasswordHandler();
        $result = $handler->handle($request);
        
        $this->assertTrue($result instanceof ChainOfResponsibilityResponse);
        $this->assertTrue( $result->state === ChainOfResponsibilityResponse::SUCCESS);        
        $this->assertTrue($result->message === '');
        $this->assertTrue($result->request->runtime?->getPassword() !== null);
        $this->assertTrue($result->request->runtime?->getPassword() !== '');
    }
}
