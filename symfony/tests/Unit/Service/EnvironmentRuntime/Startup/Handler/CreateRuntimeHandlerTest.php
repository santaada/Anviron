<?php declare(strict_types=1);


namespace Tests\Unit\Service\EnvironmentRuntime\Startup\Handler;

use App\Entity\EnvironmentFactoryPort;
use App\Entity\EnvironmentFactoryVolume;
use App\Entity\EnvironmentRuntime;
use App\Repository\EnvironmentFactoryPortRepository;
use App\Repository\EnvironmentFactoryVolumeRepository;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntime\Startup\Handler\CreateRuntimeHandler;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\Unit\Service\EnvironmentRuntime\AbstractHandlerTest;
use Exception;

final class CreateRuntimeHandlerTest extends AbstractHandlerTest
{
    /**
     * @phpstan-var MockObject&EnvironmentFactoryPortRepository
     */
    protected MockObject $factoryPortRepositoryMock;
    /**
     * @phpstan-var MockObject&EnvironmentFactoryVolumeRepository
     */
    protected MockObject $factoryVolumeRepositoryMock;
    
    protected function setUp(): void
    {
        parent::setUp();
        $this->factoryPortRepositoryMock = $this->getMockBuilder(EnvironmentFactoryPortRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->factoryVolumeRepositoryMock = $this->getMockBuilder(EnvironmentFactoryVolumeRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
    
    public function testHandleWithSuccessfulResponse(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        
        $request->runtime = null; // Make sure no runtime is present in req., so that the handler creates it

        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist', 'flush'])
            ->getMock();

        $entityManagerMock
            ->expects($this->once())
            ->method('persist');
        
        $entityManagerMock
            ->expects($this->once())
            ->method('flush');
        
        $this->factoryPortRepositoryMock->method('findBy')
            ->willReturn([
                $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)->getFakeInstance()
            ]);
        $this->factoryVolumeRepositoryMock->method('findBy')
            ->willReturn([
                $this->faker->getFakerForEntity(EnvironmentFactoryVolume::class)->getFakeInstance()
            ]);

        $handler = new CreateRuntimeHandler(
            $entityManagerMock, 
            $this->factoryPortRepositoryMock, 
            $this->factoryVolumeRepositoryMock
        );
        $response = $handler->handle($request);
        
        $this->assertTrue($response->request->runtime instanceof EnvironmentRuntime);
        $this->assertTrue($response->state === ChainOfResponsibilityResponse::SUCCESS);
        $this->assertTrue($response->message === '');
    }

    public function testHandleWithUnsuccessfulResponse_UnableToPersist(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        $request->runtime = null; // Make sure no runtime is present in req., so that the handler creates it

        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist', 'flush'])
            ->getMock();

        $entityManagerMock
            ->expects($this->once())
            ->method('persist')
            ->willThrowException(new Exception('Unable to persist for some reason.'));

        $this->factoryPortRepositoryMock->method('findBy')
            ->willReturn([
                $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)->getFakeInstance()
            ]);
        $this->factoryVolumeRepositoryMock->method('findBy')
            ->willReturn([
                $this->faker->getFakerForEntity(EnvironmentFactoryVolume::class)->getFakeInstance()
            ]);

        $handler = new CreateRuntimeHandler(
            $entityManagerMock,
            $this->factoryPortRepositoryMock,
            $this->factoryVolumeRepositoryMock
        );
        $response = $handler->handle($request);

        $this->assertTrue($response->state === ChainOfResponsibilityResponse::FAIL);
        $this->assertTrue($response->message === 'Unable to persist for some reason.');
    }

    public function testHandleWithUnsuccessfulResponse_NonDistinctPorts(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);

        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist', 'flush'])
            ->getMock();
        
        $factoryPortOne = $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)
            ->getFakeInstance();
        assert($factoryPortOne instanceof EnvironmentFactoryPort);
        $factoryPortOne->setExternalPort(8443);
        
        $factoryPortTwo = $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)
            ->getFakeInstance();
        assert($factoryPortTwo instanceof EnvironmentFactoryPort);
        $factoryPortOne->setExternalPort(8444);
        
        $this->factoryPortRepositoryMock->method('findBy')
            ->willReturn([$factoryPortOne, $factoryPortTwo]);
        $this->factoryVolumeRepositoryMock->method('findBy')
            ->willReturn([
                $this->faker->getFakerForEntity(EnvironmentFactoryVolume::class)->getFakeInstance()
            ]);
        
        $handler = new CreateRuntimeHandler(
            $entityManagerMock,
            $this->factoryPortRepositoryMock,
            $this->factoryVolumeRepositoryMock
        );
        $response = $handler->handle($request);

        $this->assertTrue($response->state === ChainOfResponsibilityResponse::FAIL);
    }

    public function testHandleWithUnsuccessfulResponse_MultipleEntryPorts(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);

        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist', 'flush'])
            ->getMock();

        $factoryPortOne = $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)
            ->getFakeInstance();
        assert($factoryPortOne instanceof EnvironmentFactoryPort);
        $factoryPortOne->setExternalPort(8443);
        $factoryPortOne->setIsEntryPort(true);

        $factoryPortTwo = $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)
            ->getFakeInstance();
        assert($factoryPortTwo instanceof EnvironmentFactoryPort);
        $factoryPortOne->setExternalPort(8444);
        $factoryPortOne->setIsEntryPort(true);

        $this->factoryPortRepositoryMock->method('findBy')
            ->willReturn([$factoryPortOne, $factoryPortTwo]);
        $this->factoryVolumeRepositoryMock->method('findBy')
            ->willReturn([
                $this->faker->getFakerForEntity(EnvironmentFactoryVolume::class)->getFakeInstance()
            ]);

        $handler = new CreateRuntimeHandler(
            $entityManagerMock,
            $this->factoryPortRepositoryMock,
            $this->factoryVolumeRepositoryMock
        );
        $response = $handler->handle($request);

        $this->assertTrue($response->state === ChainOfResponsibilityResponse::FAIL);
    }

    public function testHandleWithUnsuccessfulResponse_NoEntryPorts(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);

        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist', 'flush'])
            ->getMock();

        $this->factoryPortRepositoryMock->method('findBy')
            ->willReturn([]);
        $this->factoryVolumeRepositoryMock->method('findBy')
            ->willReturn([
                $this->faker->getFakerForEntity(EnvironmentFactoryVolume::class)->getFakeInstance()
            ]);

        $handler = new CreateRuntimeHandler(
            $entityManagerMock,
            $this->factoryPortRepositoryMock,
            $this->factoryVolumeRepositoryMock
        );
        $response = $handler->handle($request);

        $this->assertTrue($response->state === ChainOfResponsibilityResponse::FAIL);
    }


    public function testHandleWithUnsuccessfulResponse_NonDistinctVolumes(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);

        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist', 'flush'])
            ->getMock();

        $this->factoryPortRepositoryMock->method('findBy')
            ->willReturn([
                $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)->getFakeInstance()
            ]);
        $this->factoryVolumeRepositoryMock->method('findBy')
            ->willReturn([
                $this->faker->getFakerForEntity(EnvironmentFactoryVolume::class)->getFakeInstance(),
                $this->faker->getFakerForEntity(EnvironmentFactoryVolume::class)->getFakeInstance()
            ]);

        $handler = new CreateRuntimeHandler(
            $entityManagerMock,
            $this->factoryPortRepositoryMock,
            $this->factoryVolumeRepositoryMock
        );
        $response = $handler->handle($request);

        $this->assertTrue($response->state === ChainOfResponsibilityResponse::FAIL);
    }

    public function testUnhandle(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);

        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['remove', 'flush'])
            ->getMock();

        $entityManagerMock
            ->expects($this->once())
            ->method('remove')
            ->with($request->runtime);

        $entityManagerMock
            ->expects($this->once())
            ->method('flush');

        $handler = new CreateRuntimeHandler(
            $entityManagerMock,
            $this->factoryPortRepositoryMock,
            $this->factoryVolumeRepositoryMock
        );
        $handler->unhandle($request);
    }
}
