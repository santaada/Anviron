<?php declare(strict_types=1);


namespace Tests\Unit\Service\EnvironmentRuntime\Startup\Handler;

use App\Entity\EnvironmentFactoryPort;
use App\Entity\EnvironmentRuntime;
use App\Entity\EnvironmentRuntimePort;
use App\Repository\EnvironmentRuntimePortRepository;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntime\Startup\Handler\AllocatePortsHandler;
use App\Service\EnvironmentRuntimePortService;
use Psr\Log\LoggerInterface;
use Tests\Unit\Service\EnvironmentRuntime\AbstractHandlerTest;


final class AllocatePortsHandlerTest extends AbstractHandlerTest
{
    public function testHandleWithSuccessfulResponse(): void
    {
        // Build LoggerInterface mock
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->getMock();
        $loggerMock->expects($this->never())
            ->method('error');
        
        // Build EnvironmentRuntimePortService mock
        $runtimePort = new EnvironmentRuntimePort();
        $runtimePort->setExternalPort(49123);
        
        $runtimePortOperationMock = $this->getMockBuilder(EnvironmentRuntimePortService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $runtimePortOperationMock->expects($this->once())
            ->method('allocate')
            ->withConsecutive([
                $this->isInstanceOf(EnvironmentRuntime::class), 
                $this->isInstanceOf(EnvironmentFactoryPort::class),
            ])
            ->willReturn($runtimePort);
        
        // Build EnvironmentRuntimePortRepository mock
        $runtimePortRepositoryMock = $this->getMockBuilder(EnvironmentRuntimePortRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        // Build ChainOfResponsibilityRequest
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        
        $factoryPort = $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)->getFakeInstance();
        assert($factoryPort instanceof EnvironmentFactoryPort);
        
        $request->factory->addEnvironmentFactoryPort($factoryPort);
        
        $handler = new AllocatePortsHandler($runtimePortOperationMock, $runtimePortRepositoryMock, $loggerMock, 'localhost');

        $response = $handler->handle($request);
        $this->assertTrue($response->state === ChainOfResponsibilityResponse::SUCCESS);
    }

    public function testHandleWithUnsuccessfulResponse(): void
    {        
        // Build LoggerIntefrace mock
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->getMock();
        $loggerMock->expects($this->once())
            ->method('error');

        // Build EnvironmentRuntimePortService mock
        $runtimePortOperationMock = $this->getMockBuilder(EnvironmentRuntimePortService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $runtimePortOperationMock->expects($this->once())
            ->method('allocate')
            ->withConsecutive([
                $this->isInstanceOf(EnvironmentRuntime::class),
                $this->isInstanceOf(EnvironmentFactoryPort::class),
            ])
            ->willReturn(null);

        // Build EnvironmentRuntimePortRepository mock
        $runtimePortRepositoryMock = $this->getMockBuilder(EnvironmentRuntimePortRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        // Build ChainOfResponsibilityRequest
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        
        $factoryPort = $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)->getFakeInstance();
        assert($factoryPort instanceof EnvironmentFactoryPort);
        
        $request->factory->addEnvironmentFactoryPort($factoryPort);

        $handler = new AllocatePortsHandler($runtimePortOperationMock, $runtimePortRepositoryMock, $loggerMock, 'localhost');

        $response = $handler->handle($request);
        $this->assertTrue($response->state === ChainOfResponsibilityResponse::FAIL);
    }

    public function testUnhandle(): void
    {
        // Build LoggerInterface mock
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->getMock();
        $loggerMock->expects($this->never())
            ->method('error');
        
        // Build EnvironmentRuntimePortService mock
        $runtimePortOperationMock = $this->getMockBuilder(EnvironmentRuntimePortService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $runtimePortOperationMock->expects($this->once())
            ->method('free')
            ->withConsecutive([
                $this->isInstanceOf(EnvironmentRuntimePort::class),
            ]);

        // Build ChainOfResponsibilityRequest
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        $factoryPort = $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)->getFakeInstance();
        assert($factoryPort instanceof EnvironmentFactoryPort);
        $request->factory->addEnvironmentFactoryPort($factoryPort);
        
        $runtimePort = $this->faker->getFakerForEntity(EnvironmentRuntimePort::class)->getFakeInstance();

        // Build EnvironmentRuntimePortRepository mock
        $runtimePortRepositoryMock = $this->getMockBuilder(EnvironmentRuntimePortRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $runtimePortRepositoryMock->expects($this->once())
            ->method('findBy')
            ->withConsecutive([
                $this->arrayHasKey('environmentRuntime'),
            ])
            ->willReturn([$runtimePort]);
        
        $handler = new AllocatePortsHandler($runtimePortOperationMock, $runtimePortRepositoryMock, $loggerMock, 'localhost');
        $handler->unhandle($request);
    }
}
