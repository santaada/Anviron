<?php declare(strict_types=1);

namespace Tests\Unit\Service\EnvironmentRuntime\Startup\Handler;


use App\Entity\EnvironmentFactoryPort;
use App\Entity\EnvironmentRuntime;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntime\Startup\Handler\SameFactoryCheckHandler;
use App\Repository\EnvironmentRuntimeRepository;
use Tests\Unit\Service\EnvironmentRuntime\AbstractHandlerTest;


final class SameFactoryCheckHandlerTest extends AbstractHandlerTest
{
    public function testHandleWithSuccessfulResponse(): void
    {
        // Build ChainOfResponsibilityRequest
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        $factoryPort = $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)->getFakeInstance();
        assert($factoryPort instanceof EnvironmentFactoryPort);
        $request->factory->addEnvironmentFactoryPort($factoryPort);

        // Build EnvironmentRuntimeRepository mock
        $runtimeRepositoryMock = $this->getMockBuilder(EnvironmentRuntimeRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $runtimeRepositoryMock->expects($this->once())
            ->method('findOneBy')
            ->withConsecutive([[
                'usedBy' => $request->user,
                'environmentFactory' => $request->factory,
                'shutDownExecutedAt' => null
            ]])
            ->willReturn(null);

        // Perform test case
        $handler = new SameFactoryCheckHandler($runtimeRepositoryMock);
        $res = $handler->handle($request);
        
        $this->assertTrue($res instanceof ChainOfResponsibilityResponse);
        $this->assertTrue($res->request instanceof ChainOfResponsibilityRequest);
        $this->assertTrue($res->state === ChainOfResponsibilityResponse::SUCCESS);
        $this->assertTrue($res->message === '');
    }

    public function testHandleWithUnsuccessfulResponse(): void
    {
        $runtime = $this->faker->getFakerForEntity(EnvironmentRuntime::class)->getFakeInstance();
        
        // Build ChainOfResponsibilityRequest
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        
        $factoryPort = $this->faker->getFakerForEntity(EnvironmentFactoryPort::class)->getFakeInstance();
        assert($factoryPort instanceof EnvironmentFactoryPort);
        
        $request->factory->addEnvironmentFactoryPort($factoryPort);

        // Build EnvironmentRuntimeRepository mock
        $runtimeRepositoryMock = $this->getMockBuilder(EnvironmentRuntimeRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $runtimeRepositoryMock->expects($this->once())
            ->method('findOneBy')
            ->withConsecutive([[
                'usedBy' => $request->user,
                'environmentFactory' => $request->factory,
                'shutDownExecutedAt' => null
            ]])
            ->willReturn($runtime);

        // Perform test case
        $handler = new SameFactoryCheckHandler($runtimeRepositoryMock);
        $res = $handler->handle($request);

        $this->assertTrue($res instanceof ChainOfResponsibilityResponse);
        $this->assertTrue($res->request instanceof ChainOfResponsibilityRequest);
        $this->assertTrue($res->state === ChainOfResponsibilityResponse::FAIL);
        $this->assertStringContainsString('The same environment is already running for this user.', $res->message);
    }
}