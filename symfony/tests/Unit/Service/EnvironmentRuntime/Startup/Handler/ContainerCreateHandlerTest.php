<?php declare(strict_types=1);


namespace Tests\Unit\Service\EnvironmentRuntime\Startup\Handler;

use App\Entity\EnvironmentRuntime;
use App\Integration\DockerApi\DockerEngineSocketApi;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntime\Startup\Handler\ContainerCreateHandler;
use Tests\Unit\Service\EnvironmentRuntime\AbstractHandlerTest;
use Exception;

final class ContainerCreateHandlerTest extends AbstractHandlerTest
{
    public function testHandleWithSuccessfulResponse(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        assert($request->runtime instanceof EnvironmentRuntime);
        $request->runtime->setPassword('123-456');
        
        $dockerEngineApiMock = $this->getMockBuilder(DockerEngineSocketApi::class)
            ->disableOriginalConstructor()
            ->onlyMethods(["containerCreate", "containerStart"])
            ->getMock();

        $dockerEngineApiMock
            ->expects($this->once())
            ->method('containerCreate')
            ->withConsecutive([$request->runtime, '123-456']);

        $dockerEngineApiMock
            ->expects($this->once())
            ->method('containerStart')
            ->with($request->runtime);
        
        $handler = new ContainerCreateHandler($dockerEngineApiMock);
        $result = $handler->handle($request);
        
        $this->assertTrue($result instanceof ChainOfResponsibilityResponse);
        $this->assertTrue( $result->state === ChainOfResponsibilityResponse::SUCCESS);        
        $this->assertTrue($result->message === '');
    }

    public function testHandleWithUnsuccessfulResponse(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        assert($request->runtime instanceof EnvironmentRuntime);
        $request->runtime->setPassword('123-456');

        $dockerEngineApiMock = $this->getMockBuilder(DockerEngineSocketApi::class)
            ->disableOriginalConstructor()
            ->onlyMethods(["containerCreate", "containerStart"])
            ->getMock();

        $dockerEngineApiMock
            ->expects($this->once())
            ->method('containerCreate')
            ->withConsecutive([$request->runtime, '123-456']);

        $dockerEngineApiMock
            ->expects($this->once())
            ->method('containerStart')
            ->with($request->runtime)
            ->willThrowException(new Exception("Unable to start the runtime for some reason."));

        $handler = new ContainerCreateHandler($dockerEngineApiMock);
        $result = $handler->handle($request);

        $this->assertTrue($result instanceof ChainOfResponsibilityResponse);
        $this->assertTrue( $result->state === ChainOfResponsibilityResponse::FAIL);
        $this->assertTrue($result->message === 'Unable to start the runtime for some reason.');
    }
}
