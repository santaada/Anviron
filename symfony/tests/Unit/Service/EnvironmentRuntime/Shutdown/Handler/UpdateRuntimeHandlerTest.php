<?php declare(strict_types=1);


namespace Tests\Unit\Service\EnvironmentRuntime\Shutdown\Handler;

use App\Entity\EnvironmentRuntime;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntime\Shutdown\Handler\UpdateRuntimeHandler;
use Doctrine\ORM\EntityManager;
use Tests\Unit\Service\EnvironmentRuntime\AbstractHandlerTest;
use DateTimeImmutable;
use Exception;

final class UpdateRuntimeHandlerTest extends AbstractHandlerTest
{
    public function testHandleWithSuccessfulResponse(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        
        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist', 'flush'])
            ->getMock();

        $entityManagerMock
            ->expects($this->once())
            ->method('persist')
            ->with($request->runtime);

        $entityManagerMock
            ->expects($this->once())
            ->method('flush');

        $handler = new UpdateRuntimeHandler($entityManagerMock);
        $response = $handler->handle($request);
        
        $this->assertTrue($response->request->runtime instanceof EnvironmentRuntime);
        $this->assertTrue($response->request->runtime?->getShutDownExecutedAt() instanceof DateTimeImmutable);
        $this->assertTrue($response->state === ChainOfResponsibilityResponse::SUCCESS);
        $this->assertTrue($response->message === '');
    }

    public function testHandleWithUnsuccessfulResponse(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        
        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist', 'flush'])
            ->getMock();

        $entityManagerMock
            ->expects($this->once())
            ->method('persist')
            ->with($request->runtime)
            ->willThrowException(new Exception('Unable to persist for some reason.'));

        $handler = new UpdateRuntimeHandler($entityManagerMock);
        $response = $handler->handle($request);

        $this->assertTrue($response->request->runtime instanceof EnvironmentRuntime);
        $this->assertTrue($response->state === ChainOfResponsibilityResponse::FAIL);
        $this->assertTrue($response->message === 'Unable to persist for some reason.');
    }
    
    public function testUnhandle(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        $runtime = $request->runtime;
        
        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist', 'flush'])
            ->getMock();

        $entityManagerMock
            ->expects($this->once())
            ->method('persist')
            ->with($request->runtime);
        
        $handler = new UpdateRuntimeHandler($entityManagerMock);
        $handler->unhandle($request);

        $this->assertTrue($runtime instanceof EnvironmentRuntime);
        $this->assertTrue($runtime?->getShutDownExecutedAt() === null);
    }
}
