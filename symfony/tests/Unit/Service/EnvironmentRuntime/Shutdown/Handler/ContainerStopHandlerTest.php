<?php declare(strict_types=1);


namespace Tests\Unit\Service\EnvironmentRuntime\Shutdown\Handler;

use App\Integration\DockerApi\DockerEngineSocketApi;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntime\Shutdown\Handler\ContainerStopHandler;
use App\Service\EnvironmentRuntime\Startup\Handler\ContainerCreateHandler;
use Symfony\Component\Uid\Uuid;
use Tests\Unit\Service\EnvironmentRuntime\AbstractHandlerTest;
use Exception;

final class ContainerStopHandlerTest extends AbstractHandlerTest
{
    public function testHandleWithSuccessfulResponse(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        
        $dockerEngineApiMock = $this->getMockBuilder(DockerEngineSocketApi::class)
            ->disableOriginalConstructor()
            ->onlyMethods(["containerStop", "containerDelete"])
            ->getMock();

        $dockerEngineApiMock
            ->expects($this->once())
            ->method('containerStop')
            ->with($request->runtime);

        $dockerEngineApiMock
            ->expects($this->once())
            ->method('containerDelete')
            ->with($request->runtime);
        
        $handler = new ContainerStopHandler($dockerEngineApiMock);
        $result = $handler->handle($request);
        
        $this->assertTrue($result instanceof ChainOfResponsibilityResponse);
        $this->assertTrue( $result->state === ChainOfResponsibilityResponse::SUCCESS);        
        $this->assertTrue($result->message === '');
    }

    public function testHandleWithUnsuccessfulResponse(): void
    {
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert($request instanceof ChainOfResponsibilityRequest);
        
        $dockerEngineApiMock = $this->getMockBuilder(DockerEngineSocketApi::class)
            ->disableOriginalConstructor()
            ->onlyMethods(["containerStop", "containerDelete"])
            ->getMock();

        $dockerEngineApiMock
            ->expects($this->once())
            ->method('containerStop')
            ->with($request->runtime);

        $dockerEngineApiMock
            ->expects($this->once())
            ->method('containerDelete')
            ->with($request->runtime)
            ->willThrowException(new Exception("Unable to start the runtime for some reason."));

        $handler = new ContainerStopHandler($dockerEngineApiMock);
        $result = $handler->handle($request);

        $this->assertTrue($result instanceof ChainOfResponsibilityResponse);
        $this->assertTrue( $result->state === ChainOfResponsibilityResponse::FAIL);
        $this->assertTrue($result->message === 'Unable to start the runtime for some reason.');
    }
}
