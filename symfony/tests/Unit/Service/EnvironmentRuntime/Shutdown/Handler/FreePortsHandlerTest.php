<?php declare(strict_types=1);


namespace Tests\Unit\Service\EnvironmentRuntime\Shutdown\Handler;

use App\Entity\EnvironmentRuntime;
use App\Entity\EnvironmentRuntimePort;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityResponse;
use App\Service\EnvironmentRuntime\Shutdown\Handler\FreePortsHandler;
use App\Service\EnvironmentRuntimePortService;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManager;
use Tests\Unit\Service\EnvironmentRuntime\AbstractHandlerTest;

final class FreePortsHandlerTest extends AbstractHandlerTest
{
    public function testHandleWithSuccessfulResponse(): void
    {
        // Build EnvironmentRuntimePortService mock
        $runtimePort = new EnvironmentRuntimePort();
        $runtimePort->setExternalPort(49123);
        
        // Build EntityManager mock
        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['flush'])
            ->getMock();
        $entityManagerMock
            ->expects($this->once())
            ->method('flush');
        
        // Build request
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert ($request instanceof ChainOfResponsibilityRequest);
        assert ($request->runtime instanceof EnvironmentRuntime);
        $request->runtime->addRuntimePort($runtimePort);
        
        // Build EnvironmentRuntimePortService mock
        $runtimePortServiceMock = $this->getMockBuilder(EnvironmentRuntimePortService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $runtimePortServiceMock->expects($this->once())
            ->method('free')
            ->withConsecutive([
                $this->isInstanceOf(EnvironmentRuntimePort::class),
            ]);
        
        // Execute test
        $handler = new FreePortsHandler($runtimePortServiceMock, $entityManagerMock);
        $result = $handler->handle($request);
        
        $this->assertTrue($result->request->runtime instanceof EnvironmentRuntime);
    }

    public function testHandleWithUnsuccessfulResponse(): void
    {
        // Build EnvironmentRuntimePortService mock
        $runtimePort = new EnvironmentRuntimePort();
        $runtimePort->setExternalPort(49123);

        // Build EntityManager mock
        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        // Build request
        $request = $this->faker->getFakerForClass(ChainOfResponsibilityRequest::class)->getFakeInstance();
        assert ($request instanceof ChainOfResponsibilityRequest);
        assert ($request->runtime instanceof EnvironmentRuntime);
        $request->runtime->addRuntimePort($runtimePort);

        // Build EnvironmentRuntimePortService mock
        $runtimePortServiceMock = $this->getMockBuilder(EnvironmentRuntimePortService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $runtimePortServiceMock->expects($this->once())
            ->method('free')
            ->withConsecutive([
                $this->isInstanceOf(EnvironmentRuntimePort::class),
            ])
            ->willThrowException( new Exception('Unable to persist for some reason.'));

        // Execute test
        $handler = new FreePortsHandler($runtimePortServiceMock, $entityManagerMock);
        $result = $handler->handle($request);

        $this->assertTrue($result instanceof ChainOfResponsibilityResponse);
        $this->assertTrue( $result->state === ChainOfResponsibilityResponse::FAIL);
        $this->assertTrue($result->message === 'Unable to persist for some reason.');
    }
}
