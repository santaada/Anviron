<?php declare(strict_types=1);

namespace Tests\Unit\Service\EnvironmentRuntime;

use PHPUnit\Framework\TestCase;
use Tests\Faker\FakerFacade;


abstract class AbstractHandlerTest extends TestCase
{
    protected FakerFacade $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = new FakerFacade();
    }
}