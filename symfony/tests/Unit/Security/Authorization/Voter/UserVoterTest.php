<?php declare(strict_types=1);

namespace Tests\Unit\Security\Authorization\Voter;

use App\Entity\Course;
use App\Entity\CourseMember;
use App\Entity\CourseRoleType;
use App\Entity\Subject;
use App\Entity\Term;
use App\Entity\User;
use App\Repository\CourseMemberRepository;
use App\Repository\UserRepository;
use App\Security\Authorization\Voter\UserVoter;
use App\Service\CourseMemberService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;


final class UserVoterTest extends TestCase
{
    /**
     * @phpstan-var MockObject&UserRepository
     */
    private MockObject $userRepository;

    /**
     * @phpstan-var MockObject&CourseMemberRepository
     */
    private MockObject $courseMemberRepository;
    
    /**
     * @phpstan-var MockObject&CourseMemberService
     */
    private MockObject $courseMemberService;
    
    private UserVoter $userVoter;

    /**
     * @phpstan-var MockObject&TokenInterface
     */
    private TokenInterface $token;
    
    protected function setUp(): void
    {
        $this->userRepository = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->courseMemberRepository = $this->getMockBuilder(CourseMemberRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->courseMemberService = $this->getMockBuilder(CourseMemberService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->token = $this->createMock(TokenInterface::class);
        
        $this->userVoter = new UserVoter(
            $this->userRepository,
            $this->courseMemberRepository, 
            $this->courseMemberService
        );
        parent::setUp();
    }
    
    public function testCanReadAndUpdateDetail_OwnProfile(): void
    {
        $user = new User();
        $user->setUsername('umbridol');
        
        $this->token->method('getUser')->willReturn($user);
        
        $attributes = [UserVoter::READ, UserVoter::UPDATE_DETAIL];
        
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED, 
            $this->userVoter->vote($this->token, $user, $attributes),
        );
    }

    public function testCanUpdate_OwnProfile(): void
    {
        $user = new User();
        $user->setUsername('umbridol');

        $this->token->method('getUser')->willReturn($user);

        $attributes = [UserVoter::UPDATE_DETAIL];

        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->userVoter->vote($this->token, $user, $attributes),
        );
    }
    
    public function testCanUpdate_OwnRolesIfAdmin(): void
    {
        $user = new User();
        $user->setUsername('umbridol');
        $user->setRoles(['ROLE_USER', 'ROLE_SYSTEM_ADMIN']);

        $this->token->method('getUser')->willReturn($user);

        $attributes = [UserVoter::UPDATE_ROLES];

        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->userVoter->vote($this->token, $user, $attributes),
        );
    }
    
    public function testCanUpdate_OwnRolesIfNotAdmin(): void
    {
        $user = new User();
        $user->setUsername('umbridol');

        $this->token->method('getUser')->willReturn($user);

        $attributes = [UserVoter::UPDATE_ROLES];

        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $this->userVoter->vote($this->token, $user, $attributes),
        );
    }

    public function testCanRead_TeachersVisibleToAnyone(): void
    {
        $userTeacher = new User();
        $userTeacher->setUsername('umbridol');
        $userTeacher->setRoles(['ROLE_USER']);

        $userStudent = new User();
        $userStudent->setUsername('pottehar');
        $userStudent->setRoles(['ROLE_USER']);

        $userAnotherTeacher = new User();
        $userAnotherTeacher->setUsername('snapesev');
        $userAnotherTeacher->setRoles(['ROLE_USER']);

        $userSystemAdmin = new User();
        $userSystemAdmin->setUsername('dumblalb');
        $userSystemAdmin->setRoles(['ROLE_USER', 'ROLE_SYSTEM_ADMIN']);
        
        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());
        
        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');
        
        $subjectTwo = new Subject();
        $subjectTwo->setName('Poisons and venoms');
        $subjectTwo->setCode('BI-PAV');
        
        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);
        
        $courseTwo = new Course();
        $courseTwo->setTerm($term);
        $courseTwo->setSubject($subjectTwo);
        
        // Dolores teaches course BI-MAG
        $courseMemberTeacher = new CourseMember();
        $courseMemberTeacher->setCourse($courseOne)
            ->setUser($userTeacher)
            ->setRole(CourseRoleType::TEACHER->value);

        // Snape teaches course BI-PAV
        $courseMemberTeacherTwo = new CourseMember();
        $courseMemberTeacherTwo->setCourse($courseTwo)
            ->setUser($userAnotherTeacher)
            ->setRole(CourseRoleType::TEACHER->value);

        // Harry studies BI-MAG but not BI-PAV
        $courseMemberStudent = new CourseMember();
        $courseMemberStudent->setCourse($courseOne)
            ->setUser($userStudent)
            ->setRole(CourseRoleType::STUDENT->value);
        
        // Hook users to user repository
        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userTeacher, $userStudent, $userAnotherTeacher) {
            return match ($arg['username']) {
                'umbridol' => $userTeacher,
                'pottehar' => $userStudent,
                'snapesev' => $userAnotherTeacher,
                default => null,
            };
        }));
        
        // Mock course member service responses
        $this->courseMemberService->method('hasAnyTeacherRole')
            ->will($this->returnCallback(function (User $arg) {
            return match ($arg->getUserIdentifier()) {
                'umbridol' => true,
                'pottehar' => false,
                'snapesev' => true,
                default => null,
            };
        }));
        
        // Student logged in
        $this->token->method('getUser')->willReturn($userStudent);
        // Harry can see Dolores (because Dolores is a teacher and Harry is a student) 
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->userVoter->vote($this->token, $userTeacher, [UserVoter::READ]),
        );
        // Harry can see Snape (because Snape is a teacher and Harry is a student even though he doesn't teach him) 
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->userVoter->vote($this->token, $userAnotherTeacher, [UserVoter::READ]),
        );
    }

    public function testCanRead_StudentsVisibleToTheirTeachers(): void
    {
        $userTeacher = new User();
        $userTeacher->setUsername('umbridol');
        $userTeacher->setRoles(['ROLE_USER']);

        $userStudent = new User();
        $userStudent->setUsername('pottehar');
        $userStudent->setRoles(['ROLE_USER']);

        $userAnotherTeacher = new User();
        $userAnotherTeacher->setUsername('snapesev');
        $userAnotherTeacher->setRoles(['ROLE_USER']);

        $userSystemAdmin = new User();
        $userSystemAdmin->setUsername('dumblalb');
        $userSystemAdmin->setRoles(['ROLE_USER', 'ROLE_SYSTEM_ADMIN']);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $subjectTwo = new Subject();
        $subjectTwo->setName('Poisons and venoms');
        $subjectTwo->setCode('BI-PAV');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        $courseTwo = new Course();
        $courseTwo->setTerm($term);
        $courseTwo->setSubject($subjectTwo);

        // Dolores teaches course BI-MAG
        $courseMemberTeacher = new CourseMember();
        $courseMemberTeacher->setCourse($courseOne)
            ->setUser($userTeacher)
            ->setRole(CourseRoleType::TEACHER->value);

        // Snape teaches course BI-PAV
        $courseMemberTeacherTwo = new CourseMember();
        $courseMemberTeacherTwo->setCourse($courseTwo)
            ->setUser($userAnotherTeacher)
            ->setRole(CourseRoleType::TEACHER->value);

        // Harry studies BI-MAG but not BI-PAV
        $courseMemberStudent = new CourseMember();
        $courseMemberStudent->setCourse($courseOne)
            ->setUser($userStudent)
            ->setRole(CourseRoleType::STUDENT->value);

        // Hook users to user repository
        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userTeacher, $userStudent, $userAnotherTeacher) {
                return match ($arg['username']) {
                    'umbridol' => $userTeacher,
                    'pottehar' => $userStudent,
                    'snapesev' => $userAnotherTeacher,
                    default => null,
                };
            }));

        // Mock course member service responses
        $this->courseMemberService->method('hasAnyTeacherRole')
            ->will($this->returnCallback(function (User $arg) {
                return match ($arg->getUserIdentifier()) {
                    'umbridol' => true,
                    'pottehar' => false,
                    'snapesev' => true,
                    default => null,
                };
            }));

        $this->courseMemberRepository->method('findBy')
            ->will($this->returnCallback(
                function ($arg) use ($courseMemberTeacher, $courseMemberTeacherTwo, $courseMemberStudent) {
                    if ($arg['role'] === CourseRoleType::TEACHER->value) {
                        return match ($arg["user"]->getUserIdentifier()) {
                            'umbridol' => [$courseMemberTeacher],
                            'snapesev' => [$courseMemberTeacherTwo],
                            default => null,
                        };
                    }
                    if ($arg['role'] === CourseRoleType::STUDENT->value) {
                        return match ($arg["user"]->getUserIdentifier()) {
                            'pottehar' => [$courseMemberStudent],
                            default => null,
                        };
                    }
                    return null;
                }
            ));

        // Dolores can see harry because he is her student
        $this->token->method('getUser')->willReturn($userTeacher);
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->userVoter->vote($this->token, $userStudent, [UserVoter::READ]),
        );
    }

    public function testCanRead_StudentsNotVisibleToTheirNonTeachers(): void
    {
        $userTeacher = new User();
        $userTeacher->setUsername('umbridol');
        $userTeacher->setRoles(['ROLE_USER']);

        $userStudent = new User();
        $userStudent->setUsername('pottehar');
        $userStudent->setRoles(['ROLE_USER']);

        $userAnotherTeacher = new User();
        $userAnotherTeacher->setUsername('snapesev');
        $userAnotherTeacher->setRoles(['ROLE_USER']);

        $userSystemAdmin = new User();
        $userSystemAdmin->setUsername('dumblalb');
        $userSystemAdmin->setRoles(['ROLE_USER', 'ROLE_SYSTEM_ADMIN']);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $subjectTwo = new Subject();
        $subjectTwo->setName('Poisons and venoms');
        $subjectTwo->setCode('BI-PAV');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        $courseTwo = new Course();
        $courseTwo->setTerm($term);
        $courseTwo->setSubject($subjectTwo);

        // Dolores teaches course BI-MAG
        $courseMemberTeacher = new CourseMember();
        $courseMemberTeacher->setCourse($courseOne)
            ->setUser($userTeacher)
            ->setRole(CourseRoleType::TEACHER->value);

        // Snape teaches course BI-PAV
        $courseMemberTeacherTwo = new CourseMember();
        $courseMemberTeacherTwo->setCourse($courseTwo)
            ->setUser($userAnotherTeacher)
            ->setRole(CourseRoleType::TEACHER->value);

        // Harry studies BI-MAG but not BI-PAV
        $courseMemberStudent = new CourseMember();
        $courseMemberStudent->setCourse($courseOne)
            ->setUser($userStudent)
            ->setRole(CourseRoleType::STUDENT->value);

        // Hook users to user repository
        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userTeacher, $userStudent, $userAnotherTeacher) {
                return match ($arg['username']) {
                    'umbridol' => $userTeacher,
                    'pottehar' => $userStudent,
                    'snapesev' => $userAnotherTeacher,
                    default => null,
                };
            }));

        // Mock course member service responses
        $this->courseMemberService->method('hasAnyTeacherRole')
            ->will($this->returnCallback(function (User $arg) {
                return match ($arg->getUserIdentifier()) {
                    'umbridol' => true,
                    'pottehar' => false,
                    'snapesev' => true,
                    default => null,
                };
            }));

        $this->courseMemberRepository->method('findBy')
            ->will($this->returnCallback(
                function ($arg) use ($courseMemberTeacher, $courseMemberTeacherTwo, $courseMemberStudent) {
                    if ($arg['role'] === CourseRoleType::TEACHER->value) {
                        return match ($arg["user"]->getUserIdentifier()) {
                            'umbridol' => [$courseMemberTeacher],
                            'snapesev' => [$courseMemberTeacherTwo],
                            default => null,
                        };
                    }
                    if ($arg['role'] === CourseRoleType::STUDENT->value) {
                        return match ($arg["user"]->getUserIdentifier()) {
                            'pottehar' => [$courseMemberStudent],
                            default => null,
                        };
                    }
                    return null;
                }
            ));

        // Snape logged in
        $this->token->method('getUser')->willReturn($userAnotherTeacher);
        // Snape cannot see harry because he is not his student
        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $this->userVoter->vote($this->token, $userStudent, [UserVoter::READ]),
        );
    }

    public function testCanRead_StudentInvisibleToAnotherStudent(): void
    {
        $userTeacher = new User();
        $userTeacher->setUsername('umbridol');
        $userTeacher->setRoles(['ROLE_USER']);

        $userStudent = new User();
        $userStudent->setUsername('pottehar');
        $userStudent->setRoles(['ROLE_USER']);

        $userAnotherTeacher = new User();
        $userAnotherTeacher->setUsername('snapesev');
        $userAnotherTeacher->setRoles(['ROLE_USER']);

        $userSystemAdmin = new User();
        $userSystemAdmin->setUsername('dumblalb');
        $userSystemAdmin->setRoles(['ROLE_USER', 'ROLE_SYSTEM_ADMIN']);

        $userAnotherStudent = new User();
        $userAnotherStudent->setUsername('weaseher');
        $userAnotherStudent->setRoles(['ROLE_USER']);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        // Harry studies BI-MAG
        $courseMemberStudentOne = new CourseMember();
        $courseMemberStudentOne->setCourse($courseOne)
            ->setUser($userStudent)
            ->setRole(CourseRoleType::STUDENT->value);

        // Hermiona studies BI-MAG
        $courseMemberStudentTwo = new CourseMember();
        $courseMemberStudentTwo->setCourse($courseOne)
            ->setUser($userAnotherStudent)
            ->setRole(CourseRoleType::STUDENT->value);

        // Hook users to user repository
        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userTeacher, $userStudent, $userAnotherTeacher, $userAnotherStudent) {
                return match ($arg['username']) {
                    'umbridol' => $userTeacher,
                    'pottehar' => $userStudent,
                    'weaseher' => $userAnotherStudent,
                    'snapesev' => $userAnotherTeacher,
                    default => null,
                };
            }));

        // Mock course member service responses
        $this->courseMemberService->method('hasAnyTeacherRole')
            ->will($this->returnCallback(function (User $arg) {
                return match ($arg->getUserIdentifier()) {
                    'umbridol' => true,
                    'pottehar' => false,
                    'weaseher' => false,
                    'snapesev' => true,
                    default => null,
                };
            }));

        $this->courseMemberRepository->method('findBy')
            ->will($this->returnCallback(
                function ($arg) use ($courseMemberStudentOne, $courseMemberStudentTwo) {
                    if ($arg['role'] === CourseRoleType::STUDENT->value) {
                        return match ($arg["user"]->getUserIdentifier()) {
                            'umbridol' => [$courseMemberStudentOne],
                            'snapesev' => [$courseMemberStudentTwo],
                            default => null,
                        };
                    }
                    return null;
                }
            ));

        // Harry logged in
        $this->token->method('getUser')->willReturn($userStudent);
        // Harry cannot see Hermiona even though they study the same course
        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $this->userVoter->vote($this->token, $userAnotherStudent, [UserVoter::READ]),
        );
    }
}