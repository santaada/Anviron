<?php declare(strict_types=1);

namespace Tests\Unit\Security\Authorization\Voter;

use App\Entity\Course;
use App\Entity\CourseMember;
use App\Entity\CourseRoleType;
use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentImage;
use App\Entity\EnvironmentImageBuild;
use App\Entity\Subject;
use App\Entity\Term;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Security\Authorization\Voter\EnvironmentFactoryVoter;
use App\Service\CourseMemberService;
use DateTimeImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;


final class EnvironmentFactoryVoterTest extends TestCase
{
    /**
     * @phpstan-var MockObject&UserRepository
     */
    private UserRepository $userRepository;
    
    /**
     * @phpstan-var MockObject&CourseMemberService
     */
    private CourseMemberService $courseMemberService;

    /**
     * @phpstan-var MockObject&TokenInterface
     */
    private TokenInterface $token;
    
    private EnvironmentFactoryVoter $factoryVoter;

    protected function setUp(): void
    {
        $this->userRepository = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->courseMemberService = $this->getMockBuilder(CourseMemberService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->token = $this->createMock(TokenInterface::class);

        $this->factoryVoter = new EnvironmentFactoryVoter(
            $this->userRepository,
            $this->courseMemberService,
        );
        parent::setUp();
    }

    public function testCanRead_AdminCanRead(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
            UserRoles::ROLE_SYSTEM_ADMIN->value,
        ]);

        $userSnapesev = new User();
        $userSnapesev->setUsername('snapesev')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($userSnapesev);
        $build->setTag('demo-build');
        
        $factory = new EnvironmentFactory();
        $factory->setName('my-awesome-factory');
        $factory->setMaintainedBy($userSnapesev);
        $factory->setStartupScript('');
        $factory->setShutdownScript('');
        $factory->setUserDocs('Lorem Ipsum');
        $factory->setEnvironmentImageBuild($build);
        $factory->setIsVisibleForCourseStudents(false);
        
        $this->token->method('getUser')->willReturn($userUmbridol);
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->factoryVoter->vote($this->token, $build, [EnvironmentFactoryVoter::READ]),
        );
    }

    public function testCanRead_TeacherCanReadIfTeachesRelatedCourse(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);
    
        $userSnapesev = new User();
        $userSnapesev->setUsername('snapesev')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        // Dolores teaches course BI-MAG
        $courseMemberTeacher = new CourseMember();
        $courseMemberTeacher->setCourse($courseOne)
            ->setUser($userUmbridol)
            ->setRole(CourseRoleType::TEACHER->value);

        // Snape also teaches course BI-MAG
        $courseMemberTeacherTwo = new CourseMember();
        $courseMemberTeacherTwo->setCourse($courseOne)
            ->setUser($userSnapesev)
            ->setRole(CourseRoleType::TEACHER->value);

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($userUmbridol);
        $build->setTag('demo-build');
        
        // Dolores creates an environment for students of BI-MAG
        $factory = new EnvironmentFactory();
        $factory->setName('my-awesome-factory');
        $factory->setMaintainedBy($userUmbridol);
        $factory->setStartupScript('');
        $factory->setShutdownScript('');
        $factory->setUserDocs('Lorem Ipsum');
        $factory->setEnvironmentImageBuild($build);
        $factory->addCourse($courseOne);
        $factory->setIsVisibleForCourseStudents(false);

        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userSnapesev, $userUmbridol) {
                return match ($arg['username']) {
                    'umbridol' => $userUmbridol,
                    'snapesev' => $userSnapesev,
                    default => null,
                };
            }));
        
        // Mock course member service responses
        $this->courseMemberService->method('hasAnyTeacherRoleInCourses')
            ->will($this->returnCallback(function (User $user, array $courses) use ($courseOne){
                return match ($user->getUserIdentifier()) {
                    'umbridol' =>
                        in_array($courseOne, $courses),
                    'snapesev' =>
                        in_array($courseOne, $courses),
                    default => null,
                };
            }));
        
        // Snape does teach BI-PAV => he can see the Dolores's env
        $this->token->method('getUser')->willReturn($userSnapesev);
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->factoryVoter->vote($this->token, $factory, [EnvironmentFactoryVoter::READ]),
        );
    }

    public function testCanRead_TeacherCanNotReadIfDoesntTeachRelatedCourse(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $userSnapesev = new User();
        $userSnapesev->setUsername('snapesev')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $subjectTwo = new Subject();
        $subjectTwo->setName('Poisons and venoms');
        $subjectTwo->setCode('BI-PAV');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        $courseTwo = new Course();
        $courseTwo->setTerm($term);
        $courseTwo->setSubject($subjectTwo);

        // Dolores teaches course BI-MAG
        $courseMemberTeacher = new CourseMember();
        $courseMemberTeacher->setCourse($courseOne)
            ->setUser($userUmbridol)
            ->setRole(CourseRoleType::TEACHER->value);

        // Snape teaches course BI-PAV
        $courseMemberTeacherTwo = new CourseMember();
        $courseMemberTeacherTwo->setCourse($courseTwo)
            ->setUser($userSnapesev)
            ->setRole(CourseRoleType::TEACHER->value);

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($userSnapesev);
        $build->setTag('demo-build');

        // Snape creates an environment for students of BI-PAV
        $factory = new EnvironmentFactory();
        $factory->setName('my-awesome-factory');
        $factory->setMaintainedBy($userSnapesev);
        $factory->setStartupScript('');
        $factory->setShutdownScript('');
        $factory->setUserDocs('Lorem Ipsum');
        $factory->setEnvironmentImageBuild($build);
        $factory->addCourse($courseTwo);
        $factory->setIsVisibleForCourseStudents(false);

        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userSnapesev, $userUmbridol) {
                return match ($arg['username']) {
                    'umbridol' => $userUmbridol,
                    'snapesev' => $userSnapesev,
                    default => null,
                };
            }));

        // Mock course member service responses
        $this->courseMemberService->method('hasAnyTeacherRoleInCourses')
            ->will($this->returnCallback(function (User $user, array $courses) use ($courseOne, $courseTwo){
                return match ($user->getUserIdentifier()) {
                    'umbridol' =>
                    in_array($courseOne, $courses),
                    'snapesev' =>
                    in_array($courseTwo, $courses),
                    default => null,
                };
            }));

        // Dolores doesn't teach BI-PAV => she cannot see the Snape's env
        $this->token->method('getUser')->willReturn($userUmbridol);
        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $this->factoryVoter->vote($this->token, $factory, [EnvironmentFactoryVoter::READ]),
        );
    }
    
    public function testCanRead_EnrolledStudentCannotReadIfNotPublihed(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $userPottehar = new User();
        $userPottehar->setUsername('pottehar')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        // Dolores teaches course BI-MAG
        $courseMemberTeacher = new CourseMember();
        $courseMemberTeacher->setCourse($courseOne)
            ->setUser($userUmbridol)
            ->setRole(CourseRoleType::TEACHER->value);

        // Harry studies course BI-MAG
        $courseMemberTeacherTwo = new CourseMember();
        $courseMemberTeacherTwo->setCourse($courseOne)
            ->setUser($userPottehar)
            ->setRole(CourseRoleType::STUDENT->value);

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($userUmbridol);
        $build->setTag('demo-build');

        // Dolores creates an environment for students of BI-MAG
        $factory = new EnvironmentFactory();
        $factory->setName('my-awesome-factory');
        $factory->setMaintainedBy($userUmbridol);
        $factory->setStartupScript('');
        $factory->setShutdownScript('');
        $factory->setUserDocs('Lorem Ipsum');
        $factory->setEnvironmentImageBuild($build);
        $factory->addCourse($courseOne);
        $factory->setIsVisibleForCourseStudents(false);

        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userUmbridol, $userPottehar) {
                return match ($arg['username']) {
                    'umbridol' => $userUmbridol,
                    'pottehar' => $userPottehar,
                    default => null,
                };
            }));

        // Mock course member service responses
        $this->courseMemberService->method('hasAnyTeacherRoleInCourses')
            ->will($this->returnCallback(function (User $user, array $courses) use ($courseOne){
                return match ($user->getUserIdentifier()) {
                    'umbridol' => in_array($courseOne, $courses),
                    default => false,
                };
            }));

        $this->courseMemberService->method('hasAnyStudentRoleInCourses')
            ->will($this->returnCallback(function (User $user, array $courses) use ($courseOne){
                return match ($user->getUserIdentifier()) {
                    'pottehar' => in_array($courseOne, $courses),
                    default => false,
                };
            }));

        // Harry studies BI-MAG but cannot see the env since it's not published by Dolores
        $this->token->method('getUser')->willReturn($userPottehar);
        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $this->factoryVoter->vote($this->token, $factory, [EnvironmentFactoryVoter::READ]),
        );
    }

    public function testCanRead_EnrolledStudentCanReadIfPublihed(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $userPottehar = new User();
        $userPottehar->setUsername('pottehar')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        // Dolores teaches course BI-MAG
        $courseMemberTeacher = new CourseMember();
        $courseMemberTeacher->setCourse($courseOne)
            ->setUser($userUmbridol)
            ->setRole(CourseRoleType::TEACHER->value);

        // Harry studies course BI-MAG
        $courseMemberTeacherTwo = new CourseMember();
        $courseMemberTeacherTwo->setCourse($courseOne)
            ->setUser($userPottehar)
            ->setRole(CourseRoleType::STUDENT->value);

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($userUmbridol);
        $build->setTag('demo-build');

        // Dolores creates an environment for students of BI-MAG
        $factory = new EnvironmentFactory();
        $factory->setName('my-awesome-factory');
        $factory->setMaintainedBy($userUmbridol);
        $factory->setStartupScript('');
        $factory->setShutdownScript('');
        $factory->setUserDocs('Lorem Ipsum');
        $factory->setEnvironmentImageBuild($build);
        $factory->addCourse($courseOne);
        $factory->setIsVisibleForCourseStudents(true);

        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userUmbridol, $userPottehar) {
                return match ($arg['username']) {
                    'umbridol' => $userUmbridol,
                    'pottehar' => $userPottehar,
                    default => null,
                };
            }));

        // Mock course member service responses
        $this->courseMemberService->method('hasAnyTeacherRoleInCourses')
            ->will($this->returnCallback(function (User $user, array $courses) use ($courseOne){
                return match ($user->getUserIdentifier()) {
                    'umbridol' => in_array($courseOne, $courses),
                    default => false,
                };
            }));

        $this->courseMemberService->method('hasAnyStudentRoleInCourses')
            ->will($this->returnCallback(function (User $user, array $courses) use ($courseOne){
                return match ($user->getUserIdentifier()) {
                    'pottehar' => in_array($courseOne, $courses),
                    default => false,
                };
            }));

        // Harry studies BI-MAG and cannot see the env since it's published by Dolores
        $this->token->method('getUser')->willReturn($userPottehar);
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->factoryVoter->vote($this->token, $factory, [EnvironmentFactoryVoter::READ]),
        );
    }

    public function testCanRead_NonEnrolledStudentCanNotReadIfPublihed(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $userPottehar = new User();
        $userPottehar->setUsername('pottehar')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        // Dolores teaches course BI-MAG
        $courseMemberTeacher = new CourseMember();
        $courseMemberTeacher->setCourse($courseOne)
            ->setUser($userUmbridol)
            ->setRole(CourseRoleType::TEACHER->value);

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($userUmbridol);
        $build->setTag('demo-build');

        // Dolores creates an environment for students of BI-MAG
        $factory = new EnvironmentFactory();
        $factory->setName('my-awesome-factory');
        $factory->setMaintainedBy($userUmbridol);
        $factory->setStartupScript('');
        $factory->setShutdownScript('');
        $factory->setUserDocs('Lorem Ipsum');
        $factory->setEnvironmentImageBuild($build);
        $factory->addCourse($courseOne);
        $factory->setIsVisibleForCourseStudents(true);

        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userUmbridol, $userPottehar) {
                return match ($arg['username']) {
                    'umbridol' => $userUmbridol,
                    'pottehar' => $userPottehar,
                    default => null,
                };
            }));

        // Mock course member service responses
        $this->courseMemberService->method('hasAnyTeacherRoleInCourses')
            ->will($this->returnCallback(function (User $user, array $courses) use ($courseOne){
                return match ($user->getUserIdentifier()) {
                    'umbridol' => in_array($courseOne, $courses),
                    default => false,
                };
            }));

        $this->courseMemberService->method('hasAnyStudentRoleInCourses')
            ->willReturn(false);

        // Harry does not study BI-MAG and cannot see the env even though it's published by Dolores
        $this->token->method('getUser')->willReturn($userPottehar);
        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $this->factoryVoter->vote($this->token, $factory, [EnvironmentFactoryVoter::READ]),
        );
    }

    public function testCanCreate_AdminCanCreate(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
            UserRoles::ROLE_SYSTEM_ADMIN->value,
        ]);

        $this->token->method('getUser')->willReturn($userUmbridol);
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->factoryVoter->vote($this->token, null, [EnvironmentFactoryVoter::CREATE]),
        );
    }

    public function testCanCreate_AnyTeacherCanCreate(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        // Dolores teaches course BI-MAG
        $courseMemberTeacher = new CourseMember();
        $courseMemberTeacher->setCourse($courseOne)
            ->setUser($userUmbridol)
            ->setRole(CourseRoleType::TEACHER->value);

        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userUmbridol) {
                return match ($arg['username']) {
                    'umbridol' => $userUmbridol,
                    default => null,
                };
            }));
        
        $this->courseMemberService->method('hasAnyTeacherRole')
            ->will($this->returnCallback(function (User $user) {
                return match ($user->getUserIdentifier()) {
                    'umbridol' => true,
                    default => false,
                };
            }));

        $this->token->method('getUser')->willReturn($userUmbridol);
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->factoryVoter->vote($this->token, null, [EnvironmentFactoryVoter::CREATE]),
        );
    }

    public function testCanCreate_StudentCannotCreate(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        // Dolores studies course BI-MAG
        $courseMemberTeacher = new CourseMember();
        $courseMemberTeacher->setCourse($courseOne)
            ->setUser($userUmbridol)
            ->setRole(CourseRoleType::STUDENT->value);

        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($userUmbridol) {
                return match ($arg['username']) {
                    'umbridol' => $userUmbridol,
                    default => null,
                };
            }));

        $this->courseMemberService->method('hasAnyTeacherRole')->willReturn(false);

        $this->token->method('getUser')->willReturn($userUmbridol);
        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $this->factoryVoter->vote($this->token, null, [EnvironmentFactoryVoter::CREATE]),
        );
    }

    public function testCanUpdate_AdminCanUpdate(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
            UserRoles::ROLE_SYSTEM_ADMIN->value,
        ]);

        $userSnapesev = new User();
        $userSnapesev->setUsername('snapesev')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($userSnapesev);
        $build->setTag('demo-build');

        $factory = new EnvironmentFactory();
        $factory->setName('my-awesome-factory');
        $factory->setMaintainedBy($userSnapesev);
        $factory->setStartupScript('');
        $factory->setShutdownScript('');
        $factory->setUserDocs('Lorem Ipsum');
        $factory->setEnvironmentImageBuild($build);
        $factory->addCourse($courseOne);
        $factory->setIsVisibleForCourseStudents(true);

        $this->token->method('getUser')->willReturn($userUmbridol);
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->factoryVoter->vote($this->token, $factory, [EnvironmentFactoryVoter::UPDATE]),
        );
    }

    public function testCanUpdate_MaintainerCanUpdate(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($userUmbridol);
        $build->setTag('demo-build');

        $factory = new EnvironmentFactory();
        $factory->setName('my-awesome-factory');
        $factory->setMaintainedBy($userUmbridol);
        $factory->setStartupScript('');
        $factory->setShutdownScript('');
        $factory->setUserDocs('Lorem Ipsum');
        $factory->setEnvironmentImageBuild($build);
        $factory->addCourse($courseOne);
        $factory->setIsVisibleForCourseStudents(true);

        $this->token->method('getUser')->willReturn($userUmbridol);
        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->factoryVoter->vote($this->token, $factory, [EnvironmentFactoryVoter::UPDATE]),
        );
    }

    public function testCanUpdate_NonMaintainerCanNotUpdate(): void
    {
        $userUmbridol = new User();
        $userUmbridol->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $userSnapesev = new User();
        $userSnapesev->setUsername('snapesev')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $term = new Term();
        $term->setCode('T001');
        $term->setStartsAt(new \DateTimeImmutable());
        $term->setEndsAt(new \DateTimeImmutable());

        $subjectOne = new Subject();
        $subjectOne->setName('Elements of Magic 1');
        $subjectOne->setCode('BI-MAG');

        $courseOne = new Course();
        $courseOne->setTerm($term);
        $courseOne->setSubject($subjectOne);

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($userUmbridol);
        $build->setTag('demo-build');

        $factory = new EnvironmentFactory();
        $factory->setName('my-awesome-factory');
        $factory->setMaintainedBy($userUmbridol);
        $factory->setStartupScript('');
        $factory->setShutdownScript('');
        $factory->setUserDocs('Lorem Ipsum');
        $factory->setEnvironmentImageBuild($build);
        $factory->addCourse($courseOne);
        $factory->setIsVisibleForCourseStudents(true);

        $this->token->method('getUser')->willReturn($userSnapesev);
        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $this->factoryVoter->vote($this->token, $factory, [EnvironmentFactoryVoter::UPDATE]),
        );
    }
}