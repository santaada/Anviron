<?php declare(strict_types=1);

namespace Tests\Unit\Security\Authorization\Voter;

use App\Entity\EnvironmentImage;
use App\Entity\EnvironmentImageBuild;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\Authorization\UserRoles;
use App\Security\Authorization\Voter\EnvironmentImageBuildVoter;
use App\Service\CourseMemberService;
use DateTimeImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;


final class EnvironmentImageBuildVoterTest extends TestCase
{
    /**
     * @phpstan-var MockObject&UserRepository
     */
    private UserRepository $userRepository;
    
    /**
     * @phpstan-var MockObject&CourseMemberService
     */
    private CourseMemberService $courseMemberService;

    /**
     * @phpstan-var MockObject&TokenInterface
     */
    private TokenInterface $token;
    
    private EnvironmentImageBuildVoter $imageBuildVoter;

    protected function setUp(): void
    {
        $this->userRepository = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->courseMemberService = $this->getMockBuilder(CourseMemberService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->token = $this->createMock(TokenInterface::class);

        $this->imageBuildVoter = new EnvironmentImageBuildVoter(
            $this->userRepository,
            $this->courseMemberService,
        );
        parent::setUp();
    }

    public function testCanRead_AdminCanRead(): void
    {
        $user = new User();
        $user->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
            UserRoles::ROLE_SYSTEM_ADMIN->value,
        ]);
        
        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($user);
        $build->setTag('demo-build');

        $this->token->method('getUser')->willReturn($user);

        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->imageBuildVoter->vote($this->token, $build, [EnvironmentImageBuildVoter::READ]),
        );
    }

    public function testCanRead_TeacherCanRead(): void
    {
        $user = new User();
        $user->setUsername('umbridol')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);
        
        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($user) {
                return match ($arg['username']) {
                    'umbridol' => $user,
                    default => null,
                };
            }));

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($user);
        $build->setTag('demo-build');

        $this->courseMemberService->method('hasAnyTeacherRole')
            ->will($this->returnCallback(function (User $arg) {
                return match ($arg->getUserIdentifier()) {
                    'umbridol' => true,
                    default => false,
                };
            }));
        
        $this->token->method('getUser')->willReturn($user);

        $this->assertEquals(
            VoterInterface::ACCESS_GRANTED,
            $this->imageBuildVoter->vote($this->token, $build, [EnvironmentImageBuildVoter::READ]),
        );
    }
    
    public function testCanRead_StudentCannotRead(): void
    {
        $user = new User();
        $user->setUsername('weaseron')->setRoles([
            UserRoles::ROLE_USER->value,
        ]);

        $this->userRepository->method('findOneBy')
            ->will($this->returnCallback(function (array $arg) use ($user) {
                return match ($arg['username']) {
                    'weaseron' => $user,
                    default => null,
                };
            }));

        $image = new EnvironmentImage();
        $image->setName('sample image');
        $image->setDockerfile('FROM scratch');

        $build = new EnvironmentImageBuild();
        $build->setDockerfile($image->getDockerfile());
        $build->setEnvironmentImage($image);
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($user);
        $build->setTag('demo-build');

        $this->token->method('getUser')->willReturn($user);

        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $this->imageBuildVoter->vote($this->token, $build, [EnvironmentImageBuildVoter::READ]),
        );
    }
}