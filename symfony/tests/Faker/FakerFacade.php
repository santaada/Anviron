<?php declare(strict_types=1);

namespace Tests\Faker;

use App\Entity\AbstractEntity;
use Tests\Faker\ClassFakerInterface;


class FakerFacade
{
    public function __construct(
         /** @var array<string, ClassFakerInterface<mixed>> */
         protected array $fakersMap = [],
    ) {}

    /**
     * @return ClassFakerInterface<AbstractEntity>
     */
    public function getFakerForEntity(string $className): ClassFakerInterface
    {
        /** @var ClassFakerInterface<AbstractEntity> $res */
        $res = $this->getFakerForClass($className);
        assert($res instanceof ClassFakerInterface); 
        return $res;
    }
    
    /**
     * @return ClassFakerInterface<mixed>
     */
    public function getFakerForClass(string $className): ClassFakerInterface
    {
        if (isset($this->fakersMap[$className])) {
            /** @var ClassFakerInterface<mixed> */
            $res = $this->fakersMap[$className];
            assert($res instanceof ClassFakerInterface);
            return $res;
        } else {
            $fakerClassImplicitName = str_replace('App\\', 'Tests\\Faker\\', $className) . 'Faker';
            if ( class_exists($fakerClassImplicitName) ) {
                $res = new ($fakerClassImplicitName)();
                assert($res instanceof ClassFakerInterface);
                /** @retrun ClassFakerInterface<mixed> */
                return $res;
            }
            throw new \Exception(
                'FakerFacade: No explicit or implicit Faker class found for '
                . $className
            );
        }
    }
}