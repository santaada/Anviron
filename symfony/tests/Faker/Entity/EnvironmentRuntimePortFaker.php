<?php declare(strict_types=1);

namespace Tests\Faker\Entity;

use App\Entity\EnvironmentFactoryPort;
use App\Entity\EnvironmentRuntime;
use App\Entity\EnvironmentRuntimePort;
use Tests\Faker\ClassFakerInterface;
use Tests\Faker\FakerFacade;

/**
 * @implements ClassFakerInterface<EnvironmentRuntimePort>
 */
class EnvironmentRuntimePortFaker implements ClassFakerInterface
{
    protected FakerFacade $fakerFacade;

    public function __construct(?FakerFacade $fakerFacade = null) {
        $this->fakerFacade = $fakerFacade ?? new FakerFacade();
    }
    
    public function getFakeInstance(): EnvironmentRuntimePort
    {
        $runtime = $this->fakerFacade->getFakerForEntity(EnvironmentRuntime::class)->getFakeInstance();
        $factoryPort = $this->fakerFacade->getFakerForEntity(EnvironmentFactoryPort::class)->getFakeInstance();
        assert($runtime instanceof EnvironmentRuntime);
        assert($factoryPort instanceof EnvironmentFactoryPort);
        
        $runtimePort = new EnvironmentRuntimePort();
        $runtimePort->setExternalPort(49123);
        $runtimePort->setEnvironmentRuntime($runtime);
        $runtimePort->setFactoryPort($factoryPort);
        $runtimePort->setAllocatedAt(new \DateTimeImmutable());
        
        return $runtimePort;
    }
}