<?php declare(strict_types=1);

namespace Tests\Faker\Entity;

use App\Entity\ConnectionType;
use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentFactoryPort;
use Tests\Faker\ClassFakerInterface;
use Tests\Faker\FakerFacade;

/**
 * @implements ClassFakerInterface<EnvironmentFactoryPort>
 */
class EnvironmentFactoryPortFaker implements ClassFakerInterface
{
    protected FakerFacade $fakerFacade;

    public function __construct(?FakerFacade $fakerFacade = null) {
        $this->fakerFacade = $fakerFacade ?? new FakerFacade();
    }
    
    public function getFakeInstance(): EnvironmentFactoryPort
    {
        $factory = $this->fakerFacade->getFakerForEntity(EnvironmentFactory::class)->getFakeInstance();
        assert($factory instanceof EnvironmentFactory);
        
        $factoryPort = new EnvironmentFactoryPort();
        $factoryPort->setEnvironmentFactory($factory);
        $factory->addEnvironmentFactoryPort($factoryPort);
        $factoryPort->setExternalPort(443);
        $factoryPort->setIsEntryPort(true);
        $factoryPort->setConnectionType(ConnectionType::TCP->value);


        return $factoryPort;
    }
}