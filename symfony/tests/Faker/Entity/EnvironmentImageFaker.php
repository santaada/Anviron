<?php declare(strict_types=1);

namespace Tests\Faker\Entity;

use App\Entity\EnvironmentImage;
use Tests\Faker\ClassFakerInterface;

/**
 * @implements ClassFakerInterface<EnvironmentImage>
 */
class EnvironmentImageFaker implements ClassFakerInterface
{
    public function getFakeInstance(): EnvironmentImage
    {
        $image = new EnvironmentImage();
        $image->setName('Testing Image');
        $image->setDockerfile('FROM scratch');
        
        return $image;
    }
}