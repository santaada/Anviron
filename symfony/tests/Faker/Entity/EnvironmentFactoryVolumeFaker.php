<?php declare(strict_types=1);

namespace Tests\Faker\Entity;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentFactoryPort;
use App\Entity\EnvironmentFactoryVolume;
use Tests\Faker\ClassFakerInterface;
use Tests\Faker\FakerFacade;

/**
 * @implements ClassFakerInterface<EnvironmentFactoryVolume>
 */
class EnvironmentFactoryVolumeFaker implements ClassFakerInterface
{
    protected FakerFacade $fakerFacade;

    public function __construct(?FakerFacade $fakerFacade = null) {
        $this->fakerFacade = $fakerFacade ?? new FakerFacade();
    }
    
    public function getFakeInstance(): EnvironmentFactoryVolume
    {
        $factory = $this->fakerFacade->getFakerForEntity(EnvironmentFactory::class)->getFakeInstance();
        assert($factory instanceof EnvironmentFactory);

        $factoryVolume = new EnvironmentFactoryVolume();
        $factoryVolume->setEnvironmentFactory($factory);
        $factory->addEnvironmentFactoryVolume($factoryVolume);
        $factoryVolume->setContainerPath('/home/.some-dir');
        
        return $factoryVolume;
    }
}