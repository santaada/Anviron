<?php declare(strict_types=1);

namespace Tests\Faker\Entity;

use App\Entity\User;
use Generator;
use Tests\Faker\ClassFakerInterface;

/**
 * @implements ClassFakerInterface<User>
 */
class UserFaker implements ClassFakerInterface
{
    public function getFakeInstance(): User
    {
        $user = new User();
        $user->setFirstName('Tiffany');
        $user->setLastName('Testing');
        $user->setUsername('testitif');
        $user->setEmail('tiffany@testing.com');
        
        return $user;
    }
}