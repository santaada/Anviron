<?php declare(strict_types=1);

namespace Tests\Faker\Entity;

use App\Entity\EnvironmentImage;
use App\Entity\EnvironmentImageBuild;
use App\Entity\User;
use DateTimeImmutable;
use Tests\Faker\ClassFakerInterface;
use Tests\Faker\FakerFacade;

/**
 * @implements ClassFakerInterface<EnvironmentImageBuild>
 */
class EnvironmentImageBuildFaker implements ClassFakerInterface
{
    protected FakerFacade $fakerFacade;
    
    public function __construct(?FakerFacade $fakerFacade = null) {
        $this->fakerFacade = $fakerFacade ?? new FakerFacade();
    }
    
    public function getFakeInstance(): EnvironmentImageBuild
    {
        $user = $this->fakerFacade->getFakerForEntity(User::class)->getFakeInstance();
        $image = $this->fakerFacade->getFakerForEntity(EnvironmentImage::class)->getFakeInstance();
        assert($user instanceof User);
        assert($image instanceof EnvironmentImage);
        
        $build = new EnvironmentImageBuild();
        $build->setDockerfile('FROM scratch');
        $build->setTag('test-202201010000');
        $build->setCreatedAt(new DateTimeImmutable('now'));
        $build->setCreatedBy($user);
        $build->setEnvironmentImage($image);
        
        return $build;
    }
}