<?php declare(strict_types=1);

namespace Tests\Faker\Entity;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentImageBuild;
use App\Entity\User;
use Tests\Faker\ClassFakerInterface;
use Tests\Faker\FakerFacade;


/**
 * @implements ClassFakerInterface<EnvironmentFactory>
 */
class EnvironmentFactoryFaker implements ClassFakerInterface
{
    protected FakerFacade $fakerFacade;
    
    public function __construct(?FakerFacade $fakerFacade = null) {
        $this->fakerFacade = $fakerFacade ?? new FakerFacade();
    }
    
    public function getFakeInstance(): EnvironmentFactory
    {
        $user = $this->fakerFacade->getFakerForEntity(User::class)->getFakeInstance();
        $build = $this->fakerFacade->getFakerForEntity(EnvironmentImageBuild::class)->getFakeInstance();
        assert($user instanceof User);
        assert($build instanceof EnvironmentImageBuild);
        
        $factory = new EnvironmentFactory();
        $factory->setName('Testing Factory');
        $factory->setMaintainedBy($user);
        $factory->setEnvironmentImageBuild($build);
        $factory->setStartupScript("echo \"Hello world \"");
        $factory->setShutdownScript("echo \"Goodbye world \"");
        
        return $factory;
    }
}