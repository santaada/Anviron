<?php declare(strict_types=1);

namespace Tests\Faker\Entity;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentRuntime;
use App\Entity\User;
use Tests\Faker\ClassFakerInterface;
use Tests\Faker\FakerFacade;

/**
 * @implements ClassFakerInterface<EnvironmentRuntime>
 */
class EnvironmentRuntimeFaker implements ClassFakerInterface
{
    protected FakerFacade $fakerFacade;
    
    public function __construct(?FakerFacade $fakerFacade = null) {
        $this->fakerFacade = $fakerFacade ?? new FakerFacade();
    }
    
    public function getFakeInstance(): EnvironmentRuntime
    {
        $user = $this->fakerFacade->getFakerForEntity(User::class)->getFakeInstance();
        $factory = $this->fakerFacade->getFakerForEntity(EnvironmentFactory::class)->getFakeInstance();
        assert($user instanceof User);
        assert($factory instanceof EnvironmentFactory);
        
        $runtime = new EnvironmentRuntime();
        $now = new \DateTimeImmutable('now');
        $runtime->setStartedUpAt($now);
        $runtime->setShutDownScheduledAt($now->add(new \DateInterval('PT3H')));
        $runtime->setUrl('');
        $runtime->setUsedBy($user);
        $runtime->setEnvironmentFactory($factory);
        $runtime->setPassword(null);
        
        return $runtime;
    }
}