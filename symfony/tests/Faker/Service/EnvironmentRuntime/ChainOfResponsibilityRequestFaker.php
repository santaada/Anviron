<?php declare(strict_types=1);

namespace Tests\Faker\Service\EnvironmentRuntime;

use App\Entity\EnvironmentFactory;
use App\Entity\EnvironmentRuntime;
use App\Entity\User;
use App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest;
use Tests\Faker\ClassFakerInterface;
use Tests\Faker\FakerFacade;


/** @implements ClassFakerInterface<ChainOfResponsibilityRequest> */
class ChainOfResponsibilityRequestFaker implements ClassFakerInterface
{
    protected FakerFacade $fakerFacade;
    
    public function __construct() {
        $this->fakerFacade = new FakerFacade();
    }
    
    /**
     * @return \App\Service\EnvironmentRuntime\ChainOfResponsibilityRequest
     */
    public function getFakeInstance(): ChainOfResponsibilityRequest
    {
        // Build ChainOfResponsibilityRequest
        $factory = $this->fakerFacade->getFakerForEntity(EnvironmentFactory::class)->getFakeInstance();        
        $user = $this->fakerFacade->getFakerForEntity(User::class)->getFakeInstance();
        $runtime = $this->fakerFacade->getFakerForEntity(EnvironmentRuntime::class)->getFakeInstance();
        
        assert($factory instanceof EnvironmentFactory);
        assert($user instanceof User);
        assert($runtime instanceof EnvironmentRuntime);

        return new ChainOfResponsibilityRequest(
            $user,
            $factory,
            $runtime,
        );
    }
}