<?php declare(strict_types=1);

namespace Tests\Faker;

/**
 * @template FakedClass
 */
interface ClassFakerInterface
{
    /** @return FakedClass */
    public function getFakeInstance();
}