# Anviron
**Automatic environments.**

Anviron app aims to reduce time spent on development tools setup, targeting mostly younger students of Computer Science, Software Engineering and similar study branches.

The student is provided with storage for source codes and pre-installed binaries, so that the need for local installation of compilers, debuggers, or different binaries on a student's physical computer is eliminated. The starter pack of files and binaries comes prepared from the course teachers as a Docker container.

This solution should be valuable e.g. for Windows OS users, who are having hard times to set up a C / C++ compiler once they've recently enrolled to their university. Most of them are not familiar with Docker (yet) and don't wish to install any Unix-like OS to their physical computers.

The system should be also beneficial for classes teaching basic concepts of other programming languages, such as PHP or Python.

## Setup
- Copy `docker-compose.override.dist.yaml`, name it `docker-compose.override.yaml` and fill in missing values
  - Generate project tokens and secrets `AUTH_FIT_*` at https://auth.fit.cvut.cz/manager
  - Fill in (ideally absolute) path to a folder which can be used as a simple storage managed by the app, for example `/<path to this project root>/symfony/var/anviron`
- Generate certificates
  - `cd docker/nginx/certs`
  - `sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx-selfsigned.key -out nginx-selfsigned.crt`
- Expose certificates to docker
  - `sudo chmod o+r nginx-selfsigned.key`
- Make the machine's Docker socket (`/var/run/docker.sock`) accessible to the PHP-FPM container
  - `sudo chmod o+w /var/run/docker.sock`
- Run app via Docker
  - `docker compose up -d`
- Initialize database
  - `docker compose exec php-fpm bash`
  - `php bin/console doctrine:migrations:migrate`
- Load sample data (optional)
  - `php bin/console doctrine:fixtures:load --quiet`
- Grant yourself a `"ROLE_SYSTEM_ADMIN"` permission in the DB 
  - table `user`, column `roles` needs to be set to `["ROLE_SYSTEM_ADMIN"]` 
  - (you can use Adminer on port 1785), see `docker-compose.yaml` for credentials
- Make the machine's `symfony/var/anviron` volume accessible to the PHP-FPM container
  - `sudo chmod o+rw -R symfony/var/anviron`

## Tools
- Static analysis (PHPStan)
  - `vendor/bin/phpstan analyse src tests --level max --memory-limit 256M`
- Tests (PHPUnit)
  - `vendor/bin/phpunit`